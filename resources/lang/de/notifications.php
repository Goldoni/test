<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created'   => 'Neues Kommentar  wurde  von :sender erstellt.',
    'deleted'   => 'Neues Kommentar  wurde  von :sender gelöscht.',
    'login'     => 'Connectez-vous à votre compte',
    'new_comment'      => 'Sie Haben  :count neue Nachrichten',
    'to_notifications'      => 'Toutes les Notifications',
    'inbox'      => 'Sie Haben 14 neue E-Mail-Benachrichtigungen',


    'success'          => 'Success',
    'user_saved_successfully' => 'User saved successfully.',

];
