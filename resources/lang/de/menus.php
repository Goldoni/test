<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'notifications'    => 'Mes Notifications',
    'inbox'    => 'Mes Emails',
    'tasks'    => 'Mes Tâches',
    'profile'  => 'Mon Profil',
    'lock'     => 'Écran Verrouillé',
    'logout'   => 'Déconnecter',
    'language' => 'Langue',

    'fr'   => 'Français',
    'en'   => 'Anglais',
    'de'   => 'Allemand',

    'home'        => 'Accueil',
    'blog'        => 'Mon Blog',
    'book'        => 'Carnet Médical',
    'check_lists' => 'Liste de Contrôle',
    'urgents'     =>'Toutes les Urgences',
    'urgent'      => 'Urgence',
    'all_kinds'   => 'Tous Bébés',
    'my_kinds'    => 'Mes Bébés',
    'doctor'      => 'Médecins',
    'board'       => 'Tableau de Bord',
    'users'       => 'Users Manager',
    'blog'        => 'Mon Blog',

    'sport'        => 'Sport',



];
