<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'save'             => 'Speichern',
    'close'            => 'Schließen',
    'id'               => 'ID',
    'name'             => 'Name',
    'username'         => 'Benutzername',
    'first_name'       => 'Vorname',
    'last_name'        => 'Nachname',
    'full_name'        => 'Namen',
    'email'            => 'Email',
    'country'          => 'Land',
    'password'         => 'Passwort',
    'password_confirm' => 'Passwortbestätigung',
    'language'         => 'Sprache',
    'skype'            => 'Skype',
    'about'            => 'Über Mich',
    'interest'         => 'Interet',
    'actived'          => 'Active',
    'tel'              => 'Telefon',
    'street'           => 'Strasse',
    'city'             => 'Stadt',
    'zipcode'          => 'Postleitzahl',



    'login'    => 'Anmelden',
    'register' => 'Registrieren',
    'contact'  => 'Kontaktieren Uns',
    'created'  => 'Ihre Konto wurde erfolgreich Erstellt. Die E-Mail mit dem Bestätigungslink wurde versendet. Bitte prüfen Sie Ihren E-Mail-Eingang und bestätigen.',
    'subject'  => 'Anmeldung bei Health4kids',
    'welcome'  => 'Merci de Confirmer votre compte',
    'agreed'   => 'Bestätigung des Konto',
    'cancel'   => 'Benutzer unbekannt!',


    'remember' => 'Angemeldet bleiben',
    'failed'   => 'Entrez un nom d\'utilisateur et un mot de passe.',
    'reset'    => 'Geben Sie Ihre E-Mail-Adresse ein, um Ihr Passwort zurückzusetzen.',
    'login_or' => 'Anmeldung zu Ihrem Konto mit',
    'noaccount'=> 'Vous n\'avez pas encore de compte ?',
    'create'   => 'Créer un compte',
    'signup'   => 'S\'inscrire',
    'personalbelow'  => 'Entrez vos Informations:',
    'accountbelow'   => 'Entrez vos Identifiants:',
    'agree'    => 'Je suis d\'accord avec les',
    'service'  => 'Conditions d\'utilisations',
    'policy'   => 'Politique de confidentialité',

    'click'    => 'Klicken Sie',
    'here'     => 'hier, ',
    '_reset'   => 'um Ihr Passwort zurückzusetzen.',
    'forget'   => 'Passwort vergessen ?',
    'submit'   => 'Absenden',
    'back'     => 'Zurück',

];
