<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login'    => 'Connectez-vous à votre compte',
    'description'    => 'Health For Kids:Children\'s Medical Platform in Urban and Rural Areas in Cameroon',
    'about'    => 'About Us',
    'company'  => 'Health For Kids',
    '_company'  => 'Children\'s Medical Platform',


];
