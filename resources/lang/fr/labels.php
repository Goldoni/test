<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'save'             => 'Sauvegarder',
    'close'            => 'Fermer',
    'id'               => 'ID',
    'name'             => 'Nom',
    'username'         => 'Pseudo',
    'first_name'       => 'Prenom',
    'last_name'        => 'Nom',
    'full_name'        => 'Noms',
    'email'            => 'Email',
    'country'          => 'Pays',
    'password'         => 'Mot de Passe',
    'password_confirm' => 'Confirmer le Mot de Passe',
    'language'         => 'Langue',
    'skype'            => 'Skype',
    'about'            => 'Biographie',
    'fax'              => 'Fax',
    'title'            => 'title',
    'interest'         => 'Interet',
    'actived'          => 'Active',
    'tel'              => 'Numer de Telephone',
    'city'             => 'Ville',
    'interet'          => 'interet',
    'occupation'       => 'occupation',
    'street'           => 'Rue',
    'zipcode'          => 'Code Postal',
    'birth'            => 'Date de Naissance',
    'gender'           => 'Sexe du Bébé',
    'male'             => 'Garçon',
    'female'           => 'Fille',
    'size'             => 'Taille du Bébé',
    'weight'           => 'Poids du Bébé',
    'allergy'          => 'Allergies',
    'asthmatic'        => 'Asthmatique',
    'mother'           => 'Maternelle',
    'artificial'       => 'Artificielle',
    'mixed'            => 'Mixte',
    'Family'           => 'Plat Familial',
    'other'            => 'Autre',
    'food'             => 'alimentation',
    'last_login'       => 'Dernière Visite',
    'ha'               => 'Hépatite A',
    'hb'               => 'Hépatite B',
    'Haemophilus'      => 'Hib: Hémophilus',
    'bcg'              => 'BCG',
    'grippe'           => 'Grippe',
    'cough'            => 'Coqueluche',
    'cough'            => 'Coqueluche',
    'ror'              => 'Rougeole-Oreillons-Rubéole (ROR)',
    'pneumococcal'     => 'Pneumocoque',
    'status_vaccine'   => 'Vaccins à jour',
    'vaccine'          => 'Vaccins déjà effectués',
    'deworming'        => 'Date Déparasitage',
    'general'          => 'INFORMATIONS GENERALES',
    'confirm'          => 'CONFIRMATION',
    'vitamins'         => 'Vitamines',
    'medical'          => 'INFORMATIONS MEDICALES',
    'vd'               => 'VACCINS ET DATES',
    'no'               => 'Non',
    'yes'              => 'Oui',
    'board'              => 'Board',
    'description'              => 'Description',
    'members'              => 'Membres',
    'tags'              => 'Tags',
    'tag'              => 'Tag',



    'login'    => 'Se Connecter',
    'register' => 'S\'Inscrire',
    'contact'  => 'Nous Contacter',
    'created'  => 'Votre compte  à été créé, Merci de confirmer par l\'email qui vous  à été envoyé',
    'subject'  => 'Inscription à Health4kids',
    'welcome'  => 'Merci de Confirmer votre compte',
    'agreed'   => 'Confirmer mon compte',
    'cancel'   => 'cest pas moi!',

    
    'remember' => 'Se souvenir de moi',
    'username' => 'Nom d\'utilisateur',
    'password' => 'Mot de passe',
    'failed'   => 'Entrez un nom d\'utilisateur et un mot de passe.',
    'reset'    => 'Entrez votre adresse e-mail ci-dessous pour réinitialiser votre mot de passe.',
    'login_or' => 'Ou connectez-vous avec',
    'noaccount'=> 'Vous n\'avez pas encore de compte ?',
    'create'   => 'Créer un compte',
    'signup'   => 'S\'inscrire',
    'personalbelow'  => 'Entrez vos Informations:',
    'accountbelow'   => 'Entrez vos Identifiants:',
    'agree'    => 'Je suis d\'accord avec les',
    'service'  => 'Conditions d\'utilisations',
    'policy'   => 'Politique de confidentialité',
    

    'click'    => 'Cliquez',
    'here'     => 'ici',
    '_reset'   => 'pour réinitialiser votre mot de passe.',
    'forget'   => 'Mot de passe oublié ?',
    'submit'   => 'Submit',
    'back'     => 'Retour',

];
