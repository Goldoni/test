<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created'   => 'Un nouveau commentaire a été créé par :sender.',
    'reply'     => ':sender a répondu à un de vos commentaires.',
    'deleted'   => 'Un commentaire a été supprimés par :sender',
    'login'     => 'Connectez-vous à votre compte',
    'new_comment'      => 'Vous avez  :count nouvelles notifications',
    'to_notifications'      => 'Toutes les Notifications',
    'inbox'      => 'Vous avez 14 nouveaux Emails',

    
    'close'            => 'Fermer',
    'id'               => 'ID',
    'name'             => 'Nom',
    'username'         => 'Pseudo',
    'first_name'       => 'Prenom',
    'last_name'        => 'Nom',
    'full_name'        => 'Noms',
    'email'            => 'Email',
    'country'          => 'Pays',
    'password'         => 'Mot de Passe',
    'password_confirm' => 'Confirmer le Mot de Passe',
    'language'         => 'Langue',
    'skype'            => 'Skype',
    'about'            => 'Biographie',
    'interest'         => 'Interet',
    'actived'          => 'Active',
    'tel'              => 'Numer de Telephone',
    'street'           => 'Rue',
    'zipcode'          => 'Code Postal',


    'success'          => 'Success',
    'user_saved_successfully' => 'User saved successfully.',

];
