<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login'    => 'Connectez-vous à votre compte',
    'description'    => 'Health For Kids: Plateforme de suivi médical des Enfants dans les zones urbaines et  rurales au Cameroun',
    'about'    => 'À Propos de Nous',
    'company'  => 'Health For Kids',
    '_company'  => 'Plateforme de suivi médical des enfants',
    'birth'  => 'Née le',


];
