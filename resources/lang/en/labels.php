<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'save'             => 'Save',
    'close'            => 'Close',
    'id'               => 'ID',
    'name'             => 'Name',
    'username'         => 'Username',
    'first_name'       => 'Fisrtname',
    'last_name'        => 'Lastname',
    'full_name'        => 'Full Name',
    'email'            => 'Email',
    'country'          => 'Country',
    'password'         => 'Password',
    'password_confirm' => 'Confirmer le Mot de Passe',
    'language'         => 'Language',
    'skype'            => 'Skype',
    'about'            => 'About Your',
    'interest'         => 'Interet',
    'actived'          => 'Active',
    'tel'              => 'Numero de Telephone',
    'street'           => 'Street',
    'zipcode'          => 'Zip',



    'login'    => 'Sign In',
    'register' => 'Register',
    'contact'  => 'Contact Us',
    'created'  => 'Votre compte  à été créé, Merci de confirmer par l\'email qui vous  à été envoyé',
    'subject'  => 'Inscription à Health4kids',
    'welcome'  => 'Merci de Confirmer votre compte',
    'agreed'   => 'Confirmer mon compte',
    'cancel'   => 'cest pas moi!',


    'remember' => 'Remember Me',
    'failed'   => 'Entrez un nom d\'utilisateur et un mot de passe.',
    'reset'    => 'Entrez votre adresse e-mail ci-dessous pour réinitialiser votre mot de passe.',
    'login_or' => 'Ou connectez-vous avec',
    'noaccount'=> 'Vous n\'avez pas encore de compte ?',
    'create'   => 'Créer un compte',
    'signup'   => 'S\'inscrire',
    'personalbelow'  => 'Entrez vos Informations:',
    'accountbelow'   => 'Entrez vos Identifiants:',
    'agree'    => 'Je suis d\'accord avec les',
    'service'  => 'Conditions d\'utilisations',
    'policy'   => 'Politique de confidentialité',
    'profile'  => 'Mon Profil',
    'calendar' => 'Mon Calendrier',
    'inbox'    => 'Mon Inbox',
    'tasks'    => 'Mes Tâches',
    'lock'     => 'Écran Verrouillé',
    'logout'   => 'Déconnecter',

    'click'    => 'Cliquez',
    'here'     => 'ici',
    '_reset'   => 'pour réinitialiser votre mot de passe.',
    'forget'   => 'Mot de passe oublié ?',
    'submit'   => 'Submit',
    'back'     => 'Retour',

];
