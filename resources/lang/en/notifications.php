<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created'   => 'New comment has been created by :sender.',
    'deleted'   => 'Comment has been deleted by :sender .',


    'success'          => 'Success',
    'user_saved_successfully' => 'User saved successfully.',

];
