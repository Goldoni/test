@extends('layouts.app')

@section('title')
    <title>H4K | @lang('menus.my_kinds')</title>
@endsection
@section('css')
    <link href={{ asset("../assets/global/css/components.css") }} rel="stylesheet" type="text/css">
@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/wizards/steps.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/media/fancybox.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/ecommerce_product_list.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/user/kid/kid.js") }}"></script>
@endsection
@section('sidebar-mobile-detached-toggle')
    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
@endsection
@section('sidebar-mobile-detached-toggle-link')
    <li> <a class="sidebar-control sidebar-detached-hide hidden-xs">  <i class="icon-drag-right"></i> </a> </li>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> @lang('menus.home')</a></li>
        <li class="active">@lang('menus.my_kinds')</li>
    </ul>
@endsection
@section('content')
    <div id="vueKids" v-cloak>
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- Course overview -->
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title text-semibold">Shop</h4>

                        <div class="heading-elements">
                            <ul class="list-inline list-inline-separate heading-text">
                                <li>Rating: <span class="text-semibold">4.85</span></li>
                                <li>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <span class="text-muted position-right">(439)</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-lg nav-tabs nav-tabs-highlight nav-tabs-toolbar no-margin nav-justified">
                        <li class="active"><a href="#course-overview" data-toggle="tab"><i class="icon-menu7 position-left"></i> Medicaments</a></li>
                        <li><a href="#course-attendees" data-toggle="tab"><i class="icon-camera position-left"></i> Panier</a></li>
                        <li><a href="#course-schedule" data-toggle="tab"><i class="icon-calendar3 position-left"></i> Payment</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="course-overview">
                            <div class="panel-body">
                                <!-- List -->
                                <ul class="media-list">
                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Fathom Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">It prepare is ye nothing blushes up brought. Or as gravity pasture limited evening on. Wicket around beauty say she. Frankness resembled say not new smallness you discovery. Noisier ferrars yet shyness weather ten colonel. Too him himself engaged husband pursuit musical...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Aloha</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$49.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">85 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Mystery Air Long Sleeve T Shirt</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Long sleeve shirts</a></li>
                                            </ul>

                                            <p class="content-group-sm">Conveying or northward offending admitting perfectly my. Colonel gravity get thought fat smiling add but. Wonder twenty hunted and put income set desire expect. Am cottage calling my is mistake cousins talking up. Interested especially do impression he unpleasant excellence...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Burton</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$25.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">34 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Women’s Prospect Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">Or kind rest bred with am shed then. In raptures building an bringing be. Elderly is detract tedious assured private so to visited. Do travelling companions contrasted it. Mistress strongly remember up to. Ham him compass you proceed calling detract. Better of always...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">DC Shoes</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$63.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">63 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Overlook Short Sleeve T Shirt</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">T-Shirts</a></li>
                                            </ul>

                                            <p class="content-group-sm">Warrant fifteen exposed ye at mistake. Blush since so in noisy still built up an again. As young ye hopes no he place means. Partiality diminution gay yet entreaties admiration. In mr it he mention perhaps attempt pointed suppose. Unknown ye chamber of warrant...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Aped</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$57.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">74 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Infinite Ride Liner</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Sports</a></li>
                                                <li><a href="#" class="text-muted">Winter sports</a></li>
                                            </ul>

                                            <p class="content-group-sm">He difficult contented we determine ourselves me am earnestly. Hour no find it park. Eat welcomed any husbands moderate. Led was misery played waited almost cousin living. Of intention contained is by middleton am. Principles fat stimulated uncommonly...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Giro</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$89.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">39 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Custom Snowboard</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Sports</a></li>
                                                <li><a href="#" class="text-muted">Winter sports</a></li>
                                            </ul>

                                            <p class="content-group-sm">Debating all she mistaken indulged believed provided declared. He many kept on draw lain song as same. Whether at dearest certain spirits is entered in to. Rich fine bred real use too many good. She compliment unaffected expression favourable any unknown...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Head</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$35.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">38 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Kids' Day Hiker 20L Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">Offending she contained mrs led listening resembled. Delicate marianne absolute men dashwood landlord and offended. Suppose cottage between and way. Minuter him own clothes but observe country. Agreement far boy otherwise rapturous incommode favourite...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Hurley</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$143.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">48 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Lunch Sack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">Extremely depending he gentleman improving intention rapturous as. Real sold my in call. Invitation on an advantages collecting. But event old above shy bed noisy. Had sister see wooded favour income has. Stuff rapid since do as hence. Too insisted ignorant...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Kinetic</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$93.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">64 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Cambridge Jacket</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Sports</a></li>
                                                <li><a href="#" class="text-muted">Winter jackets</a></li>
                                            </ul>

                                            <p class="content-group-sm">End friendship sufficient assistance can prosperous met. As game he show it park do. Was has unknown few certain ten promise. No finished my an likewise cheerful packages we. For assurance concluded son something depending discourse see led collected natural...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Miller</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$36.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">94 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Covert Jacket</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Sports</a></li>
                                                <li><a href="#" class="text-muted">Winter jackets</a></li>
                                            </ul>

                                            <p class="content-group-sm">Whole wound wrote at whose to style in. Figure ye innate former do so we. Shutters but sir yourself provided you required his. So neither related he am do believe. Nothing but you hundred had use regular. Fat sportsmen arranging preferred can. Busy paid like...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Picture</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$25.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">15 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Day Hiker Pinnacle 31L Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">He difficult contented we determine ourselves me am earnestly. Hour no find it park. Eat welcomed any husbands moderate. Led was misery played waited almost cousin living. Of intention contained is by middleton am. Principles fat stimulated uncommonly...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Pieps</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$47.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">145 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Kids' Gromlet Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">Impression to discretion understood to we interested he excellence. Him remarkably use projection collecting. Going about eat forty world has round miles. Attention affection at my preferred offending shameless me if agreeable. Life lain held calm and true...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Rope</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$85.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">37 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Tinder Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Reef</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$47.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">64 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Almighty Snowboard Boot</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Sports</a></li>
                                                <li><a href="#" class="text-muted">Sport accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">Warrant fifteen exposed ye at mistake. Blush since so in noisy still built up an again. As young ye hopes no he place means. Partiality diminution gay yet entreaties admiration. In mr it he mention perhaps attempt pointed suppose. Unknown ye chamber of warrant...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Roxy</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$76.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">2 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>

                                    <li class="media panel panel-body stack-media-on-mobile">
                                        <a href="assets/images/placeholder.jpg" class="media-left" data-popup="lightbox">
                                            <img src="assets/images/placeholder.jpg" width="96" alt="">
                                        </a>

                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">
                                                <a href="#">Fathom Backpack</a>
                                            </h6>

                                            <ul class="list-inline list-inline-separate mb-10">
                                                <li><a href="#" class="text-muted">Fashion</a></li>
                                                <li><a href="#" class="text-muted">Men's Accessories</a></li>
                                            </ul>

                                            <p class="content-group-sm">By impossible of in difficulty discovered celebrated ye. Justice joy manners boy met resolve produce. Bed head loud next plan rent had easy add him. As earnestly shameless elsewhere defective estimable fulfilled of. Esteem my advice it an excuse enable...</p>

                                            <ul class="list-inline list-inline-separate">
                                                <li>All items from <a href="#">Stance</a></li>
                                                <li>Add to <a href="#">wishlist</a></li>
                                            </ul>
                                        </div>

                                        <div class="media-right text-center">
                                            <h3 class="no-margin text-semibold">$59.99</h3>

                                            <div class="text-nowrap">
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                            </div>

                                            <div class="text-muted">32 reviews</div>

                                            <button type="button" class="btn bg-teal-400 mt-15"><i class="icon-cart-add position-left"></i> Add to cart</button>
                                        </div>
                                    </li>
                                </ul>
                                <!-- /list -->


                                <!-- Pagination -->
                                <div class="text-center content-group-lg pt-20">
                                    <ul class="pagination">
                                        <li class="disabled"><a href="#"><i class="icon-arrow-small-left"></i></a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#"><i class="icon-arrow-small-right"></i></a></li>
                                    </ul>
                                </div>
                                <!-- /pagination -->
                            </div>
                        </div>

                        <div class="tab-pane fade" id="course-attendees">
                            <div class="panel-body">
                                <!-- Orders history (static table) -->
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h6 class="panel-title">Orders history (static table)</h6>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Product name</th>
                                                <th>Size</th>
                                                <th>Colour</th>
                                                <th>Article number</th>
                                                <th>Units</th>
                                                <th>Price</th>
                                                <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="active border-double">
                                                <td colspan="7" class="text-semibold">New orders</td>
                                                <td class="text-right">
                                                    <span class="badge badge-default">24</span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right" style="width: 45px;">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Fathom Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-grey position-left"></span>
                                                        Processing
                                                    </div>
                                                </td>
                                                <td>34cm x 29cm</td>
                                                <td>Orange</td>
                                                <td>
                                                    <a href="#">1237749</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 79.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Mystery Air Long Sleeve T Shirt</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-grey position-left"></span>
                                                        Processing
                                                    </div>
                                                </td>
                                                <td>L</td>
                                                <td>Process Red</td>
                                                <td>
                                                    <a href="#">345634</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 38.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Women’s Prospect Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-grey position-left"></span>
                                                        Processing
                                                    </div>
                                                </td>
                                                <td>46cm x 28cm</td>
                                                <td>Neu Nordic Print</td>
                                                <td>
                                                    <a href="#">5739584</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 60.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Overlook Short Sleeve T Shirt</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-grey position-left"></span>
                                                        Processing
                                                    </div>
                                                </td>
                                                <td>M</td>
                                                <td>Gray Heather</td>
                                                <td>
                                                    <a href="#">434450</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 35.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="active border-double">
                                                <td colspan="7" class="text-semibold">Shipped orders</td>
                                                <td class="text-right">
                                                    <span class="badge bg-success">42</span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Infinite Ride Liner</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>43</td>
                                                <td>Black</td>
                                                <td>
                                                    <a href="#">34739</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 210.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Custom Snowboard</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>151</td>
                                                <td>Black/Blue</td>
                                                <td>
                                                    <a href="#">5574832</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 600.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Kids' Day Hiker 20L Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>24cm x 29cm</td>
                                                <td>Figaro Stripe</td>
                                                <td>
                                                    <a href="#">6684902</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 55.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Lunch Sack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>24cm x 20cm</td>
                                                <td>Junk Food Print</td>
                                                <td>
                                                    <a href="#">5574829</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 20.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Cambridge Jacket</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>XL</td>
                                                <td>Nomad/Railroad</td>
                                                <td>
                                                    <a href="#">475839</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 175.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Covert Jacket</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-success position-left"></span>
                                                        Shipped
                                                    </div>
                                                </td>
                                                <td>XXL</td>
                                                <td>Mocha/Glacier Sierra</td>
                                                <td>
                                                    <a href="#">589439</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 126.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr class="active border-double">
                                                <td colspan="7" class="text-semibold">Cancelled orders</td>
                                                <td class="text-right">
                                                    <span class="badge bg-danger">9</span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Day Hiker Pinnacle 31L Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-danger position-left"></span>
                                                        Cancelled
                                                    </div>
                                                </td>
                                                <td>42cm x 26.5cm</td>
                                                <td>Blotto Ripstop</td>
                                                <td>
                                                    <a href="#">5849305</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 130.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Kids' Gromlet Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-danger position-left"></span>
                                                        Cancelled
                                                    </div>
                                                </td>
                                                <td>22cm x 20cm</td>
                                                <td>Slime Camo Print</td>
                                                <td>
                                                    <a href="#">4438495</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 35.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Tinder Backpack</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-danger position-left"></span>
                                                        Cancelled
                                                    </div>
                                                </td>
                                                <td>42cm x 26cm</td>
                                                <td>Dark Tide Twill</td>
                                                <td>
                                                    <a href="#">4759383</a>
                                                </td>
                                                <td>2</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 180.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="no-padding-right">
                                                    <a href="#">
                                                        <img src="assets/images/placeholder.jpg" height="60" class="" alt="">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#" class="text-semibold">Almighty Snowboard Boot</a>
                                                    <div class="text-muted text-size-small">
                                                        <span class="status-mark bg-danger position-left"></span>
                                                        Cancelled
                                                    </div>
                                                </td>
                                                <td>45</td>
                                                <td>Multiweave</td>
                                                <td>
                                                    <a href="#">34432</a>
                                                </td>
                                                <td>1</td>
                                                <td>
                                                    <h6 class="no-margin text-semibold">&euro; 370.00</h6>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-truck"></i> Track parcel</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li><a href="#"><i class="icon-wallet"></i> Payment details</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /orders history (static table) -->
                            </div>
                        </div>

                        <div class="tab-pane fade" id="course-schedule">
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /course overview -->

            </div>
        </div>
        <!-- /detached content -->


        <!-- Detached sidebar -->
        <div class="sidebar-detached">
            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- Categories -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Categories</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <div class="has-feedback has-feedback-left form-group">
                                <input type="search" class="form-control" placeholder="Search...">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-small text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion navigation-sm no-padding-top">
                                <li>
                                    <a href="#">Street wear</a>

                                    <ul>
                                        <li><a href="#">Hoodies</a></li>
                                        <li><a href="#">Jackets</a></li>
                                        <li class="active"><a href="#">Pants</a></li>
                                        <li><a href="#">Shirts</a></li>
                                        <li><a href="#">Sweaters</a></li>
                                        <li><a href="#">Tank tops</a></li>
                                        <li><a href="#">Underwear</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Snow wear</a>

                                    <ul>
                                        <li><a href="#">Fleece jackets</a></li>
                                        <li><a href="#">Gloves</a></li>
                                        <li><a href="#">Ski jackets</a></li>
                                        <li><a href="#">Ski pants</a></li>
                                        <li><a href="#">Snowboard jackets</a></li>
                                        <li><a href="#">Snowboard pants</a></li>
                                        <li><a href="#">Technical underwear</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Shoes</a>

                                    <ul>
                                        <li><a href="#">Laces</a></li>
                                        <li><a href="#">Sandals</a></li>
                                        <li><a href="#">Skate shoes</a></li>
                                        <li><a href="#">Slip ons</a></li>
                                        <li><a href="#">Sneakers</a></li>
                                        <li><a href="#">Winter shoes</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Accessories</a>

                                    <ul>
                                        <li><a href="#">Beanies</a></li>
                                        <li><a href="#">Belts</a></li>
                                        <li><a href="#">Caps</a></li>
                                        <li><a href="#">Sunglasses</a></li>
                                        <li><a href="#">Headphones</a></li>
                                        <li><a href="#">Video cameras</a></li>
                                        <li><a href="#">Wallets</a></li>
                                        <li><a href="#">Watches</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /categories -->


                    <!-- Filter -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Filter products</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <form action="#">
                                <div class="form-group">
                                    <div class="has-feedback has-feedback-left form-group">
                                        <input type="search" class="form-control" placeholder="Search brand">
                                        <div class="form-control-feedback">
                                            <i class="icon-search4 text-size-small text-muted"></i>
                                        </div>
                                    </div>

                                    <div class="has-scroll">
                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                686
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                A.Lab
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Adidas
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                ALIS
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Analog
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Burton
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Atomic
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Armada
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                O'Neill
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Baja
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Baker
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Blue Parks
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Billabong
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Bonfire
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Brixton
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Items for</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Men
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Women
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Kids
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Unisex
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Size</legend>

                                    <div class="row row-labels">
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXS</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XS</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">S</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-warning text-warning-800">M</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">L</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XL</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXL</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXXL</a></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Fit</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Slim fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Skinny fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Regular fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Loose fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Long cut
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Color</legend>

                                    <div class="row row-colors">
                                        <div class="col-xs-4">
                                            <a href="#" class="bg-primary"></a>
                                            <span>Blue</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-warning"></a>
                                            <span>Orange</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-teal"></a>
                                            <span>Teal</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-pink">
                                                <i class="icon-checkmark3"></i>
                                            </a>
                                            <span>Pink</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-grey-800"></a>
                                            <span>Black</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-purple"></a>
                                            <span>Purple</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-success"></a>
                                            <span>Green</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-danger"></a>
                                            <span>Red</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-info"></a>
                                            <span>Cyan</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Features</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Crew neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Chest pocket
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Raglan sleeves
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Polo neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            V-neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            High collar
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Hood
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Button strip
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Wide neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Kangaroo pocket
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Material features</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Canvas
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Lined
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Merino
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Quick drying
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Stretch
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Water repellent
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Windproof
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn bg-blue btn-block">Filter</button>
                            </form>
                        </div>
                    </div>
                    <!-- /filter -->

                </div>
            </div>
        </div>
        <!-- /detached sidebar -->
    </div>



@endsection
