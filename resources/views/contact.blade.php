@extends('layouts.cecam')

@section('title')
    <title>CECAM | Contact</title>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">

        <div class="contact-wrap">
            <h2 class="section-title">Contact CECAM HH</h2>
            <p>Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V</p>
            <h2 class="section-title">Send A Message</h2>
            <div class="from-style" >
                <div class="cf-msg"></div>
                <form action="mail.php" method="post" id="cf">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <input type="text" placeholder="Name" id="fname" name="fname">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <input type="text" placeholder="Email" id="email" name="email">
                        </div>
                        <div class=" col-sm-4 col-xs-12">
                            <input type="text" placeholder="Subject" id="subject" name="subject">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea class="contact-textarea" placeholder="Message" id="msg" name="msg"></textarea>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button class="cont-submit button" id="submit" name="submit">Send A Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection