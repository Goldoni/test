@if(session('locale') == 'fr')
<li class="dropdown language-switch">
    <a href="{{ route('lang', ['lang' => 'fr']) }}" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <img src={{ asset("assets/images/flags/fr.png")}} class="position-left" alt="">
        French
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu">
        <li><a href="{{ route('lang', ['lang' => 'de']) }}" class="deutsch"><img src={{ asset("assets/images/flags/de.png")}} alt=""> Deutsch</a></li>
        <li><a href="{{ route('lang', ['lang' => 'en']) }}" class="english"><img src={{ asset("assets/images/flags/gb.png")}} alt=""> English</a></li>
    </ul>
</li>
@elseif(session('locale') == 'de')
    <li class="dropdown language-switch">
        <a href="{{ route('lang', ['lang' => 'de']) }}" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <img src={{ asset("assets/images/flags/de.png")}} class="position-left" alt="">
            Deutsch
            <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ route('lang', ['lang' => 'fr']) }}" class="french"><img src={{ asset("assets/images/flags/fr.png")}} alt=""> French</a></li>
            <li><a href="{{ route('lang', ['lang' => 'en']) }}" class="english"><img src={{ asset("assets/images/flags/gb.png")}} alt=""> English</a></li>
        </ul>
    </li>
@else
    <li class="dropdown language-switch">
        <a href="{{ route('lang', ['lang' => 'en']) }}" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <img src={{ asset("assets/images/flags/gb.png")}} class="position-left" alt="">
            English
            <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ route('lang', ['lang' => 'fr']) }}" class="french"><img src={{ asset("assets/images/flags/fr.png")}} alt=""> French</a></li>
            <li><a href="{{ route('lang', ['lang' => 'de']) }}" class="deutsch"><img src={{ asset("assets/images/flags/de.png")}} alt=""> Deutsch</a></li>
        </ul>
    </li>
@endif