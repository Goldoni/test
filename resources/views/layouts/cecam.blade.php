<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @yield('title', '<title>CECAM HH</title>')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/bootstrap.min.css") }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/meanmenu.min.css") }}">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/owl.carousel.css") }}">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/font-awesome.min.css") }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/style.css") }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset("css/cecam/responsive.css") }}">
    <!-- modernizr css -->
    <script rel="stylesheet" href="{{ asset("js/cecam/vendor/modernizr-2.8.3.min.js") }}"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <script type="text/javascript" src={{ asset("js/cecam/vendor/jquery-1.12.4.min.js") }}></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header class="header-area">
    <div class="header-top bg-1 ptb-10">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <div class="socil-icon text-uppercase">
                        @if(Auth::check())
                            <a href="javascript:;">Welcome {{ Auth::user()->username }} !</a><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="{{ route('login') }}">Log-in</a><a href="{{ route('register') }}">Register</a>
                        @endif
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-3 col-md-offset-3 col-sm-3 col-xs-12">
                    <div class="search-box mt-10">
                        <form action="#">
                            <input type="search" placeholder="Search..." />
                            <button><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.cecam.navbar')
</header>
<!-- header-end -->
@include('layouts.cecam.carousel')
<!-- blog-area start-->
<div class="blog-area mtb-80">
    <div class="container">
        <div class="row">
            @yield('content')
            @yield('sidebar')
        </div>
    </div>
</div>
<!-- blog-area end-->
@include('layouts.cecam.footer')
<!-- all js here -->
<!-- jquery latest version -->

<!-- bootstrap js -->
<script type="text/javascript" src={{ asset("js/cecam/bootstrap.min.js") }}></script>
<!-- owl.carousel.2.0.0-beta.2.4 css -->
<script type="text/javascript" src={{ asset("js/cecam/owl.carousel.min.js") }}></script>
<!-- jquery countdown min js -->
<script type="text/javascript" src={{ asset("js/cecam/jquery.countdown.min.js") }}></script>
<!-- meanmenu js -->
<script type="text/javascript" src={{ asset("js/cecam/jquery.meanmenu.js") }}></script>
<!-- jquery-ui min js -->
<script type="text/javascript" src={{ asset("js/cecam/jquery-ui.min.js") }}></script>
<!-- plugins js -->
<script type="text/javascript" src={{ asset("js/cecam/plugins.js") }}></script>
<!-- main js -->
<script type="text/javascript" src={{ asset("js/cecam/main.js") }}></script>


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
