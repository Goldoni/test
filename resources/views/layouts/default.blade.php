<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
@include('layouts.header')

@yield('body', '<body class="navbar-top">')

@include('layouts.navbar')



<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        @include('layouts.sidebar')


        <!-- Main content -->
        <div class="content-wrapper">

            @include('layouts.page_header')


            <!-- Content area -->
            <div class="content">

                @yield('content')


                @include('layouts.footer')

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
