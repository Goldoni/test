<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('title')


    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/icons/icomoon/styles.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/bootstrap.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/core.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/components.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/colors.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/custom.css") }} rel="stylesheet" type="text/css">
    @yield('css')
    <!-- /global stylesheets -->

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>;
        window.locale = <?php echo json_encode([
                'lang' => App::getLocale(),
        ]); ?>
    </script>

</head>