<!-- footer-area start-->
<footer class="footer-area bg-1">
    <div class="footer-top ptb-60">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-wrap">
                        <div class="footer-logo mb-20">
                            <!-- <img src="img/logo/logo1.png" alt="" /> -->
                            <h1>CECAM HH</h1>
                        </div>
                        <p>Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V <br> Vereinsregister VR 23226  - Amtsgericht Hamburg)</p>
                        <div class="fb-follow" data-href="https://www.facebook.com/CECAM-HH-ev-1812024129118496/" data-layout="button_count" data-size="small" data-show-faces="true"></div>
                        <form action="#">
                            <input type="email" placeholder="email"/>
                            <button>Subscribe Now</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4  col-xs-12">
                    <div class="footer-wrap">
                        <h2 class="footer-title">TOP EVENTS </h2>
                        <div class="single-footer-text">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                        <div class="single-footer-text mtb-20">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                        <div class="single-footer-text">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4  col-xs-12">
                    <div class="footer-wrap">
                        <h2 class="footer-title">CECAM SPONSORS</h2>
                        <div class="footer-img clear">
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area ptb-40">
        <div class="container">
            <div class="row">
                <div class=" col-sm-6 col-xs-12">
                    <p>&copy; 2017. All rights reserved. </p>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <p>Designed & Developed by CECAM-TECH</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area end-->