<div class="header-bottom-area">
    <div class="container ">
        <div class="row">
            <div class="col-md-3 col-xs-7">
                <div class="logo mt-20">
                    <a href="index.html">
                        <img src="{{ asset("img/cecam/logo/cecam.jpeg") }}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-9  hidden-sm hidden-xs">
                <div class="mainmenu text-right">
                    <nav>
                        <ul>
                            <li class="active"><a href="{{ route('home') }}"><i class="fa fa-home" aria-hidden="true"></i> home</a> </li>
                            <li><a href="{{ route('about') }}">über uns</a></li>
                            <li><a href="#">Fachausschhuss</a>
                                <ul>
                                    <li><a href="{{ route('culture') }}">Kultur</a></li>
                                    <li><a href="{{ route('sport') }}">Sport</a></li>
                                    <li><a href="{{ route('tech') }}">Technologie Studium und Berufsausbildung</a></li>
                                    <li><a href="{{ route('marketing') }}">Marketing</a></li>
                                    <li><a href="{{ route('ethic') }}">Ethik</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('events') }}">Events</a></li>
                            <li><a href="{{ route('contact') }}">kontakte</a></li>
                            <li><a href="{{ route('impressum') }}">Impressum</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-xs-5 hidden-md hidden-lg">
                <div class="mobile-menu-area">
                    <div class="mobile-menu">
                        <div class="mobile-menu-active">
                            <nav>
                                <ul>
                                    <li class="active"><a href="{{ route('home') }}"><i class="fa fa-home" aria-hidden="true"></i> home</a> </li>
                                    <li><a href="{{ route('about') }}">über uns</a></li>
                                    <li><a href="#">Fachausschhuss</a>
                                        <ul>
                                            <li><a href="{{ route('culture') }}">Kultur</a></li>
                                            <li><a href="{{ route('sport') }}">Sport</a></li>
                                            <li><a href="{{ route('tech') }}">Technologie Studium und Berufsausbildung</a></li>
                                            <li><a href="{{ route('marketing') }}">Marketing</a></li>
                                            <li><a href="{{ route('ethic') }}">Ethik</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('events') }}">Events</a></li>
                                    <li><a href="{{ route('contact') }}">kontakte</a></li>
                                    <li><a href="{{ route('impressum') }}">Impressum</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>