<!-- slider start -->
<div class="slider-area slide-img-1">
    <div class="container">
        <div class="slider-wrap overlay">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-active">
                        <div class="single-slider text-center ptb-30">
                            <h1>Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V<br />  </h1>
                            <h2>Vereinsregister VR 23226  - Amtsgericht Hamburg</h2>
                            <a href="{{ asset('uploads/download/Satzung_CECAM_HH_10_03_2017.pdf') }}">Satzung des Vereins</a>
                        </div>
                        <div class="single-slider text-center ptb-30">
                            <h1>Name, Sitz, Rechtform und Geschäftsjahr <br /></h1>
                            <h4>(1) Der Verein führt den Namen: „Civil Engagement of Cameroonians in Hamburg“(Kurz:CECAM HH).  <br>
                                (2) Der Verein hat seinen Sitz in Hamburg und soll beim Amtsgericht Hamburg<br>
                                eingetragen werden; er soll dann den Zusatz „e. V” tragen.<br>
                                (3) Das Geschäftsjahr ist das Kalenderjahr.</h4>
                            <a href="{{ asset('uploads/download/Satzung_CECAM_HH_10_03_2017.pdf') }}">Satzung des Vereins</a>
                        </div>
                        <div class="single-slider text-center ptb-30">
                            <h1>Mitgliedschaft <br /> </h1>
                            <h4>(1) Der Verein besteht aus ordentlichen Mitgliedern und Ehrenmitglieder.  <br>
                                (2) Ordentliche Mitglieder können natürliche und juristische Personen sein. Natürliche
                                Personen jedoch nur soweit sie das 16. Lebensjahr vollendet haben.<br></h4>
                            <a href="{{ asset('uploads/download/Satzung_CECAM_HH_10_03_2017.pdf') }}">Satzung des Vereins</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider end -->