<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed">
    <div class="sidebar-content">


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion navigation-bordered">

                    <!-- Main -->
                    <li class="navigation-header"><span>@lang('menus.menus')</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li><a href="{{ route('home') }}"><i class="icon-location3 position-left"></i> @lang('menus.site')</a></li>
                    <li><a href="{{ route('admin.pages') }}"><i class="icon-folder-open2 position-left"></i> @lang('menus.pages')</a></li>
                    <li><a href="{{ route('admin.posts') }}"><i class="icon-calendar3 position-left"></i> @lang('menus.events')<span class="badge bg-primary-800 badge-inline position-right">26</span></a></li>
                    <!-- /main -->

                    <!-- Forms -->
                    <li class="navigation-header"><span>Statistics</span> <i class="icon-menu" title="Forms"></i></li>
                    <li>
                        <a href="#"><i class="icon-user-cancel"></i> <span>@lang('menus.administration')</span></a>
                        <ul>
                            <li><a href="{{ route('admin.pages') }}"><i class="icon-stack2"></i> Pages Management</a></li>
                            <li><a href="{{ route('admin.users') }}"><i class="icon-collaboration"></i> Users Management</a></li>
                            <li><a href="{{ route('admin.boards') }}"><i class="icon-users4"></i> Boards Management</a></li>
                            <li><a href="{{ route('admin.boards') }}"><i class="icon-cube3"></i> Language Management</a></li>
                        </ul>
                    </li>
                    <!-- /forms -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->