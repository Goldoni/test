<!-- Core JS files -->
<script type="text/javascript" src={{ asset("assets/js/plugins/loaders/pace.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/core/libraries/jquery.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/core/libraries/jquery_ui/widgets.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/core/libraries/bootstrap.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/loaders/blockui.min.js") }}></script>
<!-- /core JS files -->

<!-- Theme JS files -->

<script type="text/javascript" src={{ asset("assets/js/plugins/forms/styling/switchery.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/forms/selects/select2.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/forms/styling/uniform.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/forms/selects/bootstrap_multiselect.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/pickers/daterangepicker.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/ui/nicescroll.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/ui/fab.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/ui/prism.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/buttons/hover_dropdown.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/notifications/pnotify.min.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/extensions/contextmenu.js") }}></script>
<script type="text/javascript" src={{ asset("messages.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/plugins/forms/inputs/duallistbox.min.js") }}></script>

@yield('js_before')
<script type="text/javascript" src={{ asset("assets/js/core/app.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/pages/layout_fixed_custom.js") }}></script>
<script type="text/javascript" src={{ asset("assets/js/pages/form_layouts.js") }}></script>
@yield('js_after')
<!-- /theme JS files -->
<!-- Footer -->
<div class="navbar navbar-default navbar-fixed-bottom">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second">
        <div class="navbar-text">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">Help center</a></li>
                <li><a href="#">Policy</a></li>
                <li><a href="#" class="text-semibold">Upgrade your account</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-cog3"></i>
                        <span class="visible-xs-inline-block position-right">Settings</span>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-dribbble3"></i> Dribbble</a></li>
                        <li><a href="#"><i class="icon-pinterest2"></i> Pinterest</a></li>
                        <li><a href="#"><i class="icon-github"></i> Github</a></li>
                        <li><a href="#"><i class="icon-stackoverflow"></i> Stack Overflow</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /footer -->