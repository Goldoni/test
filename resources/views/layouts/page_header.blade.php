<!-- Page header -->
<div class="page-header page-header-inverse has-cover">
    @yield('page-header-content')
    <div class="breadcrumb-line bg-teal-800">
        @yield('breadcrumb')
        <ul class="breadcrumb-elements">
            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Contact</a></li>
            <li><a href="{{ route('pricing') }}"><i class="icon-price-tags position-left"></i> Abonnement</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->