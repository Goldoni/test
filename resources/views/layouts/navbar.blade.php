<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html">ADMIN CECAM PANEL</a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            @yield('sidebar-mobile-detached-toggle')
        </ul>
    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            @yield('sidebar-mobile-detached-toggle-link')
            <li><a href="{{ url('/') }}"><i class="icon-location3 position-left"></i> @lang('menus.site')</a></li>
            <li><a href="{{ route('admin.pages') }}"><i class="icon-folder-open2 position-left"></i> @lang('menus.pages')</a></li>
            <li><a href="{{ route('admin.posts') }}"><i class="icon-calendar3 position-left"></i> @lang('menus.events')<span class="badge bg-primary-800 badge-inline position-right">26</span></a></li>
            @can('isAdministrator', App\User::class)
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                    <i class=" icon-user-cancel position-left"></i> @lang('menus.administration') <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-left">
                    <li><a href="{{ route('admin.pages') }}"><i class="icon-stack2"></i> Pages Management</a></li>
                    <li><a href="{{ route('admin.users') }}"><i class="icon-collaboration"></i> Users Management</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('admin.boards') }}"><i class=" icon-users4"></i> Boards Management</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('admin.boards') }}"><i class="icon-cube3"></i> Language Management</a></li>
                </ul>
            </li>
            @endcan
        </ul>

        <ul class="nav navbar-nav navbar-right">
            @include('language')

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                    <i class="icon-bell2"></i>
                    <span class="visible-xs-inline-block position-right">Messages</span>
                    <span class="badge bg-warning-400">2</span>
                </a>

                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-heading">
                        Messages
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-compose"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body">
                        <li class="media">
                            <div class="media-left">
                                <img src={{ asset("assets/images/placeholder.jpg")}} class="img-circle img-sm" alt="">
                                <span class="badge bg-danger-400 media-badge">5</span>
                            </div>

                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">James Alexander</span>
                                    <span class="media-annotation pull-right">04:58</span>
                                </a>

                                <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <img src={{ asset("assets/images/placeholder.jpg")}} class="img-circle img-sm" alt="">
                                <span class="badge bg-danger-400 media-badge">4</span>
                            </div>

                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">Margo Baker</span>
                                    <span class="media-annotation pull-right">12:16</span>
                                </a>

                                <span class="text-muted">That was something he was unable to do because...</span>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left"><img src={{ asset("assets/images/placeholder.jpg")}} class="img-circle img-sm" alt=""></div>
                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">Jeremy Victorino</span>
                                    <span class="media-annotation pull-right">22:48</span>
                                </a>

                                <span class="text-muted">But that would be extremely strained and suspicious...</span>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left"><img src={{ asset("assets/images/placeholder.jpg") }} class="img-circle img-sm" alt=""></div>
                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">Beatrix Diaz</span>
                                    <span class="media-annotation pull-right">Tue</span>
                                </a>

                                <span class="text-muted">What a strenuous career it is that I've chosen...</span>
                            </div>
                        </li>
                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                    <img src={{ asset("assets/images/placeholder.jpg")}} alt="">
                    <span>{{ Auth::user()->full_name }}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('profile') }}"><i class="icon-user-plus"></i> @lang('menus.profile')</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('calendar') }}"><i class="icon-calendar3"></i> @lang('menus.calendar')</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('tasks') }}"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-clipboard6"></i> @lang('menus.tasks')</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('notification') }}"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i>  @lang('menus.notifications')</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-lock"></i> @lang('menus.lock')</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->