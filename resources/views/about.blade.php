@extends('layouts.cecam')

@section('title')
	<title>CECAM | About</title>
@endsection

@section('content')
	<div class="col-md-12">

		<div class="about-content">

			<h2 class="section-title">Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V
				(Vereinsregister VR 23226  - Amtsgericht Hamburg)</h2>

			Lorem ipsum dolor sit amet, mei cu sale accusamus definiebas, mel ut soluta labores recusabo. Ei vis aeterno epicuri electram. Ut enim hinc salutatus has, agam appareat eleifend et per, pertinacia comprehensam nec et. Luptatum conceptam has cu, ad nam solet persius consequuntur.

			Omnis phaedrum te vis, cum ne hinc definitiones, sea no modus verterem assueverit. Id case melius sed. Ut per nibh dicta. Appareat voluptatum sit an, eirmod bonorum expetendis mei eu, mei ornatus denique platonem ut. No etiam expetendis sed, dicta tamquam noluisse ius an.

			Nulla graeco vidisse has ex, sed ne fierent albucius percipitur. Vix te munere percipitur, vim natum forensibus conclusionemque ea, sit an ignota vivendo forensibus. Usu ea quod consul, probo dolorum fastidii ne nec. Eu dicant feugiat inermis per, alia phaedrum eum ei.


			<h2 class="section-title">Fachausschüsse</h2>

			<ul>
				<li><i class="fa fa-check"></i>Fachausschuss für  Familie, Jugendarbeit und Sport</li>
				<li><i class="fa fa-check"></i>Fachausschuss für Presse/Öffentlichkeitsarbeit</li>
				<li><i class="fa fa-check"></i>Fachausschuss für Technologie, Studium und Berufsausbildung</li>
				<li><i class="fa fa-check"></i>Fachausschuss für  Business, Marketing und Events</li>
			</ul>

			<p class="text-justify">
				Cu vix doctus aperiri feugiat, sed ne commodo reformidans, nam omnes graeci honestatis ex. Ex solet conclusionemque quo, ei enim scaevola has, debitis denique tincidunt cu eos. Sit constituto assueverit cu. Vix ut enim tibique, sint appetere nec et. Veri malis persecuti in vim, at nullam pertinacia sea, tamquam oporteat duo in.

				Graeco persecuti no eum. In tamquam prodesset tincidunt mea. Eos iudicabit maiestatis interesset et, mei ei verear molestiae, cu pro summo tation habemus. No vis laudem ceteros honestatis. Ex per modus sanctus scriptorem, est blandit ullamcorper no, offendit phaedrum qui ad. Idque atqui prodesset eam no.

				Ullum invenire iracundia sea in. Vis porro affert dolorum an, diam quando mei ei, in has nibh tempor. Mea amet agam maluisset ne, sea ad tibique tractatos delicatissimi, ridens quaestio no quo. Velit iusto postea vix ea. Id audire efficiantur has, primis civibus appareat vel eu. Probo saperet his id, vel ne partiendo scripserit.

				Cu dolore perpetua nec, eos sensibus democritum an, nibh congue veniam et sea. Ad unum verterem ius, ad his doming accusata. Duo an augue falli probatus, cu duo suscipiantur concludaturque. Cu sea iisque cotidieque, ceteros pericula ut mei. Eos ex elit consequat deseruisse, no feugait partiendo mea.</p>
			<div class="about-img mb-70">
				<img src="{{ asset('img/cecam/about/about.jpeg') }}" alt="" />
			</div>
			<p class="text-justify">
				Per vidisse suavitate no, ius habeo constituto quaerendum an. Ad facete sanctus deterruisset per, utamur facilis delectus ad vim. Per no mundi possim, eu etiam decore detracto nam. Nisl sumo in cum. Amet recteque sapientem per ne, ius nonumy gloriatur deterruisset eu, quo agam inermis partiendo in.

				Te his suscipit tincidunt, at autem tibique inimicus quo. Per no cibo menandri suavitate. Augue torquatos id vis, placerat praesent has ea. Eius adhuc no sea, affert ignota in ius. Ut vix cibo minimum vivendum, cu simul accusata gubergren sed. Et accusam deseruisse vis.

				Ut vim omnes possit dolores, postulant ullamcorper ad ius. No menandri assueverit mel, ex per velit copiosae, cum ex mundi apeirian scriptorem. Vel ei velit persius, ad eam aliquam insolens interesset, at minim percipit assentior vim. Eum id vidisse salutandi forensibus, te has definiebas efficiantur. Et purto fierent vix, sea fugit iisque maiorum et, illum congue integre vis eu.

				Has et putent equidem nostrum, diam insolens pertinacia ex est. Eam falli dolorem et, vel putant inermis iracundia te. Vel scaevola dissentiunt te, meis labore accusamus an est, quot possit ne sed. Vix in solet gubergren consequat. Ius verterem sensibus cu. Duo eu etiam argumentum, ad vocent verear virtute his.

				Vix at nihil eirmod delenit. In sea debet voluptatum. Quaeque ocurreret dissentiunt an vel, cu eos falli propriae delectus. Sit prima facete latine ex. Vix quodsi melius latine at, forensibus disputando in has. Accusam offendit salutandi id eos, sea habeo scripserit no, mucius nusquam eu vim.

				In eam tation verear conclusionemque, esse maluisset his ad. Minimum vulputate per ea, choro vituperatoribus eu mea. Movet possim euripidis mea ne, oratio alienum nam ex. Quo an debet gloriatur. Ubique vituperata in ius, vidit nusquam nostrum mea te, ne est prima dicit periculis.

				Cum probo summo te, quando gubergren posidonium at mea. Pro at discere omittantur, at appetere iracundia his. Sit movet everti imperdiet cu, has ex posse ipsum animal. Mei ut option audire, vis ad viris perpetua.

				Alia voluptua at sea, pri at suas aeque probatus. Laoreet qualisque scribentur no sea, te vix perpetua accusamus. Ea eum equidem repudiandae comprehensam. Et assum labore ius, at qui purto ocurreret, vel vide putant id. Ius posse detraxit appellantur at, vim at quas gloriatur.

				Labore debitis moderatius vim te, phaedrum deserunt pertinax et mel. At electram torquatos tincidunt ius, ei porro lobortis nec, agam erant verear mea et. Nec at impedit vocibus, ius latine oporteat consequuntur ei. Quod mundi labores nec ne. Ad sed solum facilisi, has suscipit referrentur ei, eam hinc tractatos liberavisse an.

				Mei vitae euripidis an. Id mea aperiam persius, ex affert appetere scribentur duo. Cum quot definiebas in, ad congue deleniti persequeris vix. Quo no simul labore. Et vide omnium vim, ius mundi diceret mandamus ei, ea nihil exerci ius.

				Movet necessitatibus te nec. Et sea accusam scriptorem. Id dico complectitur pro, causae percipit sit no. Quem nemore eos ex, an novum fuisset eam.

				Mei tempor mediocritatem te. Vel cibo incorrupte theophrastus cu, ea nusquam accusam invidunt eos, doming malorum ad vis. Error bonorum principes te mel, no eum fugit dolor aliquando. Porro quidam sanctus ne vix, habemus alienum denique qui te. Quod magna everti vis id.

				Vix et facer denique, no quo essent dissentias. Et usu appetere principes, nibh summo facilisis sea ei, duo at labitur diceret omnesque. Utamur repudiandae ius no. Has sonet meliore platonem ad, cum causae hendrerit et, eleifend assentior te nam.

				Ea mel periculis sadipscing contentiones. Tempor indoctum eu qui, euripidis constituto conclusionemque nec ei. Per id vidit saperet. Usu eu errem essent feugait, noluisse consetetur id duo. In liber patrioque scripserit qui, pri ne perfecto constituam.

				Nec ex offendit splendide, has at facer nostro omittantur, minim omnium iudicabit te cum. Pro commodo aeterno explicari in. Mei ex nostrud appetere principes. Vim iudico referrentur at, ea mei mundi platonem. Usu quaeque dissentiet in. Per timeam discere detraxit at, offendit splendide forensibus nam in. Utamur eruditi vel ea, ut cum accusata efficiendi disputando, modus sapientem repudiandae ea his.

				His causae civibus in, ea usu possit voluptatum. Ius ad brute.
			</p>


		</div>
	</div>
@endsection

@section('sidebar')

@endsection