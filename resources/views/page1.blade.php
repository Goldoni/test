<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>H4K | Login</title>

    <!-- Styles -->

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/icons/icomoon/styles.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/bootstrap.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/core.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/components.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/colors.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/custom.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/theme.css") }} rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src={{ asset("assets/js/plugins/loaders/pace.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/core/libraries/jquery.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/core/libraries/bootstrap.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/plugins/loaders/blockui.min.js") }}></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src={{ asset("assets/js/plugins/forms/styling/uniform.min.js") }}></script>

    <script type="text/javascript" src={{ asset("assets/js/core/app.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/pages/login.js") }}></script>
    <!-- /theme JS files -->

</head>

<body class="login-container login-cover">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content pb-20">

                <!-- Form with validation -->
                <form role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                            <h5 class="content-group">Login to your account</h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control"  placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            @if ($errors->has('email'))
                                <label class="validation-error-label" for="email-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </label>
                            @endif
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" required="required">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked="checked"  name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Remember
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="{{ route('password.request') }}">Forgot password?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-teal btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
                        </div>

                        <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
                        <a href="{{ url('/register') }}" class="btn bg-teal-800 btn-block content-group">Sign up</a>
                    </div>
                </form>
                <!-- /form with validation -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
