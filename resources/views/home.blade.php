@extends('layouts.cecam')

@section('title')
    <title>CECAM | HAMBURG</title>
@endsection

@section('content')
    @foreach ($posts as $post)
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="blog-wrap home7-blog-wrap mb-70">
                <div class="blog-img overlay">
                    @if( isset($post->attachments[0]->url['origin']))
                    <img src="{{ $post->attachments[0]->url['sm'] }}" alt="" />
                    @else
                        <img src="http://lorempixel.com/360/360/nature" alt="" />
                    @endif
                    <a href="{{ url('/post') }}"><i class="fa fa-file-image-o"></i></a>
                </div>
                <div class="blog-info pb-30">
                    <div class="blog-meta">
                        <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                        <span><a href="#">By {{ isset($post->user->full_name) ? $post->user->full_name : '' }}</a></span>
                        <span><i class="fa fa-eye"></i></span>
                        <span>255</span>
                    </div>
                    <h2><a href="#">{{ $post->name }}</a></h2>
                    <p>{!!  substr($post->content, 0, 300) !!}</p>
                    <span class="date">
                        <span>12</span>
                        <span class="month">Nov</span>
                    </span>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row">
        <div class="col-lg-12">
            <div class="blog-others floatright mt-20">
                <ul>
                    @for ($i = 0; $i <  $grid['pagination']['nbPage']; $i++)
                        <li><a href="#">{{ $i }}</a></li>
                    @endfor
                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')

@endsection