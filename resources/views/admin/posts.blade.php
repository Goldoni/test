@extends('layouts.app')

@section('title')
    <title>H4K | @lang('menus.my_kinds')</title>
@endsection
@section('css')
    <link href={{ asset("../assets/global/css/components.css") }} rel="stylesheet" type="text/css">
@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/wizards/steps.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/media/fancybox.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/ecommerce_product_list.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/user/kid/kid.js") }}"></script>
@endsection
@section('sidebar-mobile-detached-toggle')
    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
@endsection
@section('sidebar-mobile-detached-toggle-link')
    <li> <a class="sidebar-control sidebar-detached-hide hidden-xs">  <i class="icon-drag-right"></i> </a> </li>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> @lang('menus.home')</a></li>
        <li class="active">@lang('menus.my_kinds')</li>
    </ul>
@endsection
@section('content')
    <div id="vueKids" v-cloak>
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached"><span class="hidden">{{ $i = 1 }}</span>
                @foreach (json_decode($posts) as $post)
                    @if( ($i % 2) === 1)
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- Mini size -->
                                <div class="panel panel-flat blog-horizontal blog-horizontal-3 blog-horizontal-sm">
                                    <div class="panel-body">
                                        <div class="thumb">
                                            <img src="{{ asset("assets/images/placeholder.jpg") }}" alt="" class="img-responsive">
                                            <div class="caption-overflow">
                                                    <span>
                                                        <a href="{{ route('admin.posts.single', ['id' => $post->id])}}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                                    </span>
                                            </div>
                                        </div>

                                        <div class="blog-preview">
                                            <div class="content-group-sm">
                                                <h6 class="blog-title text-semibold">
                                                    <a href="#">{{ $post->name }}</a>
                                                    <a href="javascript:;" class="pull-right text-success"><i class="icon-share3"></i> Share</a>
                                                </h6>
                                                <ul class="list-inline list-inline-separate">
                                                    <li>By <a href="#" data-popup="tooltip" title="{{ isset($post->user->full_name) ? $post->user->full_name : "H4K" }}">{{ isset($post->user->full_name) ? $post->user->full_name : "H4K"}}</a></li>
                                                    <li data-popup="tooltip" title="{{ $post->created_at }}">{{ $post->created_at }}</li>
                                                </ul>
                                                <ul class="list-inline list-inline-condensed heading-text">
                                                    <li><a href="#"><span class="label bg-teal-300">Image</span></a></li>
                                                    <li><a href="#"><span class="label bg-teal-300">Blog</span></a></li>
                                                    <li><a href="#"><span class="label bg-teal-300">Post</span></a></li>
                                                </ul>
                                            </div>
                                            <p class="text-justify">{{ mb_substr($post->content,0,550) }} <a href="#">[...]</a></p>
                                        </div>
                                    </div>
                                    <div class="panel-footer panel-footer-transparent">
                                        <div class="heading-elements">
                                            <ul class="list-inline list-inline-separate heading-text">
                                                <li role="button" data-popup="tooltip" title="{{ $post->comments_count }} Comments"><i class="icon-comment position-left"></i> {{ $post->comments_count }}</li>
                                                <li><i class="icon-users position-left"></i> 67</li>
                                                <li><a href="#" class="text-default"><i class="icon-facebook text-primary position-left"></i> 1,489</a></li>
                                                <li><a href="#" class="text-default"><i class="icon-twitter text-info position-left"></i> 1,489</a></li>
                                                <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                            </ul>

                                            <a href="#" class="heading-text pull-right">Read More <i class="icon-arrow-right14 position-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /mini size -->
                            </div>
                        @else
                            <div class="col-lg-6">
                                <!-- Mini size -->
                                <div class="panel panel-flat blog-horizontal blog-horizontal-3 blog-horizontal-sm">
                                    <div class="panel-body">
                                        <div class="thumb">
                                            <img src="{{ asset("assets/images/placeholder.jpg") }}" alt="" class="img-responsive">
                                            <div class="caption-overflow">
                                                <span>
                                                    <a href="{{ route('admin.posts.single', ['id' => $post->id])}}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="blog-preview">
                                            <div class="content-group-sm">
                                                <h6 class="blog-title text-semibold">
                                                    <a href="#">{{ $post->name }}</a>
                                                    <a href="javascript:;" class="pull-right text-success"><i class="icon-share3"></i> Share</a>
                                                </h6>
                                                <ul class="list-inline list-inline-separate">
                                                    <li>By <a href="#" data-popup="tooltip" title="{{ isset($post->user->full_name) ? $post->user->full_name : "H4K" }}">{{ isset($post->user->full_name) ? $post->user->full_name : "H4K"}}</a></li>
                                                    <li data-popup="tooltip" title="{{ $post->created_at }}">{{ $post->created_at }}</li>
                                                </ul>
                                                <ul class="list-inline list-inline-condensed heading-text">
                                                    <li><a href="#"><span class="label bg-teal-300">Image</span></a></li>
                                                    <li><a href="#"><span class="label bg-teal-300">Blog</span></a></li>
                                                    <li><a href="#"><span class="label bg-teal-300">Post</span></a></li>
                                                </ul>

                                            </div>



                                            <p class="text-justify">{{ mb_substr($post->content,0,500) }} <a href="#">[...]</a></p>
                                        </div>
                                    </div>
                                    <div class="panel-footer panel-footer-condensed">
                                        <div class="heading-elements">
                                            <ul class="list-inline list-inline-separate heading-text">
                                                <li role="button" data-popup="tooltip" title="{{ $post->comments_count }} Comments"><i class="icon-comment position-left"></i> {{ $post->comments_count }}</li>
                                                <li><i class="icon-users position-left"></i> 67</li>
                                                <li><i class="icon-facebook text-primary position-left"></i> 18</li>
                                            </ul>

                                            <a href="#" class="heading-text pull-right">Read More <i class="icon-arrow-right14 position-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /mini size -->
                            </div>
                        </div>
                        @endif
                        <span class="hidden">{{ $i ++ }}</span>
                @endforeach
                        <!-- List -->
                <div class="row">
                        <div class="col-lg-6">
                            <!-- Clean blog layout #2 -->
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="content-group">
                                        <h5 class="blog-title text-semibold" style="margin-top: 0; margin-bottom: 5px;">
                                            <a href="#" class="text-default">Blog post layout #3 with image</a>
                                        </h5>
                                        <ul class="list-inline list-inline-separate">
                                            <li>By <a href="#">Eugene</a></li>
                                            <li>July 5th, 2016</li>
                                            <li><a href="#">12 comments</a></li>
                                            <li><a href="#"><i class="icon-heart6 text-size-base text-pink position-left"></i> 281</a></li>
                                        </ul>
                                        <ul class="list-inline list-inline-condensed heading-text">
                                            <li><a href="#"><span class="label bg-teal-300">Image</span></a></li>
                                            <li><a href="#"><span class="label bg-teal-300">Blog</span></a></li>
                                            <li><a href="#"><span class="label bg-teal-300">Post</span></a></li>
                                        </ul>
                                    </div>
                                    <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.</p>
                                    <blockquote>
                                        <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra.</p>
                                        <footer>Jason, <cite title="Source Title">10:39 am</cite></footer>
                                    </blockquote>
                                </div>

                                <div class="panel-footer panel-footer-condensed">
                                    <div class="heading-elements">
                                        <ul class="list-inline list-inline-separate heading-text">
                                            <li><i class="icon-users position-left"></i> 67</li>
                                            <li><i class="icon-alarm position-left"></i> 80 hours</li>
                                            <li>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                                <span class="text-muted position-right">(59)</span>
                                            </li>
                                        </ul>

                                        <a href="#" class="heading-text pull-right">Read more <i class="icon-arrow-right14 position-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /clean blog layout #2 -->
                        </div>

                        <div class="col-lg-6">
                            <!-- Clean blog layout #2 -->
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="content-group">
                                        <h5 class="blog-title text-semibold" style="margin-top: 0; margin-bottom: 5px;">
                                            <a href="#" class="text-default">Blog post layout #3 with image</a>
                                        </h5>
                                        <ul class="list-inline list-inline-separate">
                                            <li>By <a href="#">Eugene</a></li>
                                            <li>July 5th, 2016</li>
                                            <li><a href="#">12 comments</a></li>
                                            <li><a href="#"><i class="icon-heart6 text-size-base text-pink position-left"></i> 281</a></li>
                                        </ul>
                                        <ul class="list-inline list-inline-condensed heading-text">
                                            <li><a href="#"><span class="label bg-teal-300">Image</span></a></li>
                                            <li><a href="#"><span class="label bg-teal-300">Blog</span></a></li>
                                            <li><a href="#"><span class="label bg-teal-300">Post</span></a></li>
                                        </ul>
                                    </div>

                                    <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.</p>

                                    <blockquote>
                                        <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra.</p>
                                        <footer>Jason, <cite title="Source Title">10:39 am</cite></footer>
                                    </blockquote>
                                </div>

                                <div class="panel-footer panel-footer-condensed">
                                    <div class="heading-elements">
                                        <ul class="list-inline list-inline-separate heading-text">
                                            <li><i class="icon-users position-left"></i> 67</li>
                                            <li><i class="icon-alarm position-left"></i> 80 hours</li>
                                            <li>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                                <i class="icon-star-half text-size-base text-warning-300"></i>
                                                <span class="text-muted position-right">(59)</span>
                                            </li>
                                        </ul>

                                        <a href="#" class="heading-text pull-right">Read more <i class="icon-arrow-right14 position-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- /clean blog layout #2 -->
                        </div>
                    </div>
                <!-- /list -->


                <!-- Pagination -->
                <div class="text-center content-group-lg pt-20">
                    <ul class="pagination">
                        <li class="disabled"><a href="#"><i class="icon-arrow-small-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><i class="icon-arrow-small-right"></i></a></li>
                    </ul>
                </div>
                <!-- /pagination -->

            </div>
        </div>
        <!-- /detached content -->


        <!-- Detached sidebar -->
        <div class="sidebar-detached">
            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- Categories -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Categories</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <div class="has-feedback has-feedback-left form-group">
                                <input type="search" class="form-control" placeholder="Search...">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-small text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation">
                                @foreach (json_decode($categories) as $category)
                                    <li>
                                        <a href="#"> <span class="text-muted text-size-small text-regular pull-right">{{ count($category->posts) }}</span> <i class="icon-certificate"></i>
                                            {{ $category->name }}
                                        </a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <!-- /categories -->

                    <!-- Recent comments -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Recent comments</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">James Alexander</span>
                                        </a>

                                        <span class="text-muted">Who knows, maybe that...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Margo Baker</span>
                                        </a>

                                        <span class="text-muted">That was something he...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Jeremy Victorino</span>
                                        </a>

                                        <span class="text-muted">But that would be...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Beatrix Diaz</span>
                                        </a>

                                        <span class="text-muted">What a strenuous career...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Richard Vango</span>
                                        </a>

                                        <span class="text-muted">Other travelling salesmen...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /recent comments -->

                    <!-- Archive -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Archive</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation">
                                <li><a href="#">January 2017</a></li>
                                <li><a href="#">December 2016</a></li>
                                <li><a href="#">November 2016</a></li>
                                <li><a href="#">October 2016</a></li>
                                <li><a href="#">September 2016</a></li>
                                <li><a href="#">August 2016</a></li>
                                <li><a href="#">July 2016</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /archive -->



                </div>
            </div>
        </div>
        <!-- /detached sidebar -->
    </div>



@endsection
