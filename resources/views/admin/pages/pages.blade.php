@extends('layouts.app')

@section('title')
    <title>CECAM | Administration</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/datatables.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/responsive.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/buttons.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/bootstrap_select.min.js") }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/datatables_extension_buttons_html5.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/users_customers.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/admin/pages/pages.js") }}"></script>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="user_profile_tabbed.html">Admin</a></li>
        <li class="active">Pages</li>
    </ul>
@endsection
@section('content')
    <div id="vueUsers" class="row">
        @include('admin/modal/pages_dlgs')
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Pages Manager</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

            </div>

            <table class="table datatable-responsive-column-controlled">
                <thead>
                <tr>
                    <th></th>
                    <th>id</th>
                    <th>titre</th>
                    <th>content</th>
                    <th>Type</th>
                    <th>created_at</th>
                    <th>updated_at</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach (json_decode($pages) as $page)
                    <tr class="context-dynamic" data-id="{{ $page->id }}">
                        <td></td>
                        <td><h6 class="no-margin text-semibold">{{ $page->id }}</h6></td>
                        <td><a href="javascript:;" data-popup="tooltip" title="{{ $page->title }}" @click="edit({{ $page->id }})">{{ $page->title }}</a></td>
                        <td>{{ substr($page->content , 0, 150).'...'}}</td>
                        <td><a href="javascript:;" data-popup="tooltip" title="{{ $page->type }}" @click="edit({{ $page->id }})">{{ $page->type }}</a></td>
                        <td> <span data-popup="tooltip" title="{{ $page->created_at }}" role="button" >{{ $page->created_at }}</span></td>
                        <td> <span data-popup="tooltip" title="{{ $page->updated_at }}" role="button" >{{ $page->updated_at }}</span></td>

                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a @click="edit({{ $page->id }})" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="context-dynamic-menu">
                <ul class="dropdown-menu">
                    <li><a href="javascript:;"><i class="icon-pencil6"></i> Edit</a></li>
                </ul>
            </div>

        </div>
    </div>



@endsection
