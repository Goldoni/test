<!-- Large modal -->
<div id="modal_large" class="modal fade">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="#" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
        <div class="modal-content">
            <div class="modal-header bg-teal-800">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"><i class="icon-stack3"></i> &nbsp;Board Manager</h5>
            </div>
            <br>
            <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                <li class="active"><a href="#highlight-tab1" data-toggle="tab"><i class="icon-cube3 position-left"></i> Board</a></li>
                <li><a href="#highlight-tab2" data-toggle="tab"><i class="icon-users4 position-left"></i> @lang('labels.members')</a></li>
                <li><a href="#highlight-tab3" data-toggle="tab"><i class="icon-collaboration position-left"></i> Board History</a></li>
            </ul>
            <!-- Tab-content -->
            <div class="tab-content">
                <div class="tab-pane active" id="highlight-tab1">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">Enter your information</legend>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">@lang('labels.id') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <p type="text" class="form-control input-xs" v-text="board.id"></p>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('name') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.board') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control input-xs" id="name" name="name" v-model="board.name" required>
                                    <label class="validation-error-label" for="name-error" v-if="form.errors.has('name')">
                                        <strong v-text="form.errors.get('name')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('description') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.description') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <textarea rows="3" cols="5" class="form-control input-xs" id="description"  name="description" v-model="board.description"></textarea>
                                    <label class="validation-error-label" for="description-error" v-if="form.errors.has('description')">
                                        <strong v-text="form.errors.get('description')"></strong>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="tab-pane" id="highlight-tab2">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">Groupe D'utilisateurs</legend>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">@lang('labels.members') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <select multiple="multiple" id="users" class="form-control" name="users[]" multiple>
                                        @foreach (json_decode($users) as $user)
                                            <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>
                        </fieldset>
                    </div>
                </div>

                <div class="tab-pane" id="highlight-tab3">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">User History</legend>
                            <div class="table-responsive bordered">
                                <table class="table table-xxs">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Last Edited</th>
                                        <th>User</th>
                                        <th>Activity</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Eugene</td>
                                        <td>
                                            <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                            <br>
                                            <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                            <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                        </td>
                                        <td>@Kopyov</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Victoria</td>
                                        <td>Baker</td>
                                        <td>@Vicky</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>James</td>
                                        <td>Alexander</td>
                                        <td>@Alex</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Franklin</td>
                                        <td>Morrison</td>
                                        <td>@Frank</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <!--/ Tab-content -->
            <div class="modal-footer">
                <button type="button" class="btn bg-grey-700 btn-xs"  data-popup="tooltip" title=@lang('labels.close') data-dismiss="modal">@lang('labels.close')</button>
                <button type="submit" id="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save')</button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- /large modal -->

<!-- Large modal -->
<div id="modal_large_view" class="modal fade">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="#">
            <div class="modal-content">
                <div class="modal-header bg-teal-800">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title"><i class="icon-stack3"></i> &nbsp;User Manager</h5>
                </div>
                <br>
                <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                    <li class="active"><a href="#highlighted-tab1" data-toggle="tab"><i class="icon-user-check position-left"></i> User</a></li>
                    <li><a href="#highlighted-tab2" data-toggle="tab"><i class="icon-user-block position-left"></i> Permissions</a></li>
                    <li><a href="#highlighted-tab3" data-toggle="tab"><i class="icon-collaboration position-left"></i> Board</a></li>
                    <li><a href="#highlighted-tab4" data-toggle="tab"><i class="icon-file-text position-left"></i> User History</a></li>
                </ul>
                <!-- Tab-content -->
                <div class="tab-content">
                    <div class="tab-pane active" id="highlighted-tab1">
                        <div class="modal-body">


                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab2">
                        <div class="modal-body">
                            dd

                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab3">
                        <div class="modal-body">
                            <h6 class="text-semibold">Text in a modal</h6>
                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

                            <hr>

                            <h6 class="text-semibold">Another paragraph</h6>
                            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab4">
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-xxs">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Eugene</td>
                                        <td>Kopyov</td>
                                        <td>@Kopyov</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Victoria</td>
                                        <td>Baker</td>
                                        <td>@Vicky</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>James</td>
                                        <td>Alexander</td>
                                        <td>@Alex</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Franklin</td>
                                        <td>Morrison</td>
                                        <td>@Frank</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Tab-content -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /large modal -->