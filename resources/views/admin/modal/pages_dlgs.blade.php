

<!-- Large modal -->
<div id="modal_large_view" class="modal fade">
    <div class="modal-dialog modal-full">
        <form class="form-horizontal" action="#" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
            <div class="modal-content">
                <div class="modal-header bg-teal-800">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title"><i class="icon-stack3"></i> &nbsp; Page: @{{ page.type }}</h5>
                </div>
                <br>
                <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                    <li class="active"><a href="#highlighted-tab1" data-toggle="tab"><i class="icon-user-check position-left"></i> Page</a></li>
                    <li><a href="#highlighted-tab2" data-toggle="tab"><i class="icon-user-block position-left"></i> Page History</a></li>
                </ul>
                <!-- Tab-content -->
                <div class="tab-content">
                    <div class="tab-pane active" id="highlighted-tab1">
                        <div class="modal-body">
                            <fieldset>
                                <legend class="text-semibold"><i class="icon-eye4"></i> View Mode</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.id') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <p class="text-black form-control" role="button" v-text="page.id"></p>
                                    </div>
                                </div>

                                <div class="form-group" :class="form.errors.has('title') ? 'has-error' : ''">
                                    <label class="col-lg-3 control-label">@lang('labels.title') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control input-xs" id="title" name="title" v-model="page.title">
                                        <label class="validation-error-label" for="email-error" v-if="form.errors.has('title')">
                                            <strong v-text="form.errors.get('title')"></strong>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" :class="form.errors.has('content') ? 'has-error' : ''">
                                    <label class="col-lg-3 control-label">@lang('labels.description')</label>
                                    <div class="col-lg-9">
                                        <textarea   :data-id="page.id" data-type="App\Page" data-url="{{ url('attachments/store') }}" class="form-control col-md-12 rounded editor" name="content" id="post_content" v-model="page.content"></textarea>
                                        <label class="validation-error-label" for="name-error" v-if="form.errors.has('content')">
                                            <strong v-text="form.errors.get('content')"></strong>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab2">
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-xxs">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Eugene</td>
                                        <td>Kopyov</td>
                                        <td>@Kopyov</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Victoria</td>
                                        <td>Baker</td>
                                        <td>@Vicky</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>James</td>
                                        <td>Alexander</td>
                                        <td>@Alex</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Franklin</td>
                                        <td>Morrison</td>
                                        <td>@Frank</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Tab-content -->
                <div class="modal-footer">
                    <button type="submit" id="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save')</button>
                    <button type="button" class="btn bg-grey-700 btn-xs"  data-popup="tooltip" title=@lang('labels.close') data-dismiss="modal">@lang('labels.close')</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /large modal -->