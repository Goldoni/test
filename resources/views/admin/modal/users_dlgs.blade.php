<!-- Large modal -->
<div id="modal_large" class="modal fade">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="#" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
        <div class="modal-content">
            <div class="modal-header bg-teal-800">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"><i class="icon-stack3"></i> &nbsp;User Manager</h5>
            </div>
            <br>
            <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                <li class="active"><a href="#highlight-tab1" data-toggle="tab"><i class="icon-user-check position-left"></i> User</a></li>
                <li><a href="#highlight-tab2" data-toggle="tab"><i class="icon-user-block position-left"></i> Permissions</a></li>
                <li><a href="#highlight-tab3" data-toggle="tab"><i class="icon-collaboration position-left"></i> Board</a></li>
                <li><a href="#highlight-tab4" data-toggle="tab"><i class="icon-file-text position-left"></i> User History</a></li>
            </ul>
            <!-- Tab-content -->
            <div class="tab-content">
                <div class="tab-pane active" id="highlight-tab1">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">Enter your information</legend>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">@lang('labels.id') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <p type="text" class="form-control input-xs" v-text="user.id"></p>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('username') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.username') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control input-xs" id="username" name="username" v-model="user.username" required>
                                    <label class="validation-error-label" for="email-error" v-if="form.errors.has('username')">
                                        <strong v-text="form.errors.get('username')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('first_name') || form.errors.has('last_name')  ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.first_name') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control input-xs" id="first_name" name="first_name" v-model="user.first_name" required>
                                    <label class="validation-error-label" for="first_name-error" v-if="form.errors.has('first_name')">
                                        <strong v-text="form.errors.get('first_name')"></strong>
                                    </label>
                                </div>

                                <div class="col-lg-4">
                                    <input type="text" class="form-control input-xs" id="last_name" name="last_name" v-model="user.last_name" required>
                                    <label class="validation-error-label" for="last_name-error" v-if="form.errors.has('last_name')">
                                        <strong v-text="form.errors.get('last_name')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('email') || form.errors.has('phone1') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.email') &nbsp; /&nbsp; @lang('labels.tel')<span class="required">&nbsp;* </span></label>
                                <div class="col-lg-5">
                                    <input type="email" class="form-control input-xs" id="email" name="email" v-model="user.email" required>
                                    <label class="validation-error-label" for="email-error" v-if="form.errors.has('email')">
                                        <strong v-text="form.errors.get('email')"></strong>
                                    </label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="tel" class="form-control input-xs" id="phone1" name="phone1" v-model="user.phone1" required>
                                    <label class="validation-error-label" for="phone1-error" v-if="form.errors.has('phone1')">
                                        <strong v-text="form.errors.get('phone1')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('password') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.password')</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control input-xs" id="password" name="password" v-model="user.password">
                                    <label class="validation-error-label" for="password-error" v-if="form.errors.has('password')">
                                        <strong v-text="form.errors.get('password')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('password_confirm') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.password_confirm')</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control input-xs" id="password_confirm" name="password_confirm" v-model="user.password_confirm">
                                    <label class="validation-error-label" for="password_confirm-error" v-if="form.errors.has('password_confirm')">
                                        <strong v-text="form.errors.get('password_confirm')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('interest') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.interest')</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control input-xs" id="interest" name="interest" v-model="user.interest">
                                    <label class="validation-error-label" for="interest-error" v-if="form.errors.has('interest')">
                                        <strong v-text="form.errors.get('interest')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('language') ? 'has-error' : ''">
                                <label class="control-label col-md-3">@lang('labels.language') <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="mb-15">
                                        <select class="selectpicker form-control" id="language"  data-live-search="true" name="language" data-size="8" v-model="user.language" required>
                                            <option value="fr" selected>Francais</option>
                                            <option value="en">Anglais</option>
                                            <option value="de">Allemand</option>
                                        </select>
                                    </div>
                                    <label class="validation-error-label" for="interest-error" v-if="form.errors.has('language')">
                                        <strong v-text="form.errors.get('language')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('country') || form.errors.has('city') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.country') &nbsp;/&nbsp; @lang('labels.city') <span class="required">&nbsp;* </span></label>
                                <div class="col-lg-5">
                                    <select class="selectpicker form-control " id="country"  data-live-search="true" name="country" data-size="8"  value="{{ old('country') }}"  v-model="user.country" required>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BA">Bosnia and Herzegowina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BV">Bouvet Island</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian Ocean Territory</option>
                                        <option value="BN">Brunei Darussalam</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM" selected>Cameroon</option>
                                        <option value="CA">Canada</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African Republic</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CX">Christmas Island</option>
                                        <option value="CC">Cocos (Keeling) Islands</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, the Democratic Republic of the</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Cote d'Ivoire</option>
                                        <option value="HR">Croatia (Hrvatska)</option>
                                        <option value="CU">Cuba</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Republic</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guinea</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Islands (Malvinas)</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesia</option>
                                        <option value="TF">French Southern Territories</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard and Mc Donald Islands</option>
                                        <option value="VA">Holy See (Vatican City State)</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IR">Iran (Islamic Republic of)</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KP">Korea, Democratic People's Republic of</option>
                                        <option value="KR">Korea, Republic of</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Lao People's Democratic Republic</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libyan Arab Jamahiriya</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macau</option>
                                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Islands</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia, Federated States of</option>
                                        <option value="MD">Moldova, Republic of</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="AN">Netherlands Antilles</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Mariana Islands</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">Reunion</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="KN">Saint Kitts and Nevis</option>
                                        <option value="LC">Saint LUCIA</option>
                                        <option value="VC">Saint Vincent and the Grenadines</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome and Principe</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SK">Slovakia (Slovak Republic)</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                                        <option value="ES">Spain</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SH">St. Helena</option>
                                        <option value="PM">St. Pierre and Miquelon</option>
                                        <option value="SD">Sudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="SY">Syrian Arab Republic</option>
                                        <option value="TW">Taiwan, Province of China</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania, United Republic of</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad and Tobago</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks and Caicos Islands</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emirates</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="US">United States</option>
                                        <option value="UM">United States Minor Outlying Islands</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Viet Nam</option>
                                        <option value="VG">Virgin Islands (British)</option>
                                        <option value="VI">Virgin Islands (U.S.)</option>
                                        <option value="WF">Wallis and Futuna Islands</option>
                                        <option value="EH">Western Sahara</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select>
                                    <label class="validation-error-label" for="country-error" v-if="form.errors.has('country')">
                                        <strong v-text="form.errors.get('country')"></strong>
                                    </label>
                                </div>

                                <div class="col-lg-4">
                                    <input type="text" class="form-control input-xs" id="city" name="city" v-model="user.city">
                                    <label class="validation-error-label" for="city-error" v-if="form.errors.has('city')">
                                        <strong v-text="form.errors.get('city')"></strong>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" :class="form.errors.has('zipcode') || form.errors.has('street') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.zipcode') &nbsp;/&nbsp; @lang('labels.street')</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control input-xs" id="zipcode" name="zipcode" v-model="user.zipcode">
                                    <label class="validation-error-label" for="zipcode-error" v-if="form.errors.has('zipcode')">
                                        <strong v-text="form.errors.get('zipcode')"></strong>
                                    </label>
                                </div>

                                <div class="col-lg-4">
                                    <input type="text" class="form-control input-xs" id="street" name="street" v-model="user.street">
                                    <label class="validation-error-label" for="street-error" v-if="form.errors.has('street')">
                                        <strong v-text="form.errors.get('street')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" :class="form.errors.has('skype') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.skype')</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control input-xs" id="skype" name="skype" v-model="user.skype">
                                    <label class="validation-error-label" for="skype-error" v-if="form.errors.has('skype')">
                                        <strong v-text="form.errors.get('skype')"></strong>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">@lang('labels.actived') &nbsp; /&nbsp; @lang('labels.last_login')</label>
                                <div class="col-lg-9">
                                    <span class="btn" :class="user.confirmation_token == 1 ? 'text-teal-800' : 'text-danger-800'" v-text="user.confirmation_token == 1 ? 'Actived' : 'Desactived'"></span>
                                    <span class="btn " :class="user.last_login != null ? 'text-teal-600' : 'text-danger-600'" v-text="user.last_login != null ? user.last_login : 'NO'"></span>
                                </div>
                            </div>


                            <div class="form-group" :class="form.errors.has('about') ? 'has-error' : ''">
                                <label class="col-lg-3 control-label">@lang('labels.about')</label>
                                <div class="col-lg-9">
                                    <textarea rows="3" cols="5" class="form-control input-xs" id="about"  name="about" v-model="user.about"></textarea>
                                    <label class="validation-error-label" for="about-error" v-if="form.errors.has('about')">
                                        <strong v-text="form.errors.get('about')"></strong>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="tab-pane" id="highlight-tab2">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">Permissions</legend>
                            <br>
                            <div class="form-group">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <select multiple="multiple" id="roles" class="form-control" name="roles[]" multiple>
                                        @foreach (json_decode($roles) as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>
                        </fieldset>
                    </div>
                </div>

                <div class="tab-pane" id="highlight-tab3">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">Groupe D'utilisateurs</legend>
                            <br>
                            <div class="form-group">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <select multiple="multiple" id="boards" class="form-control" name="boards[]" multiple>
                                        @foreach (json_decode($boards) as $board)
                                            <option value="{{ $board->id }}">{{ $board->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>
                        </fieldset>
                    </div>
                </div>

                <div class="tab-pane" id="highlight-tab4">
                    <div class="modal-body">
                        <fieldset>
                            <legend class="text-semibold">User History</legend>
                                <div class="table-responsive bordered">
                                    <table class="table table-xxs">
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Last Edited</th>
                                            <th>User</th>
                                            <th>Activity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Eugene</td>
                                            <td>
                                                <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                                <br>
                                                <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                                <span> Kopyoggg ffff ffff fffff ffffv ffff</span>
                                            </td>
                                            <td>@Kopyov</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Victoria</td>
                                            <td>Baker</td>
                                            <td>@Vicky</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>James</td>
                                            <td>Alexander</td>
                                            <td>@Alex</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Franklin</td>
                                            <td>Morrison</td>
                                            <td>@Frank</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                    </div>
                </div>
            </div>
            <!--/ Tab-content -->
            <div class="modal-footer">
                <button type="button" class="btn bg-grey-700 btn-xs"  data-popup="tooltip" title=@lang('labels.close') data-dismiss="modal">@lang('labels.close')</button>
                <button type="submit" id="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save')</button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- /large modal -->

<!-- Large modal -->
<div id="modal_large_view" class="modal fade">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="#">
            <div class="modal-content">
                <div class="modal-header bg-teal-800">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title"><i class="icon-stack3"></i> &nbsp;User Manager</h5>
                </div>
                <br>
                <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                    <li class="active"><a href="#highlighted-tab1" data-toggle="tab"><i class="icon-user-check position-left"></i> User</a></li>
                    <li><a href="#highlighted-tab2" data-toggle="tab"><i class="icon-user-block position-left"></i> Permissions</a></li>
                    <li><a href="#highlighted-tab3" data-toggle="tab"><i class="icon-collaboration position-left"></i> Board</a></li>
                    <li><a href="#highlighted-tab4" data-toggle="tab"><i class="icon-file-text position-left"></i> User History</a></li>
                </ul>
                <!-- Tab-content -->
                <div class="tab-content">
                    <div class="tab-pane active" id="highlighted-tab1">
                        <div class="modal-body">
                            <fieldset>
                                <legend class="text-semibold"><i class="icon-eye4"></i> View Mode</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.id') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <p class="text-black form-control" role="button" v-text="user.id"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.username') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <p class="text-black form-control" role="button" v-text="user.username"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.full_name') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-5">
                                        <p class="text-black form-control" role="button" v-text="user.first_name"></p>
                                    </div>

                                    <div class="col-lg-4">
                                        <p class="text-black form-control" role="button" v-text="user.last_name"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.email') &nbsp; /&nbsp; @lang('labels.tel')<span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-5">
                                        <p class="text-black form-control" role="button" v-text="user.email"></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p  class="text-black form-control" role="button" v-text="user.phone1"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.interest')</label>
                                    <div class="col-lg-9">
                                        <p class="text-black form-control" role="button" v-text="user.interest"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">@lang('labels.language') <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <p class="text-black form-control" role="button" v-text="form.lang.get(user.language)"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.country') &nbsp;/&nbsp; @lang('labels.city') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-5">
                                        <select class="select" id="country"  name="country" v-model="user.country" required>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegowina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM" selected>Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Cote d'Ivoire</option>
                                            <option value="HR">Croatia (Hrvatska)</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard and Mc Donald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran (Islamic Republic of)</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libyan Arab Jamahiriya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="AN">Netherlands Antilles</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint LUCIA</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovakia (Slovak Republic)</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SH">St. Helena</option>
                                            <option value="PM">St. Pierre and Miquelon</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands (British)</option>
                                            <option value="VI">Virgin Islands (U.S.)</option>
                                            <option value="WF">Wallis and Futuna Islands</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4">
                                        <p class="text-black form-control" role="button"  v-text="user.city"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.zipcode') &nbsp;/&nbsp; @lang('labels.street')</label>
                                    <div class="col-lg-5">
                                        <p class="text-black form-control" role="button" v-text="user.zipcode"></p>
                                    </div>

                                    <div class="col-lg-4">
                                        <p class="text-black form-control" role="button"  v-text="user.street"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.skype')</label>
                                    <div class="col-lg-9">
                                        <p class="text-black form-control" role="button"  v-text="user.skype"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.actived')</label>
                                    <div class="col-lg-9">
                                        <p class="btn" :class="user.confirmation_token == 1 ? 'btn-success' : 'btn-danger'" v-text="user.confirmation_token == 1 ? 'Actived' : 'Desactived'"></p>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.about')</label>
                                    <div class="col-lg-9">
                                        <p class="text-black" role="button" v-text="user.about" disabled></p>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab2">
                        <div class="modal-body">
                            dd

                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab3">
                        <div class="modal-body">
                            <h6 class="text-semibold">Text in a modal</h6>
                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

                            <hr>

                            <h6 class="text-semibold">Another paragraph</h6>
                            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab4">
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-xxs">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Eugene</td>
                                        <td>Kopyov</td>
                                        <td>@Kopyov</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Victoria</td>
                                        <td>Baker</td>
                                        <td>@Vicky</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>James</td>
                                        <td>Alexander</td>
                                        <td>@Alex</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Franklin</td>
                                        <td>Morrison</td>
                                        <td>@Frank</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Tab-content -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /large modal -->