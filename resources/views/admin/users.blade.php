@extends('layouts.app')

@section('title')
    <title>H4K | Administration</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/datatables.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/responsive.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/buttons.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/bootstrap_select.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/datatables_extension_buttons_html5.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/users_customers.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/admin/users/users.js") }}"></script>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="user_profile_tabbed.html">Admin</a></li>
        <li class="active">Users</li>
    </ul>
@endsection
@section('content')
    <div id="vueUsers" class="row">
        @include('admin/modal/users_dlgs')
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">@lang('tables.user_manager')</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        @if($permitted["create"])
                        <li><a @click="create" data-popup="tooltip" title="Add"><i class="icon-plus3"></i> </a></li>
                        @endif
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

            </div>

            <table class="table datatable-responsive-column-controlled">
                <thead>
                <tr>
                    <th></th>
                    <th>id</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Adresse</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach (json_decode($users) as $user)
                    <tr class="context-dynamic" data-id="{{ $user->id }}">
                        <td></td>
                        <td><h6 class="no-margin text-semibold">{{ $user->id }}</h6></td>
                        <td>
                            <div class="media">
                                <a href="user_pages_profile_tabbed.html" class="media-left">
                                    <img src="{{ asset("assets/images/placeholder.jpg")}}" width="40" height="40" class="img-circle img-md" alt="">
                                </a>

                                <div class="media-body media-middle">
                                    <a @click="view({{ $user->id }})" data-popup="tooltip" title="{{ $user->full_name }}" class="text-semibold">{{ $user->full_name }}</a>
                                    <div class="text-muted text-size-small">
                                        Date d'enregistrement: {{ $user->created_at }}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td><a @click="view({{ $user->id }})" data-popup="tooltip" title="{{ $user->username }}">{{ $user->username }}</a></td>
                        <td><a @click="view({{ $user->id }})" data-popup="tooltip" title="{{ $user->email }}" >{{ $user->email }}</a></td>
                        <td> <span data-popup="tooltip" title="{{ $user->phone1 }}" role="button" >{{ $user->phone1 }}</span></td>
                        <td>
                            <ul class="list list-unstyled no-margin">
                                <li class="no-margin">
                                    <i class="icon-location3 text-size-base text-warning position-left"></i>
                                    Pays:
                                    <a href="javascript:;" data-popup="tooltip" title="{{ $user->country }}">{{ $user->country }}</a>
                                </li>

                                <li class="no-margin">
                                    <i class="icon-location4 text-size-base text-success position-left"></i>
                                    Ville:
                                    <a href="javascript:;" data-popup="tooltip" title="{{ $user->city }}">{{ $user->city }}</a>
                                </li>
                            </ul>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a @click="view({{ $user->id }})" data-popup="tooltip" title="Show"><i class="icon-eye4"></i></a></li>
                                @if($permitted["update"])
                                <li><a @click="edit({{ $user->id }})" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
                                @endif
                                @if($permitted["delete"])
                                <li><a @click="destroy({{ $user->id }})" data-popup="tooltip" title="Delete"><i class="icon-trash"></i></a></li>
                                @endif
                                @if($permitted["login"])
                                <li><a href="{{ route('deputy', $user->id ) }}" data-popup="tooltip" title="Sign in"><i class="icon-user-check"></i></a></li>
                                @endif
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="context-dynamic-menu">
                <ul class="dropdown-menu">
                    @if($permitted["create"])
                        <li><a href="javascript:;"><i class="icon-user-plus"></i> Add</a></li>
                    @endif
                    <li><a href="javascript:;"><i class="icon-eye4"></i> Show</a></li>
                    @if($permitted["update"])
                    <li><a href="javascript:;"><i class="icon-pencil6"></i> Edit</a></li>
                    @endif
                    @if($permitted["delete"])
                    <li><a href="javascript:;"><i class="icon-trash"></i> Delete</a></li>
                    @endif
                    @if($permitted["login"])
                    <li><a href="{{ route('deputy', $user->id ) }}"><i class="icon-user-check"></i> Login</a></li>
                    @endif
                </ul>
            </div>

        </div>
    </div>



@endsection
