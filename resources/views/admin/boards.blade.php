@extends('layouts.default')

@section('title')
    <title>H4K | Boards Admin</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/datatables.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/buttons.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/responsive.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/admin_boards.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/admin/boards/boards.js") }}"></script>
@endsection

@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active"> @lang('menus.calendar')</li>
    </ul>
    @endsection
    @section('content')
    <div class="panel panel-flat" id="vueBoards">
        @include('admin/modal/boards_dlgs')
        <div class="panel-heading">
            <h6 class="panel-title">Boards Manager</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
        </div>

        <table class="table table-striped text-nowrap table-customers">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Description</th>
                <th>Members</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach (json_decode($boards) as $board)
                <tr>
                    <td>{{ $board->id }}</td>
                    <td><a href="#" data-popup="tooltip" title="{{ $board->name }}">{{ $board->name  }}</a></td>
                    <td><a href="#" data-popup="tooltip" title="{{ $board->description }}">{{ $board->description  }}</a></td>
                    <td><a href="#" data-popup="tooltip" title="{{ $board->users_count }}">{{ $board->users_count  }}</a></td>
                    <td>{{ $board->created_at  }}</td>
                    <td>{{ $board->updated_at  }} </td>
                    <td class="text-right">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i>
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="javascript:;" @click="show({{ $board->id }})" data-popup="tooltip" title="Show"><i class="icon-eye4"></i> Show</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:;" @click="edit({{ $board->id }})" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i> Edit</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-popup="tooltip" title="Delete"><i class="icon-trash"></i> Delete</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                    <td class="no-padding-left"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /customers -->
@endsection
