@extends('layouts.default')
@section('body')
    <body class="navbar-top sidebar-xs">
@endsection
@section('title')
    <title>H4K | Calendrier</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/fullcalendar/lang/ar.js"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="assets/js/pages/extra_fullcalendar_advanced.js"></script>
@endsection

@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active"> @lang('menus.calendar')</li>
    </ul>
    @endsection
    @section('content')
            <!-- External events -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">External events</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <p class="content-group"></p>

            <div class="row">
                <div class="col-md-3">
                    <div class="content-group" id="external-events">
                        <h6>Draggable Events</h6>
                        <div class="fc-events-container content-group">
                            <div class="fc-event" data-color="#546E7A">Sauna and stuff</div>
                            <div class="fc-event" data-color="#26A69A">Lunch time</div>
                            <div class="fc-event" data-color="#546E7A">Meeting with Fred</div>
                            <div class="fc-event" data-color="#FF7043">Shopping</div>
                            <div class="fc-event" data-color="#5C6BC0">Restaurant</div>
                            <div class="fc-event">Basketball</div>
                            <div class="fc-event">Daily routine</div>
                        </div>

                        <div class="checkbox checkbox-right checkbox-switchery switchery-xs text-center">
                            <label>
                                <input type="checkbox" class="switch" checked="checked" id="drop-remove">
                                Remove after drop
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="fullcalendar-external"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /external events -->
@endsection
