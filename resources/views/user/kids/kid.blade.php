@extends('layouts.app')

@section('title')
    <title>H4K | @lang('menus.my_kinds')</title>
@endsection
@section('css')
    <link href={{ asset("../assets/global/css/components.css") }} rel="stylesheet" type="text/css">
@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/ui/moment/moment.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/ui/fullcalendar/fullcalendar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/core/libraries/jquery_ui/widgets.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/wizards/steps.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/styling/uniform.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/core/libraries/jasny_bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/validation/validate.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/extensions/cookie.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/learning_detailed.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/assets/js/plugins/horizontal-timeline/horozontal-timeline.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/wizard_steps.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/user/kid/kid.js") }}"></script>
@endsection
@section('sidebar-mobile-detached-toggle')
    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
@endsection
@section('sidebar-mobile-detached-toggle-link')
    <li> <a class="sidebar-control sidebar-detached-hide hidden-xs">  <i class="icon-drag-right"></i> </a> </li>
@endsection
@section('page-header-content')
    <div class="page-header-content">
        <div class="page-title">
            <h5>
                <i class="icon-heart5 position-left"></i>
                <span class="text-semibold">Mes Bébés</span>
            </h5>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-man-woman"></i><span>Ajouter un enfant ou un bébé</span></a>
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator"></i><span>Calendar und Events</span></a>
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-camera"></i><span>Ajouter une image</span></a>
            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> @lang('menus.home')</a></li>
        <li class="active">@lang('menus.my_kinds')</li>
    </ul>
@endsection
@section('content')
    <div id="vueKids" v-cloak>
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">
                <!-- Course overview -->
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title text-semibold">Dennis Corwin</h4>

                        <div class="heading-elements">
                            <ul class="list-inline list-inline-separate heading-text">
                                <li>Rating: <span class="text-semibold">4.85</span></li>
                                <li>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                    <span class="text-muted position-right">(439)</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-lg nav-tabs nav-tabs-highlight nav-tabs-toolbar no-margin nav-justified">
                        <li class="active"><a href="#course-overview" data-toggle="tab"><i class="icon-menu7 position-left"></i> Aperçu</a></li>
                        <li><a href="#course-attendees" data-toggle="tab"><i class="icon-camera position-left"></i> Images</a></li>
                        <li><a href="#course-schedule" data-toggle="tab"><i class="icon-calendar3 position-left"></i> Calendar & Events</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="course-overview">
                            <div class="panel-body">
                                <div class="content-group-lg">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <kid-info :id="1"></kid-info>
                                        </div>
                                        <div class="col-sm-5">
                                            <!-- Calendar widget -->
                                            <div class="panel has-scroll">
                                                <div class="datepicker no-border"></div>
                                            </div>
                                            <!-- /calendar widget -->
                                            <!-- Simple text stats with icons -->
                                            <div class="panel panel-body">
                                                <div class="row text-center">
                                                    <div class="col-xs-4">
                                                        <p><i class="icon-users2 icon-2x display-inline-block text-info"></i></p>
                                                        <h5 class="text-semibold no-margin">2,345</h5>
                                                        <span class="text-muted text-size-small">users</span>
                                                    </div>

                                                    <div class="col-xs-4">
                                                        <p><i class="icon-point-up icon-2x display-inline-block text-warning"></i></p>
                                                        <h5 class="text-semibold no-margin">3,568</h5>
                                                        <span class="text-muted text-size-small">clicks</span>
                                                    </div>

                                                    <div class="col-xs-4">
                                                        <p><i class="icon-cash3 icon-2x display-inline-block text-success"></i></p>
                                                        <h5 class="text-semibold no-margin">$9,693</h5>
                                                        <span class="text-muted text-size-small">revenue</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /simple text stats with icons -->
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-directions font-green hide"></i>
                                                    <span class="caption-subject bold font-dark uppercase "> Activities</span>
                                                    <span class="caption-helper">Horizontal Timeline</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="cd-horizontal-timeline mt-timeline-horizontal">
                                                    <div class="timeline">
                                                        <div class="events-wrapper">
                                                            <div class="events">
                                                                <ol>
                                                                    <li>
                                                                        <a href="#0" data-date="16/01/2014" class="border-after-red bg-after-red selected">16 Jan</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="28/02/2014" class="border-after-red bg-after-red">28 Feb</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="20/04/2014" class="border-after-red bg-after-red">20 Mar</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="20/05/2014" class="border-after-red bg-after-red">20 May</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="09/07/2014" class="border-after-red bg-after-red">09 Jul</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="30/08/2014" class="border-after-red bg-after-red">30 Aug</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="15/09/2014" class="border-after-red bg-after-red">15 Sep</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="01/11/2014" class="border-after-red bg-after-red">01 Nov</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="10/12/2014" class="border-after-red bg-after-red">10 Dec</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="19/01/2015" class="border-after-red bg-after-red">29 Jan</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#0" data-date="03/03/2015" class="border-after-red bg-after-red">3 Mar</a>
                                                                    </li>
                                                                </ol>
                                                                <span class="filling-line bg-red" aria-hidden="true"></span>
                                                            </div>
                                                            <!-- .events -->
                                                        </div>
                                                        <!-- .events-wrapper -->
                                                        <ul class="cd-timeline-navigation mt-ht-nav-icon">
                                                            <li>
                                                                <a href="#0" class="prev inactive btn btn-outline red md-skip bg-danger-800">
                                                                    <i class="icon-arrow-left15"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#0" class="next btn btn-outline red md-skip  bg-danger-800">
                                                                    <i class="icon-arrow-right15"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <!-- .cd-timeline-navigation -->
                                                    </div>
                                                    <!-- .timeline -->
                                                    <div class="events-content">
                                                        <ol>
                                                            <li class="selected" data-date="16/01/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">New User</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_3.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">16 January 2014 : 7:45 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, mi felis, aliquam at iaculis mi felis, aliquam
                                                                        at iaculis finibus eu ex. Integer efficitur tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur odio non est rhoncus volutpat.</p>
                                                                    <a href="javascript:;" class="btn btn-circle red btn-outline">Read More</a>
                                                                    <a href="javascript:;" class="btn btn-circle btn-icon-only blue">
                                                                        <i class="fa fa-plus"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="btn btn-circle btn-icon-only green pull-right">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            <li data-date="28/02/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Sending Shipment</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_3.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Hugh Grant</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">28 February 2014 : 10:15 AM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle btn-outline green-jungle">Download Shipment List</a>
                                                                    <div class="btn-group dropup pull-right">
                                                                        <button class="btn btn-circle blue-steel dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
                                                                            <i class="fa fa-angle-down"></i>
                                                                        </button>
                                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                                            <li>
                                                                                <a href="javascript:;">Action </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">Another action </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">Something else here </a>
                                                                            </li>
                                                                            <li class="divider"> </li>
                                                                            <li>
                                                                                <a href="javascript:;">Separated link </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li data-date="20/04/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Blue Chambray</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_1.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue">Rory Matthew</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">20 April 2014 : 10:45 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                        qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                                    <a href="javascript:;" class="btn btn-circle red">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="20/05/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Timeline Received</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_2.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">20 May 2014 : 12:20 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="09/07/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Event Success</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_1.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Matt Goldman</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">9 July 2014 : 8:15 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde.</p>
                                                                    <a href="javascript:;"
                                                                       class="btn btn-circle btn-outline purple-medium">View Summary</a>
                                                                    <div class="btn-group dropup pull-right">
                                                                        <button class="btn btn-circle green dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
                                                                            <i class="fa fa-angle-down"></i>
                                                                        </button>
                                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                                            <li>
                                                                                <a href="javascript:;">Action </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">Another action </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">Something else here </a>
                                                                            </li>
                                                                            <li class="divider"> </li>
                                                                            <li>
                                                                                <a href="javascript:;">Separated link </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li data-date="30/08/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Conference Call</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_1.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Rory Matthew</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">30 August 2014 : 5:45 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <img class="timeline-body-img pull-left" src="../assets/pages/media/blog/5.jpg" alt="">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                        qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                        qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                        qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
                                                                    <a href="javascript:;" class="btn btn-circle red">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="15/09/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Conference Decision</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_5.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Jessica Wolf</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">15 September 2014 : 8:30 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <img class="timeline-body-img pull-right" src="../assets/pages/media/blog/6.jpg" alt="">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
                                                                        qui ut.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-sharp">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="01/11/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Timeline Received</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_2.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">1 November 2014 : 12:20 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="10/12/2014">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Timeline Received</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_2.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">10 December 2015 : 12:20 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="19/01/2015">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Timeline Received</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_2.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">19 January 2015 : 12:20 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                                </div>
                                                            </li>
                                                            <li data-date="03/03/2015">
                                                                <div class="mt-title">
                                                                    <h2 class="mt-content-title">Timeline Received</h2>
                                                                </div>
                                                                <div class="mt-author">
                                                                    <div class="mt-avatar">
                                                                        <img src="../assets/pages/media/users/avatar80_2.jpg" />
                                                                    </div>
                                                                    <div class="mt-author-name">
                                                                        <a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
                                                                    </div>
                                                                    <div class="mt-author-datetime font-grey-mint">3 March 2015 : 12:20 PM</div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="mt-content border-grey-steel">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
                                                                        dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
                                                                        odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
                                                                        dignissim luctus risus sed sodales.</p>
                                                                    <a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
                                                                </div>
                                                            </li>
                                                        </ol>
                                                    </div>
                                                    <!-- .events-content -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 120px">Lessons</th>
                                        <th style="width: 250px">Name</th>
                                        <th>Description</th>
                                        <th style="width: 120px">Duration</th>
                                        <th style="width: 120px">Status</th>
                                        <th style="width: 150px">Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Lesson 1</td>
                                        <td><a href="#">Introduction</a></td>
                                        <td>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed</td>
                                        <td>10 hours</td>
                                        <td><span class="label label-default">Closed</span></td>
                                        <td>Oct 21st, 2016</td>
                                    </tr>
                                    <tr>
                                        <td>Lesson 2</td>
                                        <td><a href="#">Design tools</a></td>
                                        <td>He lay on his armour-like back, and if he lifted his head a little he could see his brown</td>
                                        <td>20 hours</td>
                                        <td><span class="label label-primary">Registration</span></td>
                                        <td>Oct 22nd, 2016</td>
                                    </tr>
                                    <tr>
                                        <td>Lesson 3</td>
                                        <td><a href="#">Workspace</a></td>
                                        <td>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many</td>
                                        <td>35 hours</td>
                                        <td><span class="label label-danger">On time</span></td>
                                        <td>Oct 23rd, 2016</td>
                                    </tr>
                                    <tr>
                                        <td>Lesson 4</td>
                                        <td><a href="#">Creating effects</a></td>
                                        <td>A collection of textile samples lay spread out on the table - Samsa was a travelling salesman</td>
                                        <td>25 hours</td>
                                        <td><span class="label label-danger">On time</span></td>
                                        <td>Oct 24th, 2016</td>
                                    </tr>
                                    <tr>
                                        <td>Lesson 5</td>
                                        <td><a href="#">Digital design</a></td>
                                        <td>Drops of rain could be heard hitting the pane, which made him feel quite sad. Gregor then</td>
                                        <td>50 hours</td>
                                        <td><span class="label label-danger">On time</span></td>
                                        <td>Oct 25th, 2016</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="course-attendees">
                            <div class="panel-body">
                                <div class="search-results-list">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">For ostrich much</a></h6>
                                                    Some various less crept gecko the jeepers dear forewent far the ouch far awherever
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Helpfully stolidly</a></h6>
                                                    Hippopotamus aside while a shrewdly this after kookaburra wow in haphazardly much
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Considering far</a></h6>
                                                    Kookaburra so hey a less tritely far congratulated this winked some under beyond
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Despite perversely</a></h6>
                                                    Coming merits and was talent enough far. Sir joy northward sportsmen education
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">To shewing demands</a></h6>
                                                    Marianne property cheerful informed at striking at. Clothes parlors however cottage
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">In views it or meant</a></h6>
                                                    Be concern parlors settled or do shyness address. Remainder northward performed
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Yet late add name</a></h6>
                                                    Prepared is me marianne pleasure likewise debating. Wonder an unable except us
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-sm-6">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="assets/images/placeholder.jpg" alt="">
                                                    <div class="caption-overflow">
												<span>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-zoomin3"></i></a>
													<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
												</span>
                                                    </div>
                                                </div>

                                                <div class="caption">
                                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Moreover speedily</a></h6>
                                                    Procured to contempt oh he raptures amounted occasion. One boy assure income
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="course-schedule">
                            <div class="panel-body">
                                <div class="schedule"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /course overview -->

            </div>
        </div>
        <!-- /detached content -->


        <!-- Detached sidebar -->
        <div class="sidebar-detached">
            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">
                    <!-- Our trainers -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>@lang('menus.my_kinds')</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <ul class="media-list">
                                <li class="media" v-for="kid in kids">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">@{{ kid.name }}</a>
                                        <span class="text-size-small text-muted display-block">@lang('messages.birth') @{{ kid.birth }}</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Poser une question</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Nouveau poids</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Nouvelles tailles</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /our trainers -->

                    <!-- Course details -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Rendez-vous importants</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <a href="#" class="btn bg-teal-800 btn-block content-group">Ajouter un enfant ou un bébé</a>
                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Duration:</label>
                                <div class="pull-right">10 hours</div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Status:</label>
                                <div class="pull-right"><span class="label bg-blue">Registration</span></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Start date:</label>
                                <div class="pull-right">Nov 1st, 2016</div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">End date:</label>
                                <div class="pull-right">Nov 20th, 2016</div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Lessons:</label>
                                <div class="pull-right">12 lessons</div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Trainer:</label>
                                <div class="pull-right"><a href="#">Eugene Kopyov</a></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label no-margin text-semibold">Attendees:</label>
                                <div class="pull-right">382 people</div>
                            </div>
                        </div>
                    </div>
                    <!-- /course details -->


                    <!-- Categories -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Categories</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li class="navigation-header">Development</li>
                                <li><a href="#"><span class="badge badge-default">37</span> Frontend development</a></li>
                                <li><a href="#"><span class="badge badge-default">58</span> Backend development</a></li>
                                <li><a href="#"><span class="badge badge-default">39</span> Engineering</a></li>
                                <li class="navigation-header">Design</li>
                                <li><a href="#"><span class="badge badge-default">21</span> Interface design</a></li>
                                <li><a href="#"><span class="badge badge-default">10</span> User experience</a></li>
                                <li><a href="#"><span class="badge badge-default">26</span> Web design</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /categories -->

                    <!-- Our trainers -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Our trainers</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <ul class="media-list">
                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">James Alexander</a>
                                        <span class="text-size-small text-muted display-block">Web development</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">Jeremy Victorino</a>
                                        <span class="text-size-small text-muted display-block">Business</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">Margo Baker</a>
                                        <span class="text-size-small text-muted display-block">Marketing</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">Beatrix Diaz</a>
                                        <span class="text-size-small text-muted display-block">Human relations</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">Richard Vango</a>
                                        <span class="text-size-small text-muted display-block">Server management</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="media">
                                    <a href="#" class="media-left">
                                        <img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt="">
                                    </a>

                                    <div class="media-body">
                                        <a href="#">Dave Raters</a>
                                        <span class="text-size-small text-muted display-block">Finances</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list text-nowrap">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Ask question</a></li>
                                                    <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-stars pull-right"></i> Rate trainer</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /our trainers -->

                </div>
            </div>
        </div>
        <!-- /detached sidebar -->
    </div>



@endsection
