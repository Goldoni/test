@extends('layouts.app')

@section('title')
    <title>H4K | @lang('menus.my_kinds')</title>
@endsection
@section('css')
    <link href={{ asset("../assets/global/css/components.css") }} rel="stylesheet" type="text/css">
@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/wizards/steps.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/media/fancybox.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/tags/tagsinput.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/tags/tokenfield.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/bootstrap_select.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/uploaders/plupload/plupload.full.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/uploaders/plupload/plupload.queue.min.js") }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/ecommerce_product_list.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/tasks_grid.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/form_bootstrap_select.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/user/posts/posts.js") }}"></script>
@endsection
@section('sidebar-mobile-detached-toggle')
    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
@endsection
@section('sidebar-mobile-detached-toggle-link')
    <li> <a class="sidebar-control sidebar-detached-hide hidden-xs">  <i class="icon-drag-right"></i> </a> </li>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> @lang('menus.home')</a></li>
        <li class="active">@lang('menus.my_kinds')</li>
    </ul>
@endsection
@section('content')
    <div id="vuePosts" v-cloak>
        @include('user/posts/modal/posts_dlgs')
        <!-- Top right menu -->
        <floating-button></floating-button>
        <!-- /top right menu -->
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- Tasks options -->
                <tools></tools>
                <!-- /tasks options -->
                <div id="grid">
                    <!-- list -->
                    <posts></posts>
                    <!-- /list -->
                    <!-- Pagination -->
                    <pagination></pagination>
                    <!-- /pagination -->
                </div>
            </div>
        </div>
        <!-- /detached content -->


        <!-- Detached sidebar -->
        <div class="sidebar-detached">
            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- Categories -->
                    <categories></categories>
                    <!-- /categories -->
                    <!-- Recent comments -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Recent comments</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">James Alexander</span>
                                        </a>

                                        <span class="text-muted">Who knows, maybe that...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Margo Baker</span>
                                        </a>

                                        <span class="text-muted">That was something he...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Jeremy Victorino</span>
                                        </a>

                                        <span class="text-muted">But that would be...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Beatrix Diaz</span>
                                        </a>

                                        <span class="text-muted">What a strenuous career...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{ asset("assets/images/placeholder.jpg") }}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="#" class="media-heading">
                                            <span class="text-semibold">Richard Vango</span>
                                        </a>

                                        <span class="text-muted">Other travelling salesmen...</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /recent comments -->

                    <!-- Archive -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Archive</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation">
                                <li><a href="#">January 2017</a></li>
                                <li><a href="#">December 2016</a></li>
                                <li><a href="#">November 2016</a></li>
                                <li><a href="#">October 2016</a></li>
                                <li><a href="#">September 2016</a></li>
                                <li><a href="#">August 2016</a></li>
                                <li><a href="#">July 2016</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /archive -->



                </div>
            </div>
        </div>
        <!-- /detached sidebar -->
    </div>



@endsection
