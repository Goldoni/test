
<!-- Large modal -->
<div id="modal_large_edit_post" class="modal fade">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="#" enctype="multipart/form-data" @submit.prevent="onSubmit" @keydown="clearErrors($event.target.name)">
            <div class="modal-content">
                <div class="modal-header bg-teal-800">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title"><i class="icon-bubbles7"></i> &nbsp;@{{ post.name }}</h5>
                </div>
                <br>
                <ul class="nav nav-tabs nav-justified nav-tabs-highlight">
                    <li class="active"><a href="#highlighted-tab1" data-toggle="tab"><i class="icon-pencil7 position-left"></i>Edit</a></li>
                    <li><a href="#highlighted-tab2" data-toggle="tab"><i class="icon-images2 position-left"></i> Media</a></li>
                    <li><a href="#highlighted-tab3" data-toggle="tab"><i class="icon-folder6 position-left"></i> Downloaded files</a></li>
                    <li><a href="#highlighted-tab4" data-toggle="tab"><i class="icon-file-text position-left"></i> Post History</a></li>
                </ul>
                <!-- Tab-content -->
                <div class="tab-content">
                    <div class="tab-pane active" id="highlighted-tab1">
                        <div class="modal-body">
                            <fieldset>
                                <legend class="text-semibold">Enter your information</legend>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.id') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <p type="text" class="form-control input-xs" v-text="post.id"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.title') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control input-xs" id="name" name="name" v-model="post.name">
                                        <label class="validation-error-label" for="name-error" v-if="hasErrors('name')">
                                            <strong v-text="getErrors('name')"></strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.description')</label>
                                    <div class="col-lg-9">
                                        <textarea   :data-id="post.id" data-type="App\Post" data-url="{{ url('attachments/store') }}" class="form-control col-md-12 rounded editor" name="content" id="post_content" v-model="post.content"></textarea>
                                        <label class="validation-error-label" for="name-error" v-if="hasErrors('content')">
                                            <strong v-text="getErrors('content')"></strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.category') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <select class="selectpicker form-control " id="category"  data-live-search="true" name="category" data-size="4"  value="{{ old('country') }}"  v-model="post.category_id">
                                            @foreach (json_decode($categories) as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <label class="validation-error-label" for="name-error" v-if="hasErrors('category_id')">
                                            <strong v-text="getErrors('category_id')"></strong>
                                        </label>
                                    </div>
                                </div>
                                <!-- Custom green color -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">@lang('labels.tags') <span class="required">&nbsp;* </span></label>
                                    <div class="col-lg-9">
                                        <input type="text" name="tags" id="tags" class="form-control tokenfield-teal" value="">
                                        <label class="validation-error-label" for="name-error" v-if="hasErrors('tags')">
                                            <strong v-text="getErrors('tags')"></strong>
                                        </label>
                                    </div>
                                </div>
                                <!-- /custom green color -->
                            </fieldset>
                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab2">
                        <div class="modal-body">
                            <!-- All runtimes -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Files Uploads</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body" id="file-uploader"  :data-id="post.id" data-type="App\Post">
                                    <div class="file-uploader"><p>Your browser doesn't have Flash installed.</p></div>
                                    <!-- Media library -->
                                     <table class="table table-responsive table-striped table-bordered media-library table-lg table-files">
                                            <thead>
                                            <tr>
                                                <th>Preview</th>
                                                <th>Name</th>
                                                <th>Author</th>
                                                <th>Date</th>
                                                <th>File info</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="attachment in post.attachments" v-if="attachment.type === 'images'">
                                                <td>
                                                    <a :href="attachment.url.sm" data-popup="lightbox">
                                                        <img :src="attachment.url.xs" alt="" class="img-rounded img-preview">
                                                    </a>
                                                </td>
                                                <td><a href="javascript:;" data-popup="tooltip" :title="attachment.name" v-text="attachment.name" :href="attachment.url.origin" :download="attachment.name"></a></td>
                                                <td data-popup="tooltip" :title="attachment.author" v-text="attachment.author"></td>
                                                <td v-text="attachment.created_at"></td>
                                                <td>
                                                    <ul class="list-condensed list-unstyled no-margin">
                                                        <li><span class="text-semibold">Size:</span> @{{ Math.round(attachment.size / 1024) + ' Kb' }}</li>
                                                        <li><span class="text-semibold">Format:</span> @{{ attachment.mime_type }}</li>
                                                    </ul>
                                                </td>
                                                <td class="text-center">
                                                    <ul class="icons-list">
                                                        <li class="text-primary" data-popup="tooltip" title="download">
                                                            <a :href="attachment.url.origin" :download="attachment.name"> <i class="icon-download"></i> </a>
                                                        </li>
                                                        <li class="text-danger">
                                                            <a href="#"  @click="deleteAttachment(attachment.id)"> <i class="icon-trash"></i> </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <!-- /media library -->
                                </div>
                            </div>
                            <!-- /all runtimes -->

                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab3">
                        <div class="modal-body">
                            <!-- All runtimes -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Docs Uploads</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="docs-uploader"><p>Your browser doesn't have Flash installed.</p></div>
                                    <!-- Media library -->
                                    <table class="table table-responsive table-striped table-bordered media-library table-lg table-files">
                                        <thead>
                                        <tr>
                                            <th>Preview</th>
                                            <th>Author</th>
                                            <th>Date</th>
                                            <th>File info</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="attachment in post.attachments" v-if="attachment.type === 'documents'">
                                            <td><a :href="attachment.url" :download="attachment.name" data-popup="tooltip" :title="attachment.name"> <i :class="addClassForDocs(attachment.mime_type)"></i> @{{ attachment.name }}</a></td>
                                            <td v-text="attachment.author" data-popup="tooltip" :title="attachment.author"></td>
                                            <td v-text="attachment.created_at" data-popup="tooltip" :title="attachment.created_at"></td>
                                            <td>
                                                <ul class="list-condensed list-unstyled no-margin">
                                                    <li><span class="text-semibold">Size:</span> @{{ Math.round(attachment.size / 1024) + ' Kb' }}</li>
                                                    <li><span class="text-semibold">Format:</span> @{{ attachment.mime_type.substr(0, 5) }}</li>
                                                </ul>
                                            </td>
                                            <td class="text-center">
                                                <ul class="icons-list">
                                                    <li class="text-primary" data-popup="tooltip" title="download">
                                                        <a :href="attachment.url" :download="attachment.name"> <i class="icon-download"></i> </a>
                                                    </li>
                                                    <li class="text-danger">
                                                        <a href="#" @click="deleteAttachment(attachment.id)"> <i class="icon-trash"></i> </a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /media library -->
                                </div>
                            </div>
                            <!-- /all runtimes -->
                        </div>
                    </div>

                    <div class="tab-pane" id="highlighted-tab4">
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-xxs">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Eugene</td>
                                        <td>Kopyov</td>
                                        <td>@Kopyov</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Victoria</td>
                                        <td>Baker</td>
                                        <td>@Vicky</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>James</td>
                                        <td>Alexander</td>
                                        <td>@Alex</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Franklin</td>
                                        <td>Morrison</td>
                                        <td>@Frank</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Tab-content -->
                <div class="modal-footer">
                    <button type="submit" id="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save')</button>
                    <button type="button" class="btn btn-link btn-xs" data-dismiss="modal">@lang('labels.close')</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /large modal -->