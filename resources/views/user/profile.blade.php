@extends('layouts.app')

@section('title')
    <title>H4K | Profil</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/bootstrap_select.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src={{ asset("/js/user/profile.js") }}></script>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Profile</li>
    </ul>
@endsection
@section('content')
    <div class="row" id="vueProfile">
        <div class="tab-pane fade in active" id="profile">
            <!-- Profile info -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Profile information</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body" id="loading">
                    <form action="#" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.id')</label>
                                    <p type="text" class="form-control" readonly="readonly" role="button" v-text="user.id"></p>
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.title')</label>
                                    <select class="selectpicker form-control" name="title" v-model="user.title" required>
                                        <option value="0" selected="selected">Mr</option>
                                        <option value="1">Ms</option>
                                        <option value="2">Dr</option>
                                        <option value="3">Pr</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.username')</label>
                                    <input type="text" class="form-control input-xs" id="username" name="username" v-model="user.username">
                                    <label class="validation-error-label" for="email-error" v-if="form.errors.has('username')">
                                        <strong v-text="form.errors.get('username')"></strong>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.email')</label>
                                    <input type="email" class="form-control input-xs" id="email" name="email" v-model="user.email" required>
                                    <label class="validation-error-label" for="first_name-error" v-if="form.errors.has('email')">
                                        <strong v-text="form.errors.get('email')"></strong>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.first_name')</label>
                                    <input type="text" class="form-control input-xs" id="first_name" name="first_name" v-model="user.first_name" required>
                                    <label class="validation-error-label" for="last_name-error" v-if="form.errors.has('first_name')">
                                        <strong v-text="form.errors.get('first_name')"></strong>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.last_name')</label>
                                    <input type="text" class="form-control input-xs" id="last_name" name="last_name" v-model="user.last_name" required>
                                    <label class="validation-error-label" for="last_name-error" v-if="form.errors.has('last_name')">
                                        <strong v-text="form.errors.get('last_name')"></strong>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.tel') 1</label>
                                    <input type="tel" class="form-control input-xs" id="phone1" name="phone1" v-model="user.phone1" required>
                                    <label class="validation-error-label" for="last_name-error" v-if="form.errors.has('phone1')">
                                        <strong v-text="form.errors.get('phone1')"></strong>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.tel') 2</label>
                                    <input type="tel" class="form-control input-xs" id="phone2" name="phone2" v-model="user.phone2">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.occupation')</label>
                                    <input type="text" class="form-control" id="occupation" name="occupation" v-model="user.occupation">
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.language')</label>
                                    <select class="selectpicker form-control" data-live-search="true" name="language" v-model="user.language" required>
                                        <option value="fr" selected>Francais</option>
                                        <option value="en">Anglais</option>
                                        <option value="de">Allemand</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>@lang('labels.country')</label>
                                    <select class="selectpicker form-control" id="country"  data-live-search="true" name="country" data-size="8"  value="{{ old('country') }}"  v-model="user.country" required>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BA">Bosnia and Herzegowina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BV">Bouvet Island</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian Ocean Territory</option>
                                        <option value="BN">Brunei Darussalam</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM" selected>Cameroon</option>
                                        <option value="CA">Canada</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African Republic</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CX">Christmas Island</option>
                                        <option value="CC">Cocos (Keeling) Islands</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, the Democratic Republic of the</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Cote d'Ivoire</option>
                                        <option value="HR">Croatia (Hrvatska)</option>
                                        <option value="CU">Cuba</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Republic</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guinea</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Islands (Malvinas)</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesia</option>
                                        <option value="TF">French Southern Territories</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard and Mc Donald Islands</option>
                                        <option value="VA">Holy See (Vatican City State)</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IR">Iran (Islamic Republic of)</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KP">Korea, Democratic People's Republic of</option>
                                        <option value="KR">Korea, Republic of</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Lao People's Democratic Republic</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libyan Arab Jamahiriya</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macau</option>
                                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Islands</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia, Federated States of</option>
                                        <option value="MD">Moldova, Republic of</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="AN">Netherlands Antilles</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Mariana Islands</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">Reunion</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="KN">Saint Kitts and Nevis</option>
                                        <option value="LC">Saint LUCIA</option>
                                        <option value="VC">Saint Vincent and the Grenadines</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome and Principe</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SK">Slovakia (Slovak Republic)</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                                        <option value="ES">Spain</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SH">St. Helena</option>
                                        <option value="PM">St. Pierre and Miquelon</option>
                                        <option value="SD">Sudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="SY">Syrian Arab Republic</option>
                                        <option value="TW">Taiwan, Province of China</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania, United Republic of</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad and Tobago</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks and Caicos Islands</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emirates</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="US">United States</option>
                                        <option value="UM">United States Minor Outlying Islands</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Viet Nam</option>
                                        <option value="VG">Virgin Islands (British)</option>
                                        <option value="VI">Virgin Islands (U.S.)</option>
                                        <option value="WF">Wallis and Futuna Islands</option>
                                        <option value="EH">Western Sahara</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>@lang('labels.zipcode')</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" v-model="user.zipcode">
                                </div>
                                <div class="col-md-3">
                                    <label>@lang('labels.city')</label>
                                    <input type="text" class="form-control" id="city" name="city" v-model="user.city">
                                </div>
                                <div class="col-md-3">
                                    <label>@lang('labels.street')</label>
                                    <input type="text" class="form-control" id="street" name="street" v-model="user.street">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.fax')</label>
                                    <input type="text" class="form-control input-xs" id="fax" name="fax" v-model="user.fax">
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('labels.skype')</label>
                                    <input type="text" class="form-control input-xs" id="skype" name="skype" v-model="user.skype">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.interest')</label>
                                    <input type="text" class="form-control" id="interest" name="interest" v-model="user.interest">
                                    <span class="help-block">+99-99-9999-9999</span>
                                </div>

                                <div class="col-md-6">
                                    <label class="display-block">Upload profile image</label>
                                    <input type="file" class="file-styled">
                                    <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>@lang('labels.about')</label>
                                    <textarea rows="3" cols="5" class="form-control input-xs" id="about"  name="about" v-model="user.about"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save') <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /profile info -->


            <!-- Account settings -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Account settings</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Username</label>
                                    <input type="text" value="Kopyov" readonly="readonly" class="form-control">
                                </div>

                                <div class="col-md-6">
                                    <label>Current password</label>
                                    <input type="password" value="password" readonly="readonly" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>New password</label>
                                    <input type="password" placeholder="Enter new password" class="form-control">
                                </div>

                                <div class="col-md-6">
                                    <label>Repeat password</label>
                                    <input type="password" placeholder="Repeat new password" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Profile visibility</label>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="visibility" class="styled" checked="checked">
                                            Visible to everyone
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="visibility" class="styled">
                                            Visible to friends only
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="visibility" class="styled">
                                            Visible to my connections only
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="visibility" class="styled">
                                            Visible to my colleagues only
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Notifications</label>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" checked="checked">
                                            Password expiration notification
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" checked="checked">
                                            New message notification
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" checked="checked">
                                            New task notification
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled">
                                            New contact request notification
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn bg-teal-800 btn-xs" data-popup="tooltip" title=@lang('labels.save')>@lang('labels.save') <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /account settings -->

        </div>
    </div>
@endsection