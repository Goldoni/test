@extends('layouts.default')

@section('title')
    <title>H4K | Abonnement</title>
@endsection
@section('css')

@endsection
@section('js_before')

@endsection
@section('js_after')

@endsection

@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active"> @lang('menus.pricing')</li>
    </ul>
    @endsection
    @section('content')
            <!-- Pricing table one -->
    <h6 class="content-group text-semibold">
        Separate boxes
        <small class="display-block">Pricing tables grid</small>
    </h6>

    <div class="row pricing-table">
        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-body">
                    <h4>Basic</h4>
                    <h1 class="pricing-table-price">Gratuit</h1>
                    <ul class="list-unstyled content-group">
                        <li><strong>25GB</strong> space</li>
                        <li><strong>2GB</strong> RAM</li>
                        <li><strong>1</strong> domain</li>
                        <li><strong>5</strong> emails</li>
                        <li><strong>Daily</strong> backups</li>
                        <li><strong>24/7</strong> support</li>
                    </ul>
                    <a href="#" class="btn bg-success-400 btn-lg text-uppercase text-size-small text-semibold">Purchase</a>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-body">
                    <h4>Premium</h4>
                    <h1 class="pricing-table-price">Gratuit</h1>
                    <ul class="list-unstyled content-group">
                        <li><strong>75GB</strong> space</li>
                        <li><strong>4GB</strong> RAM</li>
                        <li><strong>10</strong> domains</li>
                        <li><strong>15</strong> emails</li>
                        <li><strong>Daily</strong> backups</li>
                        <li><strong>24/7</strong> support</li>
                    </ul>
                    <a href="#" class="btn bg-success-400 btn-lg text-uppercase text-size-small text-semibold">Purchase</a>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-body">
                    <h4>Professionnel</h4>
                    <h1 class="pricing-table-price">Gratuit</h1>
                    <ul class="list-unstyled content-group">
                        <li><strong>150GB</strong> space</li>
                        <li><strong>8GB</strong> RAM</li>
                        <li><strong>20</strong> domains</li>
                        <li><strong>50</strong> emails</li>
                        <li><strong>Daily</strong> backups</li>
                        <li><strong>24/7</strong> support</li>
                    </ul>
                    <a href="#" class="btn bg-danger-400 btn-lg text-uppercase text-size-small text-semibold">Purchase</a>

                    <div class="ribbon-container">
                        <div class="ribbon bg-danger-400">Popular</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-body">
                    <h4>Corporate</h4>
                    <h1 class="pricing-table-price"><span>$</span>1/Mois</h1>
                    <ul class="list-unstyled content-group">
                        <li><strong>256GB</strong> space</li>
                        <li><strong>16GB</strong> RAM</li>
                        <li><strong>Unlimited</strong> domains</li>
                        <li><strong>Unlimited</strong> emails</li>
                        <li><strong>Daily</strong> backups</li>
                        <li><strong>24/7</strong> support</li>
                    </ul>
                    <a href="#" class="btn bg-success-400 btn-lg text-uppercase text-size-small text-semibold">Purchase</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /pricing table one -->
@endsection
