@extends('layouts.default')

@section('title')
    <title>H4K | Boards Admin</title>
@endsection
@section('css')

@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/datatables.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/buttons.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/tables/datatables/extensions/responsive.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/ecommerce_customers.js") }}"></script>
@endsection

@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active"> @lang('menus.calendar')</li>
    </ul>
    @endsection
    @section('content')
            <!-- Customers -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Users List</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
        </div>

        <table class="table table-striped text-nowrap table-customers">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Username</th>
                <th>Pays</th>
                <th>Telephone</th>
                <th>Adresse</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach (json_decode($users) as $user)
                <tr>
                    <td>
                        <div class="media">
                            <a href="user_pages_profile_tabbed.html" class="media-left">
                                <img src="{{ asset("assets/images/placeholder.jpg")}}" width="40" height="40" class="img-circle img-md" alt="">
                            </a>

                            <div class="media-body media-middle">
                                <a href="user_pages_profile_tabbed.html" class="text-semibold">James Alexander</a>
                                <div class="text-muted text-size-small">
                                    Latest order: 2016.12.30
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>July 12, 2016</td>
                    <td><a href="#">james@interface.club</a></td>
                    <td>MasterCard</td>
                    <td>
                        <ul class="list list-unstyled no-margin">
                            <li class="no-margin">
                                <i class="icon-infinite text-size-base text-warning position-left"></i>
                                Pending:
                                <a href="#">25 orders</a>
                            </li>

                            <li class="no-margin">
                                <i class="icon-checkmark3 text-size-base text-success position-left"></i>
                                Processed:
                                <a href="#">34 orders</a>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <h6 class="no-margin text-semibold">&euro; 322.00</h6>
                    </td>
                    <td class="text-right">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu7"></i>
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-file-pdf"></i> Invoices</a></li>
                                    <li><a href="#"><i class="icon-cube2"></i> Shipping details</a></li>
                                    <li><a href="#"><i class="icon-credit-card"></i> Billing details</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-warning2"></i> Report problem</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                    <td class="no-padding-left"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /customers -->
@endsection
