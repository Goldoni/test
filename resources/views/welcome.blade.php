@extends('layouts.cecam')

@section('title')
    <title>CECAM HH | @lang('menus.home')</title>
@endsection

@section('content')
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="{{ route('post') }}"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="{{ route('post') }}"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="{{ route('post') }}">How to Train Your Dog to Wear  </a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>12</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Dog Training Tips - How To Train a Dog</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>08</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Do You Give Your Outdoor Dog Super </a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>03</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Tips & Advice For Dog Lovers</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>10</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Dog Health Care | Veterinary Advice</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>18</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="{{ asset("img/cecam/blog/meta/1.png") }}" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">10 Tips & Advice For Dog Lovers</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>18</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="blog-others floatright mt-20">
                <ul>
                    <li><a href="#">01</a></li>
                    <li><a href="#">02</a></li>
                    <li><a href="#">03</a></li>
                    <li><a href="#">04</a></li>
                    <li><a href="#">05</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')

@endsection