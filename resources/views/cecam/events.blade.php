@extends('layouts.cecam')

@section('title')
	<title>CECAM | Events</title>
@endsection

@section('content')


	<div class="col-md-12">

		<div class="about-content">

			{!! $calendar->calendar() !!}
			{!! $calendar->script() !!}
		</div>
	</div>
@endsection

@section('sidebar')

@endsection