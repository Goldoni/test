@extends('layouts.cecam')

@section('title')
	<title>CECAM | About</title>
@endsection

@section('content')
	<div class="col-md-12">

		<div class="about-content">
			{!! $page[0]['content'] !!}
		</div>
	</div>
@endsection

@section('sidebar')

@endsection