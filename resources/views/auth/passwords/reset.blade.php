<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>H4K | Reset Password</title>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/icons/icomoon/styles.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/bootstrap.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/core.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/components.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/colors.css") }} rel="stylesheet" type="text/css">
    <link href={{ asset("assets/css/custom.css") }} rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src={{ asset("assets/js/plugins/loaders/pace.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/core/libraries/jquery.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/core/libraries/bootstrap.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/plugins/loaders/blockui.min.js") }}></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src={{ asset("assets/js/plugins/forms/styling/uniform.min.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/plugins/forms/selects/bootstrap_select.min.js") }}></script>

    <script type="text/javascript" src={{ asset("assets/js/core/app.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/pages/login.js") }}></script>
    <script type="text/javascript" src={{ asset("assets/js/pages/form_bootstrap_select.js") }}></script>
    <!-- /theme JS files -->

</head>

<body class="login-container login-cover">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                @include('errors')
                <!-- Password recovery -->
                <form role="form" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-info text-info"><i class="icon-unlocked2"></i></div>
                            <h5 class="content-group">Password Reset</h5>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control input-xs"  placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-mail5 text-muted"></i>
                            </div>

                            @if ($errors->has('email'))
                                <label class="validation-error-label" for="email-error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </label>
                            @endif
                        </div>
                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" id="password" class="form-control input-xs" placeholder="Password" name="password" required="required">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>

                            @if ($errors->has('password'))
                                <label class="validation-error-label" for="email-error">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </label>
                            @endif
                        </div>
                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" id="password-confirm" class="form-control input-xs" placeholder="Confirm Password" name="password_confirmation" required="required">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>

                            @if ($errors->has('password_confirmation'))
                                <label class="validation-error-label" for="email-error">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </label>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-xs bg-blue btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
                        <br><br>
                    </div>
                </form>
                <!-- /password recovery -->


                <!-- Footer -->
                <div class="footer text-muted text-center">
                    Copyright &copy; 2014. <a href="#">Limitless admin template</a> by <a href="http://interface.club">Eugene Kopyov</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
