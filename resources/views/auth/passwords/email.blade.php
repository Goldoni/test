@extends('layouts.cecam')

@section('title')
    <title>CECAM</title>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="contact-wrap">
            <h1 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h1>
            <div class="from-style">
                <div class="cf-msg"></div>
                @include('errors')
                        <!-- Password recovery -->
                <form role="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control input-xs"  placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-mail5 text-muted"></i>
                            </div>
                            @if ($errors->has('email'))
                                <label class="validation-error-label" for="email-error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </label>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-xs bg-blue btn-block">Send Password Reset Link <i class="icon-arrow-right14 position-right"></i></button>
                        <br><br>
                        <a href="{{ route('login') }}" class="btn btn-xs bg-blue btn-block">Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection