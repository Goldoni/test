@extends('layouts.cecam')

@section('title')
    <title>CECAM</title>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="contact-wrap">
            <h2 class="section-title">LOGIN CECAM HH</h2>
            <p>Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V</p>
            <div class="from-style">
                @include('errors')
                <form  role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                        <div class="col-sm-12 col-xs-12{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block"> <strong>{{ $errors->first('email') }}</strong>  </span>
                            @endif
                        </div>


                        <div class="col-sm-12 col-xs-12{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span>
                            @endif
                        </div>

                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="cont-submit button"> Login </button>
                            <a class="btn btn-link" href="{{ route('password.request') }}"> Forgot Your Password?  </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection