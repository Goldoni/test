@extends('layouts.app')

@section('title')
    <title>H4K | @lang('menus.my_kinds')</title>
@endsection
@section('css')
    <link href={{ asset("../assets/global/css/components.css") }} rel="stylesheet" type="text/css">
@endsection
@section('js_before')
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/wizards/steps.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/forms/selects/select2.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/plugins/media/fancybox.min.js") }}"></script>
@endsection
@section('js_after')
    <script type="text/javascript" src="{{ asset("assets/js/pages/ecommerce_product_list.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/js/pages/components_notifications_pnotify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("/js/user/kid/kid.js") }}"></script>
@endsection
@section('sidebar-mobile-detached-toggle')
    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
@endsection
@section('sidebar-mobile-detached-toggle-link')
    <li> <a class="sidebar-control sidebar-detached-hide hidden-xs">  <i class="icon-drag-right"></i> </a> </li>
@endsection
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> @lang('menus.home')</a></li>
        <li class="active">@lang('menus.my_kinds')</li>
    </ul>
@endsection
@section('content')
    <div id="vueKids" v-cloak>
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- List -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Data Governance</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Eugene Kopyov</a></li>
                                                <li>Nov 1st, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$49.99</h5>
                                    </div>

                                    <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate.</p>
                                    Oh goodness jeez trout distinct hence cobra despite taped laughed. One morning, when Gregor <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 382</li>
                                        <li><i class="icon-alarm position-left"></i> 60 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(49)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Java language</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Malcolm Davis</a></li>
                                                <li>Oct 25th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$54.90</h5>
                                    </div>

                                    <p>"How about if I sleep a little bit longer and forget all this nonsense", he thought, but that was something.</p>
                                    However hard he threw himself onto his right, he always rolled back to where he was. He must <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 544</li>
                                        <li><i class="icon-alarm position-left"></i> 90 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(53)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Python language</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Mark Staters</a></li>
                                                <li>Oct 26th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$89.90</h5>
                                    </div>

                                    <p>Anyone or become friendly with them. It can all go to Hell!" He felt a slight itch up on his belly.</p>
                                    Headboard so that he could lift his head better; found where the itch was, and saw that it  <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 64</li>
                                        <li><i class="icon-alarm position-left"></i> 60 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(654)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">LESS language</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">James Alexander</a></li>
                                                <li>Nov 3rd, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring.</p>
                                    I am alone, and feel the charm of existence in this spot, which was created for the bliss <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 272</li>
                                        <li><i class="icon-alarm position-left"></i> 15 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(12)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Data Management</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Jeremy Victorino</a></li>
                                                <li>Nov 4th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$79.99</h5>
                                    </div>

                                    <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was.</p>
                                    When, while the lovely valley teems with vapour around me, and the meridian sun strikes upper <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 34</li>
                                        <li><i class="icon-alarm position-left"></i> 80 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(8)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Web Development</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Margo Baker</a></li>
                                                <li>Nov 5th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$39.99</h5>
                                    </div>

                                    <p>I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth.</p>
                                    Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 84</li>
                                        <li><i class="icon-alarm position-left"></i> 60 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(36)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">SASS language</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Monica Smith</a></li>
                                                <li>Nov 6th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$44.90</h5>
                                    </div>

                                    <p>Who formed us in his own image, and the breath of that universal love which bears and sustains us.</p>
                                    When darkness overspreads my eyes, and heaven and earth seem to dwell in my soul and absorb its <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 183</li>
                                        <li><i class="icon-alarm position-left"></i> 30 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(96)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Office Management</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Bastian Miller</a></li>
                                                <li>Nov 7th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>Oh, would I could describe these conceptions, could impress upon paper all that is living so full and.</p>
                                    Oh my friend -- but it is too much for my strength -- I sink under the weight of the splendour <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 93</li>
                                        <li><i class="icon-alarm position-left"></i> 40 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(57)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">PHP language</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Jordana Mills</a></li>
                                                <li>Nov 8th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was.</p>
                                    When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 301</li>
                                        <li><i class="icon-alarm position-left"></i> 20 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(19)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Software testing</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Buzz Brenson</a></li>
                                                <li>Nov 9th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs thin.</p>
                                    "What's happened to me?" he thought. It wasn't a dream. His room, a proper human room although <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 48</li>
                                        <li><i class="icon-alarm position-left"></i> 90 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(4)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Server management</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Zachary Willson</a></li>
                                                <li>Nov 10th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$84.90</h5>
                                    </div>

                                    <p>A collection of textile samples lay spread out on the table - Samsa was a travelling salesman.</p>
                                    And above it there hung a picture that he had recently cut out of an illustrated magazine and <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 43</li>
                                        <li><i class="icon-alarm position-left"></i> 100 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(64)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Database management</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">William Miles</a></li>
                                                <li>Nov 11th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$112.50</h5>
                                    </div>

                                    <p>However hard he threw himself onto his right, he always rolled back to where he was fully right.</p>
                                    Travelling day in and day out. Doing business like this takes much more effort than doing <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 293</li>
                                        <li><i class="icon-alarm position-left"></i> 120 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(86)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Graphic design</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Freddy Walden</a></li>
                                                <li>Nov 12th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$76.00</h5>
                                    </div>

                                    <p>Gregor then turned to look out the window at the dull weather. Drops of rain could be heard hitting.</p>
                                    "How about if I sleep a little bit longer and forget all this nonsense", he thought, but that <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 419</li>
                                        <li><i class="icon-alarm position-left"></i> 160 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(38)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">User experience</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Dori Laperriere</a></li>
                                                <li>Nov 13th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>He felt a slight itch up on his belly; pushed himself slowly up on his back towards the headboard.</p>
                                    So that he could lift his head better; found where the itch was, and saw that it was covered <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 190</li>
                                        <li><i class="icon-alarm position-left"></i> 200 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <i class="icon-star-empty3 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(193)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Human relationships</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Vanessa Aurelius</a></li>
                                                <li>Nov 14th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-danger-300 text-semibold media-right no-margin-bottom">Free</h6>
                                    </div>

                                    <p>Which he didn't know what to make of; and when he tried to feel the place with one of his legs he drew.</p>
                                    It quickly back because as soon as he touched it he was overcome by a cold shudder. He slid <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 205</li>
                                        <li><i class="icon-alarm position-left"></i> 80 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(128)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Leadership</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Nathan Jacobson</a></li>
                                                <li>Nov 15, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$55.00</h5>
                                    </div>

                                    <p>"Getting up early all the time", he thought, "it makes you very stupid. You've got to get enough sleep.</p>
                                    For instance, whenever I go back to the guest house during the morning to copy out the contract <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 82</li>
                                        <li><i class="icon-alarm position-left"></i> 90 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(27)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Business development</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Barbara Walden</a></li>
                                                <li>Nov 16th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$95.90</h5>
                                    </div>

                                    <p>But who knows, maybe that would be the best thing for me. If I didn't have my parents to think about.</p>
                                    I'd have gone up to the boss and told him just what I think, tell him everything I would let <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 67</li>
                                        <li><i class="icon-alarm position-left"></i> 80 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-half text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(59)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                            <div class="panel-body">
                                <div class="thumb">
                                    <a href="#course_preview" data-toggle="modal">
                                        <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded" alt="">
                                        <span class="zoom-image"><i class="icon-play3"></i></span>
                                    </a>
                                </div>

                                <div class="blog-preview">
                                    <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                                        <div class="media-body">
                                            <h5 class="text-semibold no-margin"><a href="#" class="text-default">Business operations</a></h5>

                                            <ul class="list-inline list-inline-separate no-margin text-muted">
                                                <li>by <a href="#">Craig Johnson</a></li>
                                                <li>Nov 17th, 2016</li>
                                            </ul>
                                        </div>

                                        <h5 class="text-success media-right no-margin-bottom text-semibold">$89.90</h5>
                                    </div>

                                    <p>And it's a funny sort of business to be sitting up there at your desk, talking down at your subordinates.</p>
                                    Well, there's still some hope; once I've got the money together to pay off my parents' debt <a href="#">[...]</a>
                                </div>
                            </div>

                            <div class="panel-footer panel-footer-condensed">
                                <div class="heading-elements">
                                    <ul class="list-inline list-inline-separate heading-text">
                                        <li><i class="icon-users position-left"></i> 42</li>
                                        <li><i class="icon-alarm position-left"></i> 90 hours</li>
                                        <li>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <i class="icon-star-full2 text-size-base text-warning-300"></i>
                                            <span class="text-muted position-right">(432)</span>
                                        </li>
                                    </ul>

                                    <a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /list -->


                <!-- Pagination -->
                <div class="text-center content-group-lg pt-20">
                    <ul class="pagination">
                        <li class="disabled"><a href="#"><i class="icon-arrow-small-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><i class="icon-arrow-small-right"></i></a></li>
                    </ul>
                </div>
                <!-- /pagination -->

            </div>
        </div>
        <!-- /detached content -->


        <!-- Detached sidebar -->
        <div class="sidebar-detached">
            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- Categories -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Categories</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <div class="has-feedback has-feedback-left form-group">
                                <input type="search" class="form-control" placeholder="Search...">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-small text-muted"></i>
                                </div>
                            </div>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion navigation-sm no-padding-top">
                                <li>
                                    <a href="#">Street wear</a>

                                    <ul>
                                        <li><a href="#">Hoodies</a></li>
                                        <li><a href="#">Jackets</a></li>
                                        <li class="active"><a href="#">Pants</a></li>
                                        <li><a href="#">Shirts</a></li>
                                        <li><a href="#">Sweaters</a></li>
                                        <li><a href="#">Tank tops</a></li>
                                        <li><a href="#">Underwear</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Snow wear</a>

                                    <ul>
                                        <li><a href="#">Fleece jackets</a></li>
                                        <li><a href="#">Gloves</a></li>
                                        <li><a href="#">Ski jackets</a></li>
                                        <li><a href="#">Ski pants</a></li>
                                        <li><a href="#">Snowboard jackets</a></li>
                                        <li><a href="#">Snowboard pants</a></li>
                                        <li><a href="#">Technical underwear</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Shoes</a>

                                    <ul>
                                        <li><a href="#">Laces</a></li>
                                        <li><a href="#">Sandals</a></li>
                                        <li><a href="#">Skate shoes</a></li>
                                        <li><a href="#">Slip ons</a></li>
                                        <li><a href="#">Sneakers</a></li>
                                        <li><a href="#">Winter shoes</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Accessories</a>

                                    <ul>
                                        <li><a href="#">Beanies</a></li>
                                        <li><a href="#">Belts</a></li>
                                        <li><a href="#">Caps</a></li>
                                        <li><a href="#">Sunglasses</a></li>
                                        <li><a href="#">Headphones</a></li>
                                        <li><a href="#">Video cameras</a></li>
                                        <li><a href="#">Wallets</a></li>
                                        <li><a href="#">Watches</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /categories -->


                    <!-- Filter -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>Filter products</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content">
                            <form action="#">
                                <div class="form-group">
                                    <div class="has-feedback has-feedback-left form-group">
                                        <input type="search" class="form-control" placeholder="Search brand">
                                        <div class="form-control-feedback">
                                            <i class="icon-search4 text-size-small text-muted"></i>
                                        </div>
                                    </div>

                                    <div class="has-scroll">
                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                686
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                A.Lab
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Adidas
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                ALIS
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Analog
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Burton
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Atomic
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Armada
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                O'Neill
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Baja
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Baker
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Blue Parks
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Billabong
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Bonfire
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                            <label class="display-block">
                                                <input type="checkbox" class="styled">
                                                Brixton
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Items for</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Men
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Women
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Kids
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Unisex
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Size</legend>

                                    <div class="row row-labels">
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXS</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XS</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">S</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-warning text-warning-800">M</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">L</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XL</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXL</a></div>
                                        <div class="col-xs-3"><a href="#" class="label label-flat border-grey text-grey-800">XXXL</a></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Fit</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Slim fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Skinny fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Regular fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Loose fit
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Long cut
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Color</legend>

                                    <div class="row row-colors">
                                        <div class="col-xs-4">
                                            <a href="#" class="bg-primary"></a>
                                            <span>Blue</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-warning"></a>
                                            <span>Orange</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-teal"></a>
                                            <span>Teal</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-pink">
                                                <i class="icon-checkmark3"></i>
                                            </a>
                                            <span>Pink</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-grey-800"></a>
                                            <span>Black</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-purple"></a>
                                            <span>Purple</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-success"></a>
                                            <span>Green</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-danger"></a>
                                            <span>Red</span>
                                        </div>

                                        <div class="col-xs-4">
                                            <a href="#" class="bg-info"></a>
                                            <span>Cyan</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Features</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Crew neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Chest pocket
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Raglan sleeves
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Polo neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            V-neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            High collar
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Hood
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Button strip
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Wide neck
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Kangaroo pocket
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <legend class="text-size-mini text-muted no-border no-padding">Material features</legend>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Canvas
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Lined
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Merino
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Quick drying
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Stretch
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Water repellent
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label class="display-block">
                                            <input type="checkbox" class="styled">
                                            Windproof
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn bg-blue btn-block">Filter</button>
                            </form>
                        </div>
                    </div>
                    <!-- /filter -->

                </div>
            </div>
        </div>
        <!-- /detached sidebar -->
    </div>



@endsection
