@extends('layouts.cecam')

@section('title')
	<title>CECAM | Events</title>
@endsection

@section('content')
	<div class="col-md-12">

		<div class="about-content">

			<h2 class="section-title">Civil Engagement of Cameroonians in Hamburg (CECAM HH) e.V
				(Vereinsregister VR 23226  - Amtsgericht Hamburg)</h2>

			Lorem ipsum dolor sit amet, mei cu sale accusamus definiebas, mel ut soluta labores recusabo. Ei vis aeterno epicuri electram. Ut enim hinc salutatus has, agam appareat eleifend et per, pertinacia comprehensam nec et. Luptatum conceptam has cu, ad nam solet persius consequuntur.

			Omnis phaedrum te vis, cum ne hinc definitiones, sea no modus verterem assueverit. Id case melius sed. Ut per nibh dicta. Appareat voluptatum sit an, eirmod bonorum expetendis mei eu, mei ornatus denique platonem ut. No etiam expetendis sed, dicta tamquam noluisse ius an.

			Nulla graeco vidisse has ex, sed ne fierent albucius percipitur. Vix te munere percipitur, vim natum forensibus conclusionemque ea, sit an ignota vivendo forensibus. Usu ea quod consul, probo dolorum fastidii ne nec. Eu dicant feugiat inermis per, alia phaedrum eum ei.


			<h2 class="section-title">Fachausschüsse</h2>

			<ul>
				<li><i class="fa fa-check"></i>Fachausschuss für  Familie, Jugendarbeit und Sport</li>
				<li><i class="fa fa-check"></i>Fachausschuss für Presse/Öffentlichkeitsarbeit</li>
				<li><i class="fa fa-check"></i>Fachausschuss für Technologie, Studium und Berufsausbildung</li>
				<li><i class="fa fa-check"></i>Fachausschuss für  Business, Marketing und Events</li>
			</ul>

			<p class="text-justify">
				Cu vix doctus aperiri feugiat, sed ne commodo reformidans, nam omnes graeci honestatis ex. Ex solet conclusionemque quo, ei enim scaevola has, debitis denique tincidunt cu eos. Sit constituto assueverit cu. Vix ut enim tibique, sint appetere nec et. Veri malis persecuti in vim, at nullam pertinacia sea, tamquam oporteat duo in.

				Graeco persecuti no eum. In tamquam prodesset tincidunt mea. Eos iudicabit maiestatis interesset et, mei ei verear molestiae, cu pro summo tation habemus. No vis laudem ceteros honestatis. Ex per modus sanctus scriptorem, est blandit ullamcorper no, offendit phaedrum qui ad. Idque atqui prodesset eam no.
			</p>


			<p class="text-justify">
				Per vidisse suavitate no, ius habeo constituto quaerendum an. Ad facete sanctus deterruisset per, utamur facilis delectus ad vim. Per no mundi possim, eu etiam decore detracto nam. Nisl sumo in cum. Amet recteque sapientem per ne, ius nonumy gloriatur deterruisset eu, quo agam inermis partiendo in.

				His causae civibus in, ea usu possit voluptatum. Ius ad brute.
			</p>


		</div>
	</div>
@endsection

@section('sidebar')

@endsection