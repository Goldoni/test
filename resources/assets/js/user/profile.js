class Errors{
    constructor(){
        this.errors = {};
    }
    get(field){
        if(this.errors[field]){
            return this.errors[field][0]
        }
    }
    clear(field){
        if( this.errors[field] )  delete  this.errors[field]

    }
    has(field){
        return this.errors.hasOwnProperty(field)
    }
    any(){
        return Object.keys(this.errors).length > 0;
    }
    record(errors){
        this.errors = errors;
    }
}

class Lang {
    constructor(){
        this.lang = {}
    }
    record(lang){
        this.lang = lang;
    }
    get(field){
        if(this.lang[field]){
            return this.lang[field]
        }
    }
}

class Multiple {
    constructor () {
        this.multiple = {}
    }
    record (target, records) {
        this.multiple = records
        target.val(records)
        target.bootstrapDualListbox('refresh')
    }
    get (target) {
        return target.val()
    }
}

class Form {
    constructor(data){
        this.originalData = data;
        for (let field in data ) {
            this[field] = data[field];
        }
        this.errors = new Errors();
        this.lang   = new Lang();
        this.multiple   = new Multiple();
    }
    view (show) {
        if(show){
            $('#modal_large_view').modal('show');
        } else {
            $('#modal_large_view').modal('hide');
        }
    }
    edit (show) {
        if(show){
            $('#modal_large').modal('show');
        } else {
            $('#modal_large').modal('hide');
        }
    }
    load (loading) {
        if ( loading ){
            $('#loading').block({
                message: '<i class="icon-spinner9 spinner"></i> Chargement...',
                overlayCSS: {
                    backgroundColor: '#1B2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        } else {
            $('#loading').unblock();
        }

    }
    reset () {

    }
    notify (type, message) {

        switch (type) {
            case 'success':
                new PNotify({ title: type, text: message, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            case 'error':
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            default:
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
        }
    }
    selected (val, trigger) {
        trigger.selectpicker('val',val);
        trigger.selectpicker('refresh');
    }
}
require('../bootstrap');
Vue.use(Vuex);

let vueUsers = new Vue({
    el: '#vueProfile',
    components: { },
    data: {
        form : new Form(),
        user: [],
        roles: [],
    },
    mounted ()  {
        this.view()
    },
    watch: {
        id(value){

        }
    },
    methods: {
        view() {
            this.$http.get('profile')
                .then(this.onView)
                .catch(this.onFailed);
        },
        edit(id) {
            this.$http.get('users/view/' + id)
                .then(this.onEdit)
                .catch(this.onFailed);
        },
        create(id) {
            this.$http.get('users/create')
                .then(this.onEdit)
                .catch(this.onFailed);
        },
        onSubmit() {
            this.form.load(true)
            this.user.roles = this.form.multiple.get($('#roles'))
            this.user.boards = this.form.multiple.get($('#boards'))

            this.$http.put('users/update', this.user)
                .then(this.onUpdated)
                .catch(this.onFailed)
        },
        onView(response){
            this.user = response.data.user
            this.form.lang.record(response.data.lang)
            this.form.selected(response.data.user.language, $('#language'))
            this.form.selected(response.data.user.country, $('#country'))
            this.form.view(true)
        },
        onUpdated(response){
            this.form.load(false)
            this.form.notify('success','User saved successfully.')
        },
        onEdit(response){
            debugger
            this.user = response.data.user
            this.form.edit(true)
            this.form.multiple.record($('#roles'), response.data.pluckedRoles)
            this.form.multiple.record($('#boards'), response.data.pluckedBoards)
            this.form.selected(response.data.user.language, $('#language'))
            this.form.selected(response.data.user.country, $('#country'))

        },
        onFailed(error) {
            if (error.response.data) {
                this.form.errors.record(error.response.data)
                this.form.load(false)
            } else {
                console.log(error)
            }
        },
        destroy(id) {
            //error => this.form.errors.record(error.response.data)
        }
    }

});
$(function() {
    $('#language').on('changed.bs.select', function (e) {
        vueUsers.user.language = $(this).selectpicker('val');
    });
    $('#country').on('changed.bs.select', function (e) {
        vueUsers.user.country = $(this).selectpicker('val');
    });
    var edit = function () {
        debugger
    }
})
