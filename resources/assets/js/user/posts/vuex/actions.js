export const addPosts = function (store, posts) {
    store.commit('ADD_POSTS', posts)
};

export const addPagination = function (store, pagination) {
    store.commit('ADD_PAGINATION', pagination)
};

export const addFiltersDate = function (store, date) {
    store.commit('ADD_FILTERS_DATE', date)
};

export const addMode = function (store, mode) {
    store.commit('ADD_MODE', mode)
};

export const addCategories = function (store, categories) {
    store.commit('ADD_CATEGORIES', categories)
};

export const addCategoryId = function (store, category_id) {
    store.commit('ADD_CATEGORY_ID', category_id)
};

export const addSearch = function (store, search) {
    store.commit('ADD_SEARCH', search)
};

export const addSearchPosts = function (store, global_search) {
    store.commit('ADD_SEARCH_POSTS', global_search)
};

export const addCurrentPost = function (store, currentPost) {
    store.commit('ADD_CURRENT_POST', currentPost)
};

export const addFormErrors = function (store, errors) {
    store.commit('ADD_FORM_ERRORS', errors)
};

export const addEditPost = function (store, post) {
    store.commit('EDIT_POST', post)
};

export const formModal = function (store, modal) {
    modal.show ? $('#'+ modal.target).modal('show') : $('#'+ modal.target).modal('hide')
    store.commit('FORM_MODAL', modal)
};

export const toggleLoading = function (store, loading, target) {
    if (loading) {
        $('#grid').block({
            message: '<i class="icon-spinner9 spinner"></i> Chargement...',
            overlayCSS: {
                backgroundColor: '#1B2024',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

    } else {
        $('#grid').unblock();
    }
    store.commit('LOADING', loading)
};

export const loading = function (store, load) {
    if (load.show) {
        $(load.target).block({
            message: '<i class="icon-spinner9 spinner"></i> Chargement...',
            overlayCSS: {
                backgroundColor: '#1B2024',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

    } else {
        $(load.target).unblock();
    }
    store.commit('LOAD', loading)
};