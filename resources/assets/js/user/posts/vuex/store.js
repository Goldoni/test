require('es6-promise').polyfill();
import "babel-polyfill";
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.config.debug = true
import * as actions from './actions'
import * as getters from './getters'

const state = {
    posts: [],
    currentPost: [],
    categories: [],
    pagination: {
        perPage: 8,
        nbPage: 1,
        page: 1,
    },
    filters: {
        date: {},
    },
    load: {
        show : false,
        target : ''
    },
    mode : true,
    loading: false,
    search: '',
    category_id : null,
    global_search : '',
    form: {
        target:'',
        modal: {
            target : '',
            show : ''
        },
        busy: false,
        success: false,
        errors: [],
    }
}

const mutations = {
    ADD_POSTS (state, posts) {
        state.posts = posts
    },
    ADD_CURRENT_POST (state, currentPost) {
        state.currentPost = currentPost
    },
    ADD_CATEGORIES (state, categories) {
        state.categories = categories
    },
    ADD_FORM_ERRORS (state, errors) {
        state.form.errors = errors
    },
    FORM_MODAL (state, modal) {
        state.form.modal = modal
    },
    ADD_CATEGORY_ID (state, category_id) {
        state.category_id = category_id
    },
    ADD_POST (state, post) {
        state.posts.push(post);
    },
    EDIT_POST (state, post) {
        let c = state.posts.find((c) => c.id === post.id)
        c.name = post.name;
        c.content = post.content;
        c.category_id = post.category_id;
        c.tags = post.tags;
        c.category = post.category;
        c.comments = post.comments;
        c.attachments = post.attachments;

    },
    ADD_PAGINATION (state, pagination) {
        state.pagination = pagination;
    },
    LOADING(state, loading){
        state.loading = loading
    },
    LOAD(state, load){
        state.load.show = load.show
        state.load.target = load.target
    },
    ADD_FILTERS_DATE(state, date){
        state.filters.date = date
    },
    ADD_MODE(state, mode){
        state.mode = mode
    },
    ADD_SEARCH (state, search) {
        state.search = search
    },
    ADD_SEARCH_POSTS (state, global_search) {
        state.global_search = global_search
    },
}


export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})