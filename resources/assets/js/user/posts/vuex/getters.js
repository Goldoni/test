export const allPosts = (state) => state.posts

export const allPagination = (state) => state.pagination

export const getDate = (state) => state.filters.date

export const getMode = (state) => state.mode

export const getCategories = (state) => state.categories

export const getSearch = (state) => state.search

export const getCategoryId = (state) => state.category_id

export const getSearchPosts = (state) => state.global_search

export const getCurrentPost = (state) => state.currentPost

export const getFormErrors = (state) => state.form.errors