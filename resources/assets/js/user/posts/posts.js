require('../../bootstrap');


import store from './vuex/store'
import  Posts from  './components/Posts.vue';
import Post from './components/Posts/Post.vue';
import  Pagination from  './components/Pagination.vue';
import  Tools from  './components/Tools.vue';
import  Categories from  './components/Categories.vue';
import FloatingButton from './components/FloatingButton.vue'


window.vuePosts = new Vue({
    el: '#vuePosts',
    store,
    components: {Posts, Post, Pagination, FloatingButton, Tools, Categories},

    data: {},

    mounted() {

    },

    computed: {
        ...Vuex.mapGetters({
            mode: 'getMode',
            post: 'getCurrentPost',
            errors: 'getFormErrors',
        }),
    },
    watch: {
        post(value){
            this.bindTagsForCurrentPost(value.tags)
            this.selected(value.category_id, $('#category'))
        }
    },
    updated () {
        this.addPlupload()
        this.setTinyMCE()
        tinymce.get('post_content').setContent(this.post.content);
    },
    methods: {
        ...Vuex.mapActions({
            addFormErrors : 'addFormErrors',
            addCurrentPost : 'addCurrentPost',
            toggleLoading : 'toggleLoading',
            addEditPost : 'addEditPost',
            loading : 'loading',
            modal  :  'formModal',
        }),
        getAllPosts() {
            this.$http.get('posts')
                .then(this.onAllPosts)
                .catch(this.onFailed);
        },
        onSubmit() {
            this.post.tags = this.getTagsForCurrentPost()
            this.post.content = this.getTextForContent()
            this.$http.put('posts/update', this.post)
                .then(this.onPost)
                .catch(this.onFailed);
        },
        deleteAttachment(attachment_id) {
            this.loading({ show: true, target : '.table-files' })
            this.$http.delete('posts/attachments/delete/' + this.post.id + '/' + this.post.user_id + '/' + attachment_id)
                .then(this.addPost)
                .catch(this.onFailed);
        },

        addPost : function(response) {
            this.addCurrentPost(response.data.post)
            this.notify('success','Files deleted successfully.')
            this.loading({ show: false, target : '.table-files' })
        },

        bindTagsForCurrentPost: function (tags) {
            let result = ''
            for (let field in tags) {
                if (field == 0) {
                    result = result + '' + tags[field].name;
                } else {
                    result = result + ',' + tags[field].name;
                }
            }
            $('.tokenfield-teal').tokenfield('setTokens', result);
            return result
        },
        getTagsForCurrentPost: function () {
            let items = $('.tokenfield-teal').tokenfield('getTokens');
            let tags = []

            for (let field in items) {
                let item = {}
                item.tagable_id = this.post.id
                item.tagable_type = 'App\\Post';
                item.name = items[field].value;
                tags.push(item)
            }

            return tags;
        },

        onAllPosts(response){
            this.posts = response.data.posts
        },
        onPost(response){
            this.notify('success','User saved successfully.')
            this.addEditPost(response.data.post)
            this.modal({show : false, target : 'modal_large_edit_post'})
        },

        onFailed(error) {
            this.toggleLoading(false)
            this.loading({ show: false, target : '.table-files' })
            if (error.response.data) {
                this.addFormErrors(error.response.data)
                this.notify('error','All fields with (*) must be completed.')
            } else {
                console.log(error)
            }
        },
        selected (val, trigger) {
            trigger.selectpicker('val', val);
            trigger.selectpicker('refresh');
        },
        addClassForDocs (ext) {
            switch (ext) {
                case 'pdf':
                    return 'icon-file-pdf';
                    break;
                case 'vnd.openxmlformats-officedocument.presentationml.presentation':
                    return 'icon-file-spreadsheet';
                    break;
                case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
                    return 'icon-file-word';
                    break;
                case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    return 'icon-file-excel';
                    break;
                default:
                    return 'icon-file-empty';

            }
        },
        addPlupload () {
            // Setup all runtimes for Files
            var formData = document.querySelector('#file-uploader')
            $(".file-uploader").pluploadQueue({
                runtimes: 'html5, html4, Flash, Silverlight',
                url: 'attachments',
                chunk_size: '5mb',
                unique_names: true,
                filters: {
                    max_file_size: '10mb',
                    mime_types: [{
                        title: "Image files",
                        extensions: "jpg,jpeg,gif,png"
                    }]
                },
                headers: {
                    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
                },
                multipart_params: {
                    attachable_id : formData.dataset.id,
                    attachable_type : formData.dataset.type,
                    type : 'images',
                },
            });

            //Setup all runtimes for Docs
            $(".docs-uploader").pluploadQueue({
                runtimes: 'html5, html4, Flash, Silverlight',
                url: 'attachments/upload',
                chunk_size: '5mb',
                unique_names: true,
                filters: {
                    max_file_size: '10mb',
                    mime_types: [{
                        title: "Image files",
                        extensions: "xlsx,xls,docx,doc,pdf,pptx"
                    }]
                },
                headers: {
                    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
                },
                multipart_params: {
                    attachable_id : formData.dataset.id,
                    attachable_type : formData.dataset.type,
                    type : 'documents',
                },
            });
        },
        getTextForContent () {
            tinyMCE.triggerSave();
            return $('#post_content').val()
        },
        setTinyMCE() {
            let that = window.axios
            if(window.tinyMCE){
                var textarea = document.querySelector('#post_content')
                tinymce.init({
                    selector:'.editor',
                    height: 300,
                    theme: 'modern',
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code imagetools'
                    ],
                    toolbar1: 'undo redo  | bold italic underline | alignleft aligncenter alignright alignjustify |  link unlink image',
                    content_css: '//www.tinymce.com/css/codepen.min.css',
                    image_advtab: true,
                    paste_data_images:true,
                    images_reuse_filename: true,
                    automatic_uploads:true,
                    images_upload_handler: function (blobinfo,success,failure) {
                        var data = new FormData()
                        data.append('attachable_id', textarea.dataset.id);
                        data.append('attachable_type', textarea.dataset.type);
                        data.append('type', 'wysiwyg');
                        data.append('file', blobinfo.blob(), blobinfo.filename())
                        that.post('posts/attachments', data).then((response) => {
                            success(response.data.url)
                        }, (response) => {
                            failure('Error', response)
                            success('http://placehold.it/350x150')
                        });
                    },
                    file_browser_callback : function (field_name, url, type, win) {
                        tinyMCE.activeEditor.windowManager.open({
                            title: 'Browse Image',
                            file: "yourbrowser.php?field=" + field_name + "&url=" + url,
                            width: 450,
                            height: 305,
                            resizable : "yes",
                            inline : "yes",
                            close_previous : "no",
                            buttons: [{
                                text: 'Insert',
                                classes: 'widget btn primary first abs-layout-item',
                                disabled: true,
                                onclick: 'close'
                            }, {
                                text: 'Close',
                                onclick: 'close',
                                window : win,
                                input : field_name
                            }]
                        });

                    }
                });
            }
        },
        getTextForContent () {
            tinyMCE.triggerSave();
            return $('#post_content').val()
        },
        hasErrors(field){
            return this.errors.hasOwnProperty(field)
        },
        getErrors(field){
            if(this.errors[field]){
                return this.errors[field][0]
            }
        },
        clearErrors(field){
            if( this.errors[field] )  delete  this.errors[field]
        },
        notify (type, message) {

            switch (type) {
                case 'success':
                    new PNotify({ title: type, text: message, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                    break;
                case 'error':
                    new PNotify({ title: type, text: message, addclass: 'bg-warning-800',  icon: 'icon-warning22' });
                    break;
                default:
                    new PNotify({ title: type, text: message, addclass: 'bg-info-800',  icon: 'icon-info22' });
            }
        },
    }
});


$(function () {

    $(".datepicker").datepicker();


    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    $('#category').on('changed.bs.select', function (e) {
        vuePosts.post.category_id = $(this).selectpicker('val');
    });

    $('#fab-menu-affixed-demo-right').affix({
        offset: {
            top: $('#fab-menu-affixed-demo-right').offset().top - 20
        }
    });
    // Add class on init
    $('.tokenfield-teal').on('tokenfield:initialize', function (e) {
        $(this).parent().find('.token').addClass('bg-teal')
    });

    // Initialize plugin
    $('.tokenfield-teal').tokenfield();

    // Add class when token is created
    $('.tokenfield-teal').on('tokenfield:createdtoken', function (e) {
        $(e.relatedTarget).addClass('bg-teal')
    });





})

