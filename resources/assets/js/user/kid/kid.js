require('../../bootstrap');

import store from './vuex/store'
import  KidInfo from  './components/KidInfo.vue';



window.vueKids = new Vue({
    el: '#vueKids',
    store,
    components: { KidInfo },

    data: {
        kids: [],
    },

    mounted() {
        this.getAllKids()
    },

    methods: {
        getAllKids() {
            this.$http.get('kids/view')
                .then(this.onAllKids)
                .catch(this.onFailed);
        },

        onAllKids(response){
            this.kids = response.data.kids
        },

        onFailed(error) {
            if (error) {
                console.log(error)
            } else {
                console.log(error)
            }
        },
    }
});


$(function() {
    
    $(".datepicker").datepicker();


    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
})

