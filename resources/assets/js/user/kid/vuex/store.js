require('es6-promise').polyfill();
import "babel-polyfill";
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.config.debug = true
import * as actions from './actions'
import * as getters from './getters'

const state = {
    kid: [],
}

const mutations = {
    ADD_KID (state, kid) {
        state.kid = kid
    }
}


export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})