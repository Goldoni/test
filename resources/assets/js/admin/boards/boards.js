class Errors{
    constructor(){
        this.errors = {};
    }
    get(field){
        if(this.errors[field]){
            return this.errors[field][0]
        }
    }
    clear(field){
       if( this.errors[field] )  delete  this.errors[field]

    }
    has(field){
        return this.errors.hasOwnProperty(field)
    }
    any(){
        return Object.keys(this.errors).length > 0;
    }
    record(errors){
        this.errors = errors;
    }
}

class Lang {
    constructor(){
        this.lang = {}
    }
    record(lang){
        this.lang = lang;
    }
    get(field){
        if(this.lang[field]){
            return this.lang[field]
        }
    }
}

class Multiple {
    constructor () {
        this.multiple = {}
    }
    record (target, records) {
        this.multiple = records
        target.val(records)
        target.bootstrapDualListbox('refresh')
    }
    get (target) {
        return target.val()
    }
}

class Form {
    constructor(data){
        this.originalData = data;
        for (let field in data ) {
            this[field] = data[field];
        }
        this.errors = new Errors();
        this.lang   = new Lang();
        this.multiple   = new Multiple();
    }
    view (show) {
        if(show){
            $('#modal_large_view').modal('show');
        } else {
            $('#modal_large_view').modal('hide');
        }
    }
    edit (show) {
        if(show){
            $('#modal_large').modal('show');
        } else {
            $('#modal_large').modal('hide');
        }
    }
    load (loading) {
        if ( loading ){
            $('.modal-dialog').block({
                message: '<i class="icon-spinner9 spinner"></i> Chargement...',
                overlayCSS: {
                    backgroundColor: '#1B2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        } else {
            $('.modal-dialog').unblock();
        }

    }
    reset () {

    }
    notify (type, message) {

        switch (type) {
            case 'success':
                new PNotify({ title: type, text: message, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            case 'error':
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            default:
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
        }
    }
    selected (val, trigger) {
        trigger.selectpicker('val',val);
        trigger.selectpicker('refresh');
    }
}
require('../../bootstrap');
Vue.use(Vuex);
import store from './vuex/store'

let vueUsers = new Vue({
    el: '#vueBoards',
    components: { },
    data: {
        form : new Form({
            loading : false
        }),
        board: [],
        users: [],
        roles: [],
    },
    mounted ()  {
    },
    watch: {
        id(value){

        }
    },
    methods: {
        view(id) {
            this.$http.get('boards/show/' + id)
                .then(this.onView)
                .catch(this.onFailed);
        },
        edit(id) {
            this.$http.get('boards/show/' + id)
                    .then(this.onEdit)
                    .catch(this.onFailed);
        },
        create(id) {
            this.$http.get('users/create')
                    .then(this.onEdit)
                    .catch(this.onFailed);
        },
        onSubmit() {
            this.form.load(true)
            this.board.users = this.form.multiple.get($('#users'))

            this.$http.put('boards/update', this.board)
                    .then(this.onUpdated)
                    .catch(this.onFailed)
        },
        onView(response){
            this.user = response.data.user
            this.form.lang.record(response.data.lang)
            this.form.view(true)
        },
        onUpdated(response){
            this.form.load(false)
            this.form.edit(false)
            this.form.notify('success','Board saved successfully.')
        },
        onEdit(response){
            this.board = response.data.board
            this.form.edit(true)
            this.form.multiple.record($('#users'), response.data.plucked)
            // this.form.multiple.record($('#boards'), response.data.pluckedBoards)
            // this.form.selected(response.data.user.language, $('#language'))
            // this.form.selected(response.data.user.country, $('#country'))

        },
        onFailed(error) {
            if (error.response.data) {
                this.form.errors.record(error.response.data)
                this.form.load(false)
            } else {
                console.log(error)
            }
        },
        destroy(id) {
            //error => this.form.errors.record(error.response.data)
        }
    }

});
$(function() {
    $('#language').on('changed.bs.select', function (e) {
        vueUsers.user.language = $(this).selectpicker('val');
    });
    $('#country').on('changed.bs.select', function (e) {
        vueUsers.user.country = $(this).selectpicker('val');
    });
    $(".modal").draggable({
        handle: ".modal-header"
    });

    $('.context-dynamic').contextmenu({
        target: '.context-dynamic-menu',
        before: function(e,element) {
            //this.getMenu().find('li').eq(0).find('a').html('<i class="icon-eye4"></i> Show');
            //this.getMenu().find('li').eq(1).find('a').html('<i class="icon-pencil6"></i> Edit');
            //this.getMenu().find('li').eq(2).find('a').html('<i class="icon-trash"></i> Delete');
            //this.getMenu().find('li').eq(3).find('a').html('<i class="icon-user-check"></i> Login');
            return true;
        },
        onItem: function(context, e) {
            switch($(e.target).text().trim()) {
                case 'Show':
                    vueUsers.view($(context).attr('data-id').trim())
                    break;
                case 'Edit':
                    vueUsers.edit($(context).attr('data-id').trim())
                    break;
                case 'Delete':
                    //vueUsers.edit($(context).attr('data-id').trim())
                    break;
                case 'Add':
                    vueUsers.create()
                    break;
                case 'Login':
                    //return true;
                    break;
                default:

            }
        }
    });

    $('#users').bootstrapDualListbox({
        nonSelectedListLabel: 'Non-Choisi',
        selectedListLabel: 'Choisi',
        infoText: 'Showing all {0}',
        infoTextFiltered: '<span class="label label-warning">Filtered </span> {0} sur {1}',
        infoTextEmpty: 'Liste vide',
        filterPlaceHolder: 'Rechercher',
        filterTextClear: 'show all'
    });
    $('#boards').bootstrapDualListbox({
        nonSelectedListLabel: 'Non-Choisi',
        selectedListLabel: 'Choisi',
        infoText: 'Showing all {0}',
        infoTextFiltered: '<span class="label label-warning">Filtered </span> {0} sur {1}',
        infoTextEmpty: 'Liste vide',
        filterPlaceHolder: 'Rechercher',
        filterTextClear: 'show all'
    });
    var edit = function () {
        debugger
    }
})
