class Errors{
    constructor(){
        this.errors = {};
    }
    get(field){
        if(this.errors[field]){
            return this.errors[field][0]
        }
    }
    clear(field){
       if( this.errors[field] )  delete  this.errors[field]

    }
    has(field){
        return this.errors.hasOwnProperty(field)
    }
    any(){
        return Object.keys(this.errors).length > 0;
    }
    record(errors){
        this.errors = errors;
    }
}

class Lang {
    constructor(){
        this.lang = {}
    }
    record(lang){
        this.lang = lang;
    }
    get(field){
        if(this.lang[field]){
            return this.lang[field]
        }
    }
}

class Multiple {
    constructor () {
        this.multiple = {}
    }
    record (target, records) {
        this.multiple = records
        target.val(records)
        target.bootstrapDualListbox('refresh')
    }
    get (target) {
        return target.val()
    }
}

class Form {
    constructor(data){
        this.originalData = data;
        for (let field in data ) {
            this[field] = data[field];
        }
        this.errors = new Errors();
        this.lang   = new Lang();
        this.multiple   = new Multiple();
    }
    view (show) {
        if(show){
            $('#modal_large_view').modal('show');
        } else {
            $('#modal_large_view').modal('hide');
        }
    }
    edit (show) {
        if(show){
            $('#modal_large_view').modal('show');
        } else {
            $('#modal_large_view').modal('hide');
        }
    }
    load (loading) {
        if (loading) {
            $('.modal-dialog').block({
                message: '<i class="icon-spinner9 spinner"></i> Chargement...',
                overlayCSS: {
                    backgroundColor: '#1B2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        } else {
            $('.modal-dialog').unblock();
        }

    }
    reset () {

    }
    notify (type, message) {

        switch (type) {
            case 'success':
                new PNotify({ title: type, text: message, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            case 'error':
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
                break;
            default:
                new PNotify({ title: title, text: msg, addclass: 'bg-teal-400',  icon: 'icon-checkmark4' });
        }
    }
    selected (val, trigger) {
        trigger.selectpicker('val',val);
        trigger.selectpicker('refresh');
    }
}
require('../../bootstrap');
Vue.use(Vuex);
import store from './vuex/store'

let vueUsers = new Vue({
    el: '#vueUsers',
    store,
    components: { },
    data: {
        form : new Form({
            loading : false
        }),
        page: [],
    },
    mounted ()  {
    },
    updated () {
        this.addPlupload()
        this.setTinyMCE()
        tinymce.get('post_content').setContent(this.page.content);
    },
    watch: {
        id(value){

        }
    },
    methods: {
        view(id) {
            this.$http.get('users/view/' + id)
                .then(this.onView)
                .catch(this.onFailed);
        },
        edit(id) {
            this.$http.get('pages/view/' + id)
                    .then(this.onEdit)
                    .catch(this.onFailed);
        },
        create(id) {
            this.$http.get('users/create')
                    .then(this.onEdit)
                    .catch(this.onFailed);
        },
        onSubmit() {
            this.form.load(true)
            this.page.content = this.getTextForContent()
            this.$http.put('pages/update', this.page)
                    .then(this.onUpdated)
                    .catch(this.onFailed)
        },
        onView(response){
            this.user = response.data.user
            this.form.lang.record(response.data.lang)
            this.form.view(true)
        },
        onUpdated(response){
            this.form.load(false)
            this.form.edit(false)
            this.form.notify('success','User saved successfully.')
        },
        onEdit(response){
            this.page = response.data.page
            this.form.edit(true)

        },
        onFailed(error) {
            if (error.response.data) {
                this.form.errors.record(error.response.data)
                this.form.load(false)
            } else {
                console.log(error)
            }
        },
        destroy(id) {
            //error => this.form.errors.record(error.response.data)
        },
        addPlupload () {
            // Setup all runtimes for Files
        },
        setTinyMCE() {
            let that = window.axios
            if(window.tinyMCE){
                var textarea = document.querySelector('#post_content')
                tinymce.init({
                    selector:'.editor',
                    height: 300,
                    theme: 'modern',
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code imagetools'
                    ],
                    toolbar1: 'undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist |  link unlink image',
                    content_css: '//www.tinymce.com/css/codepen.min.css',
                    image_advtab: true,
                    paste_data_images:true,
                    images_reuse_filename: true,
                    automatic_uploads:true,
                    images_upload_handler: function (blobinfo,success,failure) {
                        var data = new FormData()
                        data.append('attachable_id', textarea.dataset.id);
                        data.append('attachable_type', textarea.dataset.type);
                        data.append('type', 'wysiwyg');
                        data.append('file', blobinfo.blob(), blobinfo.filename())
                        that.post('pages/attachments', data).then((response) => {
                            success(response.data.url)
                        }, (response) => {
                            failure('Error', response)
                            success('http://placehold.it/350x150')
                        });
                    },
                    file_browser_callback : function (field_name, url, type, win) {
                        tinyMCE.activeEditor.windowManager.open({
                            title: 'Browse Image',
                            file: "yourbrowser.php?field=" + field_name + "&url=" + url,
                            width: 450,
                            height: 305,
                            resizable : "yes",
                            inline : "yes",
                            close_previous : "no",
                            buttons: [{
                                text: 'Insert',
                                classes: 'widget btn primary first abs-layout-item',
                                disabled: true,
                                onclick: 'close'
                            }, {
                                text: 'Close',
                                onclick: 'close',
                                window : win,
                                input : field_name
                            }]
                        });

                    }
                });
            }
        },
        getTextForContent () {
            tinyMCE.triggerSave();
            return $('#post_content').val()
        },
    }

});
$(function() {
    $('#language').on('changed.bs.select', function (e) {
        vueUsers.user.language = $(this).selectpicker('val');
    });
    $('#country').on('changed.bs.select', function (e) {
        vueUsers.user.country = $(this).selectpicker('val');
    });
    $(".modal").draggable({
        handle: ".modal-header"
    });

    $('.context-dynamic').contextmenu({
        target: '.context-dynamic-menu',
        before: function(e,element) {
            //this.getMenu().find('li').eq(0).find('a').html('<i class="icon-eye4"></i> Show');
            //this.getMenu().find('li').eq(1).find('a').html('<i class="icon-pencil6"></i> Edit');
            //this.getMenu().find('li').eq(2).find('a').html('<i class="icon-trash"></i> Delete');
            //this.getMenu().find('li').eq(3).find('a').html('<i class="icon-user-check"></i> Login');
            return true;
        },
        onItem: function(context, e) {
            switch($(e.target).text().trim()) {
                case 'Edit':
                    vueUsers.edit($(context).attr('data-id').trim())
                    break;
                default:

            }
        }
    });

    $('#roles').bootstrapDualListbox({
        nonSelectedListLabel: 'Non-Choisi',
        selectedListLabel: 'Choisi',
        infoText: 'Showing all {0}',
        infoTextFiltered: '<span class="label label-warning">Filtered </span> {0} sur {1}',
        infoTextEmpty: 'Liste vide',
        filterPlaceHolder: 'Rechercher',
        filterTextClear: 'show all'
    });
    $('#boards').bootstrapDualListbox({
        nonSelectedListLabel: 'Non-Choisi',
        selectedListLabel: 'Choisi',
        infoText: 'Showing all {0}',
        infoTextFiltered: '<span class="label label-warning">Filtered </span> {0} sur {1}',
        infoTextEmpty: 'Liste vide',
        filterPlaceHolder: 'Rechercher',
        filterTextClear: 'show all'
    });
    var edit = function () {
        debugger
    }
})
