<?php
/**
 * Created by PhpStorm.
 * User: Goldoni
 * Date: 29.03.2017
 * Time: 02:54
 */

namespace Goldoni;


use App\Comment;
use App\Events\OnComment;
use App\Notifications\DeletedComment;
use App\Notifications\SavedComment;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class CommentSubscriber
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen('eloquent.saved: App\Comment', [$this, 'onNewComment']);
        $events->listen('eloquent.deleted: App\Comment', [$this, 'onDeleteComment']);
    }

    /**
     * @param Comment $comment
     */
    public function onNewComment(Comment $comment)
    {
        //Auth::user()->notify(new SavedComment($comment));
        //$comment->load('user');
        $users = User::limit(5)->get();
        Notification::send($users, new SavedComment($comment));
        $user = Auth::user();
        broadcast(new OnComment($comment,$user))->toOthers();
        Log::info('onNewComment');
        //dd($comment);
    }

    /**
     * @param Comment $comment
     */
    public function onDeleteComment(Comment $comment)
    {
        $admin = Role::where('name', 'ADMINISTRATOR')->first();
        $users = $admin->users;
        Notification::send($users, new DeletedComment($comment));
        Log::info('onDeleteComment');
        //dd($comment);
    }
}