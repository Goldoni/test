<?php

namespace Tests\Unit;

use Badge\Badge;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class  CommentsApiTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }
    public function testFakeThings(){

        Badge::create(['name'=>'Pipelette','action'=>'comments','action_count'=>2]);
        $this->assertEquals(1,Badge::count());
    }

}
