<?php
/**
 * Created by PhpStorm.
 * User: Goldoni
 * Date: 29.03.2017
 * Time: 01:58
 */
namespace Badge;

trait Badgeable
{
    /**
     * @return mixed
     */
    public function badges(){
        return $this->belongsToMany(Badge::class);
    }
}