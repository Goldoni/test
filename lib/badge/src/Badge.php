<?php

namespace Badge;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
