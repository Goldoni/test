<?php
/**
 * Created by PhpStorm.
 * User: Goldoni
 * Date: 29.03.2017
 * Time: 02:22
 */
namespace Badge;
class BadgeSubscriber{
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen('eloquent.save: App\Comment', [$this, 'onNewComment']);
    }
    public function onNewComment(App\Comment $comment)
    {
       $comment = $comment->user;
    }

}