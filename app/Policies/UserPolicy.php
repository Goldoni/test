<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        //
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyRole(["UPDATE_USERS", "ADMINISTRATOR"]);
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAnyRole(["UPDATE_USERS", "USERS_MANAGER", "ADMINISTRATOR"]);
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAnyRole(["DELETE_USERS", "USERS_MANAGER", "ADMINISTRATOR"]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function deputy(User $user)
    {
        return $user->hasAnyRole(["DEPUTY"]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isAdministrator(User $user)
    {
        return $user->hasAnyRole(["ADMINISTRATOR", "UPDATE_USERS", "DELETE_USERS", "USERS_MANAGER", "BOARDS_MANAGER", "UPDATE_BOARDS", "DELETE_BOARDS"]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isDoctor(User $user)
    {
        return $user->hasAnyRole(["DOCTOR", "ADMINISTRATOR"]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isUser(User $user)
    {
        return $user->hasAnyRole(["USER", "ADMINISTRATOR"]);
    }
}
