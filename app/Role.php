<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected $guarded = [];
    
    public function users ()
    {
        return $this->belongsToMany('App\User');
    }
}
