<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function scopeSearchCategories($query, $search)
    {
        if (($search !== '')) {
            $search = filter_var($search, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $search = str_replace(";", "", $search);
            $items = explode(" ", $search);
            $i = 0;
            foreach ($items as $item) {
                if ($i == 0) {
                    $query->where('name', 'LIKE', '%' . $item . '%')->withCount('posts');
                } else {
                    $query->orWhere('name', 'LIKE', '%' . $item . '%')->withCount('posts');
                }
                $i ++;
            }
        }
        return $query;
    }
}
