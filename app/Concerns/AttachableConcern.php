<?php
/**
 * Created by PhpStorm.
 * User: Goldoni
 * Date: 01.07.2017
 * Time: 01:27
 */

namespace App\Concerns;


use App\Attachment;

trait AttachableConcern
{
    /**
     *
     */
    public static function bootAttachableConcern ()
    {
        self::deleted(function ($subject) {
            foreach ($subject->attachments()->get() as $attachment) {
                $attachment->deleteFile();
            }
            $subject->attachments()->delete();
        });

        self::updating(function ($subject) {
            if ($subject->content !== $subject->getOriginal('content')) {
                if (preg_match_all('/src="([^"]+)"/', $subject->content, $matches) > 0) {
                    $images = array_map(function ($mach) {
                        return basename($mach);
                    }, $matches[1]);
                    $attachments = $subject->attachments()->whereNotIn('basename', $images);
                } else {
                    $attachments = $subject->attachments();
                }
                foreach ($attachments->get() as $attachment) {
                    if ($attachment->type === 'wysiwyg') {
                        $attachment->deleteFile();
                        $attachment->delete();
                    }
                }
            }

        });

    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }
}
