<?php

namespace App;

use App\Concerns\AttachableConcern;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use AttachableConcern;
    public $guarded = [];
    public static function getPage($id)
    {
        $page = self::find($id);
        if ($page) {
            $result['success'] = true;
            $result['message'] = 'success';
            $result['page']   = $page;
            $result['status']  = 200;
        } else {
            $result['success'] = false;
            $result['message'] = 'failed';
            $result['status']  = 404;
        }
        return $result;
    }
}
