<?php

namespace App;

use App\Http\Controllers\Includes\DataUsersController;
use Badge\Badgeable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use Badgeable;


    public $appends =['full_name'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','full_name','first_name','last_name','phone1','title','country', 'email','confirmation_token','last_login','about','interest','phone1','language','street','zipcode','city','occupation','confirmation_token','password','visitor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boards()
    {

        return $this->belongsToMany('App\Board');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kids()
    {
        return $this->hasMany('App\Kid');
    }
    /**
     * @param $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getUser($id)
    {
        $user = self::select('id', 'username', 'first_name', 'last_name', 'last_login', 'confirmation_token', 'phone1', 'country', 'about', 'interest', 'language', 'street', 'zipcode', 'city', 'occupation', 'email')->find($id);
        $pluckedRoles = $user->roles->pluck('id')->all();
        $pluckedBoards = $user->boards->pluck('id')->all();
        if ($user) {
            $result['success'] = true;
            $result['message'] = 'success';
            $result['user']   = $user;
            $result['pluckedRoles'] = $pluckedRoles;
            $result['pluckedBoards'] = $pluckedBoards;
            $result['lang'] = DataUsersController::getLanguageOptions();
            $result['status']  = 200;
        } else {
            $result['success'] = false;
            $result['message'] = 'failed';
            $result['status']  = 404;
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public static function draft()
    {
        return self::firstOrCreate(['username' => null]);
    }


    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return  ucfirst($this->first_name)  . ' ' . ucfirst($this->last_name);
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {

        $this->attributes['password'] = bcrypt($value);

    }
}
