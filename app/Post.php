<?php

namespace App;

use App\Concerns\AttachableConcern;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Post extends Model
{
    use AttachableConcern;

    public $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function tags()
    {
        return $this->morphMany(Tag::class, 'tagable');
    }

    /**
     * @param $query
     * @param array $date
     * @return mixed
     */
    public function scopeFilter($query, $date = [])
    {
        if ($date['date']['day'] == true) {
            $query->whereDay('created_at', Carbon::now()->day);
        }
        if ($date['date']['yesterday'] == true) {
            $query->whereDay('created_at', Carbon::yesterday()->day);
        }
        if ($date['date']['week'] == true) {
            $query->whereBetween(DB::raw('date(created_at)'), [Carbon::parse('last monday')->startOfDay(), Carbon::parse('next sunday')->endOfDay()]);
        }
        if ($date['date']['month'] == true) {
            $query->whereMonth('created_at', Carbon::now()->month);
        }
        if ($date['date']['year'] == true) {
            $query->whereYear('created_at', Carbon::now()->year);
        }
        if ($date['category_id'] != null) {
            $query->where('category_id', '=', $date['category_id']);
        }
        return $query->orderBy('created_at', 'desc')->withCount('comments');
    }
    public function scopeSearchPosts($query, $search)
    {
        
        if (($search !== '')) {
            $search = filter_var($search, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $search = str_replace(";", "", $search);
            $items = explode(" ", $search);
            $i = 0;
            foreach ($items as $item) {
                if ($i == 0) {
                    $query->where('name', 'LIKE', '%' . $item . '%')->orWhere('content', 'LIKE', '%' . $item . '%');
                } else {
                    $query->orWhere('name', 'LIKE', '%' . $item . '%')->orWhere('content', 'LIKE', '%' . $item . '%');
                }
                $i ++;
            }
        }
        return $query;
    }

    /**
     * @param $date
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
        Carbon::setLocale(session('locale'));
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}
