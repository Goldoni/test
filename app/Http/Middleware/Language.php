<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // Get the user specific language

            $lang = Auth::user()->language;
            if (App::getLocale() != $lang) {
                App::setLocale($lang);
                session(['locale' => $lang]);
            }
        } elseif (session()->exists('locale')) {
            App::setLocale(session('locale'));
        }

        return $next($request);
    }
}
