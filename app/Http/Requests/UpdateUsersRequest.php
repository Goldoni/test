<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|int',
            'username' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'country' => 'string',
            'language' => 'required|string',
            'phone1' => 'required|max:20',
            'skype' => 'string',
            'roles' => 'array',
            'boards' => 'array',
            'password_confirm' =>'min:6|required_with:password',
            'password'   => 'sometimes|required_with:password_confirm|same:password_confirm|min:6',
        ];
    }
}
