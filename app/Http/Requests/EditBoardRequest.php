<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditBoardRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function wantsJson():bool
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|int',
            'name' => 'required|string',
            'description' => 'required|string',
            'users' => 'array',
        ];
    }
}
