<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsUploadDocsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attachable_id' => 'required|int',
            'attachable_type' => 'required',
            'file' => 'required|mimes:pdf,xls,xlsx,doc,docx,pptx,pps',
        ];
    }
}
