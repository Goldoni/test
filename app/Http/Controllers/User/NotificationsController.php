<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function show($id){
        $notification = Auth::user()->unreadNotifications()->find($id);
        if($notification){
            $notification->markAsRead();
        }
        return redirect()->back();
    }
}
