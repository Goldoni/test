<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @param int $page
     * @param int $perPage
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, int $page = 1, int $perPage = 8)
    {
        $nbPage = ceil(Post::count() / $perPage);
        $posts = Post::orderBy('created_at', 'desc')->withCount('comments')->withCount('attachments')->forPage($page, $perPage)->get();
        $posts->load('user', 'comments', 'category', 'attachments', 'tags');
        if ($request->ajax()) {
            $grid['posts'] = $posts;
            $grid['pagination']['page'] = $page;
            $grid['pagination']['perPage'] = $perPage;
            $grid['pagination']['nbPage'] = $nbPage;

            return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
        } else {
            $categories = Category::orderBy('name', 'asc')->get();
            $categories->load('posts');
            $grid['pagination']['page'] = $page;
            $grid['pagination']['perPage'] = $perPage;
            $grid['pagination']['nbPage'] = $nbPage;


            return view('home', compact('posts', 'categories', 'boards', 'grid'));
        }
    }

    /**
     * @param $lang
     * @return mixed
     */
    public function lang($lang)
    {
        if (Auth::check()) {
            // Get the user specific language
            $user = Auth::user();
            $user->update(['language' => $lang]);
        }
        session(['locale' => $lang]);
        //dd($user->language);
        return redirect()->back();
    }
}
