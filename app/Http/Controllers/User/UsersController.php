<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    public function update(UpdateUserRequest $request)
    {
        $inputs = $request->except(['full_name']);
        $user = User::findOrFail($request->get('id'));
        $user->update($inputs);
        $result['user']    =  $user;
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }
}
