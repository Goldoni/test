<?php

namespace App\Http\Controllers\User;

use App\CalendarEvent;
use App\Http\Controllers\Includes\DataUsersController;
use App\Page;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class PagesController extends Controller
{
    public function profile(Request $request)
    {
        $result['user'] = Auth::user();
        $result['lang'] = DataUsersController::getLanguageOptions();
        if ($request->ajax()) {
            //$result['user']->load('attachments');
            return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
        }
        return view('user.profile', compact('result'));
    }
    
   
    public function calendar()
    {
//        $events = [];
//
//        $data = CalendarEvent::all();
//
//        if ($data->count()) {
//
//            foreach ($data as $key => $value) {
//
//                $events[] = Calendar::event(
//
//                    $value->title,
//
//                    true,
//
//                    new \DateTime($value->start_date),
//
//                    new \DateTime($value->end_date.' +1 day')
//
//                );
//
//            }
//
//        }

        return view('user.calendar');
    }

    public function notification()
    {
        $users = User::select('id', 'first_name', 'last_name')->orderBy('first_name', 'asc')->get();
        return view('user.notification', compact('boards', 'users'));
    }
    public function inbox()
    {
        return view('user.inbox');
    }
    public function tasks()
    {
        return view('user.tasks');
    }
    public function locked()
    {
        return view('user.locked');
    }
    public function pricing()
    {
        return view('user.pricing');
    }

    // Cecam Pages
    public function about()
    {
        $page = Page::where('type', 'about')->get();
        return view('cecam.about', compact('page'));
    }
    public function events()
    {
        $events = [];
        $events[] = Calendar::event(
            'Event One', //event title
            false, //full day event?
            '2017-07-11T0800', //start time (you can also use Carbon instead of DateTime)
            '2017-07-12T0800', //end time (you can also use Carbon instead of DateTime)
            0, //optional event ID
            [
                'url' => 'http://cecam.goldo.org/post',
                //any other full-calendar supported parameters
            ]
        );

        $events[] = Calendar::event(
            'Event Two', //event title
            true, //full day event?
            '2017-07-15T0800', //start time (you can also use Carbon instead of DateTime)
            '2017-07-16T0800', //end time (you can also use Carbon instead of DateTime)
            0, //optional event ID
            [
                'url' => 'http://cecam.goldo.org/post',
                //any other full-calendar supported parameters
            ]
        );

        $events[] = Calendar::event(
            "Valentine's Day", //event title
            true, //full day event?
            new \DateTime('2017-02-14'), //start time (you can also use Carbon instead of DateTime)
            new \DateTime('2017-02-14'), //end time (you can also use Carbon instead of DateTime)
            'stringEventId' //optionally, you can specify an event ID
        );

        $eloquentEvent = CalendarEvent::first(); //EventModel implements MaddHatter\LaravelFullcalendar\Event

        $calendar = Calendar::addEvents($events);
        return view('cecam.events', compact('calendar'));
    }
    public function sport()
    {
        $page = Page::where('type', 'sport')->get();
        return view('cecam.sport', compact('page'));
    }
    public function culture()
    {
        $page = Page::where('type', 'culture')->get();
        return view('cecam.culture', compact('page'));
    }
    public function tech()
    {
        $page = Page::where('type', 'tech')->get();
        return view('cecam.tech', compact('page'));
    }
    public function marketing()
    {
        $page = Page::where('type', 'marketing')->get();
        return view('cecam.marketing', compact('page'));
    }
    public function contact()
    {
        return view('cecam.contact');
    }
    public function ethic()
    {
        $page = Page::where('type', 'ethic')->get();
        return view('cecam.ethic', compact('page'));
    }
    public function post()
    {
        return view('cecam.post');
    }
    public function impressum()
    {
        $page = Page::where('type', 'impressum')->get();
        return view('cecam.impressum', compact('page'));
    }
}
