<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        if ($this->_deputy()) {
            return redirect()->route('home');
        }

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    /**
     * @return bool
     */
    private function _deputy()
    {
        $id = session('deputy');
        if ($id) {
            Auth::loginUsingId($id);
            session()->forget('deputy');
            return true;
        }
        return false;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), ['confirmation_token' => null]);
    }
}
