<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Http\Requests\UpdateUsersRequest;
use App\Role;
use App\User;
use App\Http\Requests\EditUsersRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        //dd(Auth::user()->hasAnyRole(["DEPUTY"]));
        //$this->authorize('deputy', User::class);
        $permitted['create'] = Auth::user()->can('create', User::class);
        $permitted['update'] = Auth::user()->can('update', User::class);
        $permitted['delete'] = Auth::user()->can('delete', User::class);
        $permitted['login'] = Auth::user()->can('deputy', User::class);
        $roles = Role::select('id', 'name')->where('name', '<>', null)->get();
        $boards= Board::select('id', 'name')->where('name', '<>', null)->get();
        $users = User::select('id', 'username', 'first_name', 'last_name', 'country', 'city', 'email', 'phone1', 'created_at')->where('username', '<>', null)->get();
        return view('admin.users', compact('users', 'roles', 'boards', 'permitted'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function view(int $id)
    {
        $result = User::getUser($id);
        return Response::json($result, $result['status'], [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param UpdateUsersRequest $request
     * @return mixed
     */
    public function update(UpdateUsersRequest $request)
    {
        $this->authorize('update', User::class);
        $inputs = $request->except(['full_name', 'created_at', 'confirmation_token']);
        $user = User::findOrFail($request->get('id'));
        $user->update($inputs);
        $user->roles()->sync($request->get('roles'));
        $user->boards()->sync($request->get('boards'));
        $result['id']    =  $user->id;
        $result['username']  = '<a class="theme-color" href="javascript:User.edit(' . $user->id . ');">' . $user->username . '</a>';
        $result['full_name']  = '<a class="theme-color" href="javascript:User.edit(' . $user->id . ');">' . $user->full_name . '</a>';
        $result['country'] =  $user->country;
        $result['actions']  = '<a class="btn theme-color actionA ico_btn tooltips" href="javascript:User.edit(' . $user->id . ');"  title="Edit Board"> <i class="fa fa-edit" aria-hidden="true"></i> </a>';
        $result['actions'] .= '<a class="btn theme-color actionA ico_btn tooltips" href="javascript:User.del(' . $user->id . ');"  title="Delete Board"> <i class="fa fa-trash" aria-hidden="true"></i> </a>';
        $result['message'] = 'Saved successfully';
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $this->authorize('create', User::class);
        $user = User::draft();
        $user->roles()->sync([1]);
        $result = User::getUser($user->id);
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }
    /**
     * @param int $id
     * @return mixed
     */
    public function attempt(int $id)
    {
        $this->authorize('deputy', User::class);
        session(['deputy' => Auth::user()->id]);
        Auth::loginUsingId($id);
        return redirect()->route('home');
    }
}
