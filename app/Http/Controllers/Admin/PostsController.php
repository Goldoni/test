<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Includes\DataPostsController;
use App\Http\Requests\UpdatePostsRequest;
use App\Http\Requests\ViewPostRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class PostsController extends Controller
{

    /**
     * @param Request $request
     * @param int $perPage
     * @param int $page
     * @return mixed
     */
    public function index(Request $request, int $page = 1, int $perPage = 8)
    {
        $nbPage = ceil(Post::count() / $perPage);
        $posts = Post::orderBy('created_at', 'desc')->withCount('comments')->withCount('attachments')->forPage($page, $perPage)->get();
        $posts->load('user', 'comments', 'category', 'attachments', 'tags');
        if ($request->ajax()) {
            $grid['posts'] = $posts;
            $grid['pagination']['page'] = $page;
            $grid['pagination']['perPage'] = $perPage;
            $grid['pagination']['nbPage'] = $nbPage;

            return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
        } else {
            $categories = Category::orderBy('name', 'asc')->get();
            $boards = Auth::user()->boards()->published()->get();
            $categories->load('posts');

            return view('user.posts.posts', compact('posts', 'categories', 'boards'));
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $post->load('user', 'comments', 'category', 'attachments', 'tags');
        $grid['post'] = $post;
        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param ViewPostRequest $request
     * @return mixed
     * @internal param int $page
     * @internal param int $perPage
     * @internal param string $date
     */
    public function view(ViewPostRequest $request)
    {
        $inputs = $request->all();
        $filters = DataPostsController::loadFilters($inputs);
        $posts = Post::Filter($filters)->forPage($filters['page'], $filters['perPage'])->searchPosts($filters['search'])->get();
        $filters['nbPage'] = ceil(Post::Filter($filters)->searchPosts($filters['search'])->count() / $filters['perPage']);
        $posts->load('user', 'comments', 'category', 'attachments', 'tags');
        $grid['posts'] = $posts;
        $grid['date'] = $filters['date'];
        $grid['search'] = $filters['search'];
        $grid['category_id'] = $filters['category_id'];
        $grid['pagination']['page'] = $filters['page'];
        $grid['pagination']['perPage'] = $filters['perPage'];
        $grid['pagination']['nbPage'] = $filters['nbPage'];

        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    public function single(int $id)
    {
        $categories = Category::withCount('posts')->orderBy('created_at', 'desc')->limit(20)->get();
        $categories->load('posts');
        $post = Post::findOrFail($id);
        $grid['post'] = $post;

        return view('user.posts.single', compact('post', 'categories'));
    }

    public function update(UpdatePostsRequest $request)
    {
        $inputs = $inputs = $request->only('id', 'name', 'content', 'category_id');
        $id = $request->get('id');
        $post = Post::findOrFail($id);
        $oldTags = Tag::where('tagable_type', 'App\Post')->where('tagable_id', $id)->delete();
        $post->update($inputs);
        $post->tags()->createMany($request->get('tags'));
        $post->load('user', 'comments', 'category', 'attachments', 'tags');
        $result['post']    =  $post;
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function getVar($data, $key, $value)
    {
        return isset($data[$key]) ? $data[$key] : $value;
    }
}
