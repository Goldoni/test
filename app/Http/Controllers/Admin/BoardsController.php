<?php

namespace App\Http\Controllers\Admin;

use App\Board;
use App\Http\Requests\EditBoardRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class BoardsController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {

        $boards = Board::published()->withCount('users')->get();
        $users = User::select('id', 'first_name', 'last_name')->orderBy('first_name', 'asc')->get();
        return view('admin.boards', compact('boards', 'users'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        $result = Board::getItem($id);
        return Response::json($result, $result['status'], [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param EditBoardRequest $request
     * @return mixed
     */
    public function update(EditBoardRequest $request)
    {
        $this->authorize('update', User::class);
        $inputs = $request->all();
        $board = Board::findOrFail($request->get('id'));
        $board->update($inputs);
        $board->users()->sync($request->get('users'));
        $result['id']    =  $board->id;
        $result['name']  = '<a class="theme-color" href="javascript:Board.edit(' . $board->id . ');">' . $board->name . '</a>';
        $result['description'] =  $board->description;
        $result['actions']  = '<a class="btn theme-color actionA ico_btn tooltips" href="javascript:Board.edit(' . $board->id . ');"  title="Edit Board"> <i class="fa fa-edit" aria-hidden="true"></i> </a>';
        $result['actions'] .= '<a class="btn theme-color actionA ico_btn tooltips" href="javascript:Board.del(' . $board->id . ');"  title="Delete Board"> <i class="fa fa-trash" aria-hidden="true"></i> </a>';
        $result['message'] = 'Saved successfully';
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $board = Board::draft();
        $board->users()->sync([Auth::id()]);
        $result = Board::getItem($board->id);
        return Response::json($result, $result['status'], [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $this->authorize('delete', Board::class);
        $board = Board::find($id);
        if ($board) {
            $board->delete();
            $result['success'] = true;
            $result['message'] = 'Deleted was successfully';
            $result['board']   = $board;
            $result['status']  = 200;
        } else {
            $result['success'] = false;
            $result['message'] = 'Deleted was successfully failed';
            $result['status']  = 404;
        }
        return Response::json($result, $result['status'], [], JSON_NUMERIC_CHECK);
    }
}
