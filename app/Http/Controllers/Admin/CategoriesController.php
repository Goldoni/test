<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\UpdateCategoriesRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class CategoriesController extends Controller
{
    /**
     * @param Request $request
     * @return
     * @internal param int $id
     */
    public function view(Request $request)
    {
        $inputs = $request->all();
        $filters['page'] = $this->getVar($inputs, 'page', 1);
        $filters['date']['day'] = $this->getVar($inputs, 'day', null);
        $filters['date']['yesterday'] = $this->getVar($inputs, 'yesterday', null);
        $filters['date']['week'] = $this->getVar($inputs, 'week', null);
        $filters['date']['month'] = $this->getVar($inputs, 'month', null);
        $filters['date']['year'] = $this->getVar($inputs, 'year', null);
        $filters['perPage'] = $this->getVar($inputs, 'perPage', 8);

        $posts = Post::Filter($filters['date'])->orderBy('created_at', 'desc')->withCount('comments')->withCount('attachments')->forPage($filters['page'], $filters['perPage'])->where('category_id', '=', $inputs['id'])->get();
        $filters['nbPage'] = ceil(Post::Filter($filters['date'])->orderBy('created_at', 'desc')->withCount('comments')->withCount('attachments')->where('category_id', '=', $inputs['id'])->count() / $filters['perPage']);
        $posts->load('user', 'comments', 'category');
        $grid['posts'] = $posts;
        $grid['date'] = $filters['date'];
        $grid['pagination']['page'] = $filters['page'];
        $grid['pagination']['perPage'] = $filters['perPage'];
        $grid['pagination']['nbPage'] = $filters['nbPage'];

        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    public function load(Request $request, string $search)
    {
        $categories = Category::searchCategories($search)->get();
        $grid['categories'] = $categories;
        $grid['search'] = $search;

        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @return mixed
     */
    public function show()
    {
        $categories = Category::withCount('posts')->orderBy('created_at', 'desc')->get();
        $grid['categories'] = $categories;

        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param UpdateCategoriesRequest $request
     * @return mixed
     */
    public function update(UpdateCategoriesRequest $request)
    {
        $inputs = $inputs = $request->only('id', 'name', 'content');
        $id = $request->get('id');
        $category = Category::findOrFail($id);
        $oldTags = Tag::where('tagable_type', 'App\Post')->where('tagable_id', $id)->delete();
        $category->update($inputs);
        $result['category']    =  $category;
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function getVar($data, $key, $value)
    {
        return isset($data[$key]) ? $data[$key] : $value;
    }
}
