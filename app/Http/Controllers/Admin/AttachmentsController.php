<?php

namespace App\Http\Controllers\Admin;

use App\Attachment;
use App\Http\Requests\AttachmentRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImagesRequest;
use App\Http\Requests\PostsUploadDocsRequest;
use App\Http\Requests\PostsUploadFilesRequest;
use App\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Request;

class AttachmentsController extends Controller
{
    public function index(ImagesRequest $request)
    {
        $type = $request->get('attachable_type');
        $id = $request->get('attachable_id');
        $file = $request->file('attachment');

        if (class_exists($type) && method_exists($type, 'attachments')) {
            $subject = call_user_func($type . '::find', $id);
            if ($subject) {
                switch ($type) {
                    case 'App\Post':
                        $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                        $attachment->uploadPostsFile($file);
                        $attachment->save();
                        $result['success'] = 'save successful';
                        break;
                    case 'App\Post':
                        echo "i égal 1";
                        break;
                    case 2:
                        echo "i égal 2";
                        break;
                }
                return $attachment;
            } else {
                return Response::json(['attachable_id' => 'Ce contenu ne peu pas recevoir de fichier'], 403, [], JSON_NUMERIC_CHECK);
            }
        } else {
            return Response::json(['error' => 'method attachments undefined'], 403, [], JSON_NUMERIC_CHECK);
        }
    }

    /**
     * @param int $post_id
     * @param int $user_id
     * @param int $attachment_id
     * @return mixed
     */
    public function destroy(int $post_id, int $user_id, int $attachment_id)
    {
        $attachment = Attachment::findOrFail($attachment_id);
        if ($user_id == Auth::user()->id || Auth::user()->hasAnyRole(["ADMINISTRATOR"])) {
            $attachment->delete();
            $post = Post::findOrFail($post_id);
            $post->load('user', 'comments', 'category', 'attachments', 'tags');
            $grid['post'] = $post;
            return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
        } else {
            return Response::json('ce fichier ne vous appartient pas');
        }

    }

    /**
     * @param AttachmentRequest $request
     * @return mixed
     */
    public function store(AttachmentRequest $request)
    {
        $type = $request->get('attachable_type');
        $id = $request->get('attachable_id');
        $file = $request->file('attachment');

        if (class_exists($type) && method_exists($type, 'attachments')) {
            $subject = call_user_func($type . '::find', $id);
            if ($subject) {
                switch ($type) {
                    case 'App\Post':
                        $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                        $attachment->uploadPostsFile($file);
                        $attachment->save();
                        $result['success'] = 'save successful';
                        break;
                    case 'App\User':
                        $oldAttachment = Attachment::where('attachable_type', 'App\User')->where('attachable_id', $id)->first();
                        if ($oldAttachment) {
                            $oldAttachment->deleteFileForUser();
                            $oldAttachment->delete();
                        }
                        $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                        $attachment->uploadUsersFile($file);
                        $attachment->save();
                        $result['success'] = 'save successful';
                        break;
                    case 2:
                        echo "i égal 2";
                        break;
                }
                return $attachment;
            } else {
                return Response::json(['attachable_id' => 'Ce contenu ne peu pas recevoir de fichier'], 403, [], JSON_NUMERIC_CHECK);
            }
        } else {
            return Response::json(['error' => 'method attachments undefined'], 403, [], JSON_NUMERIC_CHECK);
        }
    }

    /**
     * @param PostsUploadFilesRequest $request
     * @return Attachment
     */
    public function uploadFiles(PostsUploadFilesRequest $request)
    {
        $type = $request->get('attachable_type');
        $id = $request->get('attachable_id');
        $file = $request->file('file');
        $dataType = $request->get('type');
        if (class_exists($type) && method_exists($type, 'attachments')) {
            $subject = call_user_func($type . '::find', $id);
            if ($subject) {
                if ($type === 'App\Post') {
                    $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                    $attachment->uploadPostsFile($file, $dataType);
                    $attachment->save();
                    $result['success'] = 'save successful';
                }
                if ($type === 'App\Page') {
                    $ds = DIRECTORY_SEPARATOR;
                    $path = 'uploads' . $ds . 'pages' . $ds . $type;
                    $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                    $attachment->uploadPagesFile($file, $dataType);
                    $attachment->save();
                    $result['success'] = 'save successful';
                }
                return $attachment;
            } else {
                return Response::json(['attachable_id' => 'Ce contenu ne peu pas recevoir de fichier'], 403, [], JSON_NUMERIC_CHECK);
            }
        } else {
            return Response::json(['error' => 'method attachments undefined'], 403, [], JSON_NUMERIC_CHECK);
        }

    }

    public function uploadDocuments(PostsUploadDocsRequest $request)
    {
        $type = $request->get('attachable_type');
        $id = $request->get('attachable_id');
        $file = $request->file('file');
        $dataType = $request->get('type');
        if (class_exists($type) && method_exists($type, 'attachments')) {
            $subject = call_user_func($type . '::find', $id);
            if ($subject) {
                if ($type === 'App\Post') {
                    $attachment = new Attachment($request->only('attachable_type', 'attachable_id'));
                    $attachment->uploadPostsFile($file, $dataType);
                    $attachment->save();
                    $result['success'] = 'save successful';
                }
                return $attachment;
            } else {
                return Response::json(['attachable_id' => 'Ce contenu ne peu pas recevoir de fichier'], 403, [], JSON_NUMERIC_CHECK);
            }
        } else {
            return Response::json(['error' => 'method attachments undefined'], 403, [], JSON_NUMERIC_CHECK);
        }

    }
}
