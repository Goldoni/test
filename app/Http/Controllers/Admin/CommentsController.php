<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CommentsController extends Controller
{
    public function index($id, $type)
    {
        //$comment1 = Comment::create(['content' => 'test','commentable_type' => 'App\User','commentable_id' =>1]);
        $comments = Comment::allFor('App\\' . $type, $id);
        return Response::json($comments, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function store(Request $request, $comment)
    {
        $data = $request->all();
        $comment = Comment::create([
            'commentable_id' => $data['commentable_id'],
            'commentable_type' => 'App\\Post',
            'content' => $data['content'],
            'reply' => $data['reply'],
            'user_id' => $data['user_id']
        ]);
        $comment->load('user');
        $grid['comment'] = $comment;
        $grid['unreadNotifications'] = Auth::user()->unreadNotifications;
        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request, $id)
    {
        $comment = Comment::find($id);
        if ($comment->user_id == Auth::user()->id) {
            Comment::where('reply', '=', $comment->id)->delete();
            $comment->delete();
            return Response::json($comment, 200, [], JSON_NUMERIC_CHECK);
        } else {
            return Response::json('ce commentaire ne vous appartent');
        }
    }

    public function edit(Request $request, $id)
    {
        $comment = Comment::find($id);
        $inputs = $request->all();
        if ($comment->user_id == Auth::user()->id) {
            $comment->update($inputs);
            return Response::json($comment, 200, [], JSON_NUMERIC_CHECK);
        } else {
            return Response::json('ce commentaire ne vous appartient pas');
        }
    }
}
