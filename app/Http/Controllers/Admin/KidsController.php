<?php

namespace App\Http\Controllers\Admin;

use App\Kid;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class KidsController extends Controller
{
    public function kid()
    {
        $users = User::select('id', 'username', 'first_name', 'last_name', 'country', 'city', 'email', 'phone1', 'created_at')->where('username', '<>', null)->get();
        return view('user.kids.kid', compact('users'));
    }

    /**
     * @return mixed
     */
    public function view()
    {
        $kids = Auth::user()->kids()->get();
        $grid['kids'] = $kids;
        //$grid['vaccine'] = DataKidsController::getSelectOptionsVaccine();
        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        $kid = Kid::find($id);
        $grid['kid'] = $kid;
        //$grid['vaccine'] = DataKidsController::getSelectOptionsVaccine();
        return Response::json($grid, 200, [], JSON_NUMERIC_CHECK);
    }
}
