<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Includes\DataUsersController;
use App\Http\Requests\UpdatePagesRequest;
use App\Page;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::get();
        return view('admin.pages.pages', compact('pages'));
    }

    public function view(int $id)
    {
        $result = Page::getPage($id);
        return Response::json($result, $result['status'], [], JSON_NUMERIC_CHECK);
    }
    public function update(UpdatePagesRequest $request)
    {
        $inputs = $inputs = $request->only('id', 'title', 'content', 'type');
        $id = $request->get('id');
        $page = Page::findOrFail($id);
        $page->update($inputs);
        $result['page']    =  $page;
        return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function profile(Request $request)
    {
        $result['user'] = Auth::user();
        $result['lang'] = DataUsersController::getLanguageOptions();
        if ($request->ajax()) {
            //$result['user']->load('attachments');
            return Response::json($result, 200, [], JSON_NUMERIC_CHECK);
        }
        return view('user.profile', compact('result'));
    }

    public function calendar()
    {
//        $events = [];
//
//        $data = CalendarEvent::all();
//
//        if ($data->count()) {
//
//            foreach ($data as $key => $value) {
//
//                $events[] = Calendar::event(
//
//                    $value->title,
//
//                    true,
//
//                    new \DateTime($value->start_date),
//
//                    new \DateTime($value->end_date.' +1 day')
//
//                );
//
//            }
//
//        }

        return view('user.calendar');
    }

    public function notification()
    {
        $users = User::select('id', 'first_name', 'last_name')->orderBy('first_name', 'asc')->get();
        return view('user.notification', compact('boards', 'users'));
    }
    public function inbox()
    {
        return view('user.inbox');
    }
    public function tasks()
    {
        return view('user.tasks');
    }
    public function locked()
    {
        return view('user.locked');
    }
    public function pricing()
    {
        return view('user.pricing');
    }
}
