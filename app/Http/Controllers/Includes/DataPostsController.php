<?php

namespace App\Http\Controllers\Includes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataPostsController extends Controller
{
    public static function loadFilters ($data = [])
    {
        $filters =[];
        $filters['page'] = self::getVar($data, 'page', 1);
        $filters['search'] = self::getVar($data, 'search', '');
        $filters['category_id'] = self::getVar($data, 'category_id', null);
        $filters['date']['day'] = self::getVar($data, 'day', null);
        $filters['date']['yesterday'] = self::getVar($data, 'yesterday', null);
        $filters['date']['week'] = self::getVar($data, 'week', null);
        $filters['date']['month'] = self::getVar($data, 'month', null);
        $filters['date']['year'] = self::getVar($data, 'year', null);
        $filters['perPage'] = self::getVar($data, 'perPage', 8);

        return $filters;
    }

    public static function getVar($data, $key, $value)
    {
        return isset($data[$key]) ? $data[$key] : $value;
    }
}
