<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Kid extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * @param $date
     * @return string
     */
    public function getBirthAttribute($date)
    {
        Carbon::setLocale(session('locale'));

        return Carbon::parse($date)->format('d/m/Y');
    }

    /**
     * @param $date
     * @return string
     */
    public function getDewormingAttribute($date)
    {
        Carbon::setLocale(session('locale'));

        return Carbon::parse($date)->format('d/m/Y');
    }

    public function getVaccineAttribute($data)
    {

        return json_decode($data);
    }

}
