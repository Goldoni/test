<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = [ 'name','description' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public static function draft()
    {
        return self::firstOrCreate(['name' => null, 'description' => '']);
    }
    public static function getItem($id)
    {
        $board = self::find($id);
        $plucked = $board->users->pluck('id')->all();
        if ($board) {
            $result['success'] = true;
            $result['message'] = 'success';
            $result['board']   = $board;
            $result['plucked'] = $plucked;
            $result['status']  = 200;
        } else {
            $result['success'] = false;
            $result['message'] = 'failed';
            $result['status']  = 404;
        }
        return $result;
    }
    public function scopePublished($query)
    {
        return $query->where('name', '<>', null)->orderBy('name', 'asc');
    }
}
