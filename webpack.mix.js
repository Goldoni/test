const { mix } = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admin/users/users.js', 'public/js/admin/users')
    .js('resources/assets/js/admin/boards/boards.js', 'public/js/admin/boards')
    .js('resources/assets/js/admin/pages/pages.js', 'public/js/admin/pages')
    .js('resources/assets/js/user/profile.js', 'public/js/user')
    .js('resources/assets/js/user/kid/kid.js', 'public/js/user/kid')
    .js('resources/assets/js/user/posts/posts.js', 'public/js/user/posts');
