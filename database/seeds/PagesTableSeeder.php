<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'welcome',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'home',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);
        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'about',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'sport',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'culture',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'impressum',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'contact',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'tech',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'marketing',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('pages')->insert([
            'title' => $faker->name,
            'content' => $faker->sentence(100),
            'type' => 'ethic',
            'created_at' => $faker->dateTimeThisYear,
            'updated_at' => DB::raw('now()')
        ]);
    }
}
