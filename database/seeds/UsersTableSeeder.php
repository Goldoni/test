<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $user  = DB::table('users')->insertGetId([
            'username' => 'Goldoni',
            'first_name' => 'Goldoni',
            'last_name' => 'Goldoni',
            'email' => 'goldoni@contact.de',
            'country' => 'CM',
            'language' => 'fr',
            'phone1' => $faker->phoneNumber,
            'password' => \Illuminate\Support\Facades\Hash::make('000000'),
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
            'confirmation_token' => null
        ]);
        $u = \App\User::find($user);
        $u->roles()->sync([1, 2, 3, 4, 5]);

        $user = DB::table('users')->insertGetId([
            'username' => 'Bertrand',
            'first_name' => 'Bertrand',
            'last_name' => 'Bertrand',
            'email' => 'bertrand@contact.de',
            'country' => 'CM',
            'language' => 'fr',
            'phone1' => $faker->phoneNumber,
            'password' => \Illuminate\Support\Facades\Hash::make('000000'),
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
            'confirmation_token' => null
        ]);
        $u = \App\User::find($user);
        $u->roles()->sync([1, 2, 3, 4, 5]);

        $user = DB::table('users')->insertGetId([
            'username' => 'Wald',
            'first_name' => 'Wald',
            'last_name' => 'Wald',
            'email' => 'wald@contact.de',
            'country' => 'CM',
            'language' => 'fr',
            'phone1' => $faker->phoneNumber,
            'password' => \Illuminate\Support\Facades\Hash::make('000000'),
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
            'confirmation_token' => null
        ]);
        $u = \App\User::find($user);
        $u->roles()->sync([1, 2, 3, 4, 5]);
    }
}
