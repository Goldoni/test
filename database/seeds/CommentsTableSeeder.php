<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 300; $i++) {
            DB::table('comments')->insert([
                'content' => $faker->sentence(100),
                'commentable_type' => 'App\Post',
                'commentable_id' => $faker->numberBetween(1, 150),
                'user_id' => $faker->numberBetween(1, 20),
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => DB::raw('now()')
            ]);
        }
    }
}
