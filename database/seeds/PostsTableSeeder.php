<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 4; $i++) {
            DB::table('posts')->insert([
                'name' => $faker->name,
                'content' => $faker->sentence(100),
                'user_id' => $faker->numberBetween(1, 3),
                'category_id' => $faker->numberBetween(1, 5),
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => DB::raw('now()')
            ]);
        }
    }
}
