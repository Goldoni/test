<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            DB::table('categories')->insert([
                'name' => $faker->name,
                'content' => $faker->sentence(100),
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => DB::raw('now()')
            ]);
        }
    }
}
