<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->dateTime('birth')->nullable();
            $table->char('gender',1)->nullable();
            $table->float('weight',4,2)->nullable();
            $table->integer('size')->nullable();
            $table->string('food')->default('');
            $table->char('status_vaccine',3)->default('NO');
            $table->string('vaccine')->default(0);
            $table->char('vitamins',3)->default('NO');
            $table->date('deworming')->nullable();
            $table->char('allergy',3)->default('NO');
            $table->char('asthmatic',3)->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kids');
    }
}
