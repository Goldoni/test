<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('lang/{lang}', 'HomeController@lang')->name('lang');


Route::get('/shop', function () {
    return view('shop');
})->name('shop');
Route::get('/timetable', function () {
    return view('timetable');
})->name('timetable');
Route::get('/all_doctors', function () {
    return view('all_doctors');
})->name('all_doctors');
Route::get('/blog', function () {
    return view('blog');
})->name('blog');
Route::get('/confirm/{id}/{token}', 'Auth\RegisterController@confirm');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'roles', 'roles' => ["ADMINISTRATOR", "UPDATE_USERS", "DELETE_USERS", "USERS_MANAGER", "BOARDS_MANAGER", "UPDATE_BOARDS", "DELETE_BOARDS"] ], function () {
    Route::get('boards', 'BoardsController@index')->name('admin.boards');
    Route::get('boards/show/{id}', 'BoardsController@show')->where('id', '[0-9]+');
    Route::put('boards/update', 'BoardsController@update');
    Route::get('boards/create', 'BoardsController@create');
    Route::delete('boards/destroy/{id}', 'BoardsController@destroy')->where('id', '[0-9]+');
    //users routes
    Route::get('users', 'UsersController@index')->name('admin.users');
    Route::get('users/view/{id}', 'UsersController@view');
    Route::put('users/update', 'UsersController@update');
    Route::get('users/create', 'UsersController@create');

    //users routes
    Route::get('posts/single/{id}', 'PostsController@single')->name('admin.posts.single')->where('id', '[0-9]+');

    Route::get('users/attempt/{id}', 'UsersController@attempt')->name('deputy');

    //cecam routes
    // Posts Routes
    Route::get('posts', 'PostsController@index')->name('admin.posts');
    Route::get('posts/show/{id}', 'PostsController@show')->where('id', '[0-9]+');
    Route::post('posts/view', 'PostsController@view');
    Route::put('posts/update', 'PostsController@update');



    //Categories Routes
    Route::get('categories/show', 'CategoriesController@show');
    Route::post('categories/view', 'CategoriesController@view');
    Route::get('categories/load/{search?}', 'CategoriesController@load');
    Route::put('categories/update', 'CategoriesController@update');

    // Notifications Routes
    Route::post('attachments/store', 'AttachmentsController@store');
    Route::post('posts/attachments', 'AttachmentsController@uploadFiles');
    Route::post('posts/attachments/upload', 'AttachmentsController@uploadDocuments');
    Route::delete('posts/attachments/delete/{post_id}/{user_id}/{attachment_id}', 'AttachmentsController@destroy')->where('post_id', '[0-9]+')->where('user_id', '[0-9]+')->where('attachment_id', '[0-9]+');
    Route::get('blog', function () {
        return view('admin.blog');
    })->name('admin.blog');

});
Route::group(['namespace' => 'Admin', 'middleware' => 'roles', 'roles' => ["ADMINISTRATOR", "UPDATE_USERS", "DELETE_USERS", "USERS_MANAGER", "BOARDS_MANAGER", "UPDATE_BOARDS", "DELETE_BOARDS"] ], function () {
    Route::get('pages', 'PagesController@index')->name('admin.pages');
    Route::get('pages/view/{id}', 'PagesController@view')->name('admin.pages.view');
    Route::put('pages/update', 'PagesController@update')->name('admin.pages.update');
    Route::post('pages/attachments', 'AttachmentsController@uploadFiles');
});
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'roles', 'roles' => ["DEPUTY"] ], function () {
    Route::get('users/attempt/{id}', 'UsersController@attempt')->name('deputy');
});
Route::group(['middleware' => 'roles', 'roles' => ["USER", "ADMINISTRATOR"] ], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['namespace' => 'User', 'middleware' => 'roles', 'roles' => ["USER", "ADMINISTRATOR"]], function () {
    Route::get('profile', 'PagesController@profile')->name('profile');
    Route::put('users/update', 'UsersController@update');
    Route::get('notification', 'PagesController@notification')->name('notification');
    Route::get('tasks', 'PagesController@tasks')->name('tasks');
    Route::get('inbox', 'PagesController@inbox')->name('inbox');
    Route::get('locked', 'PagesController@locked')->name('locked');
    Route::get('calendar', 'PagesController@calendar')->name('calendar');
    Route::get('pricing', 'PagesController@pricing')->name('pricing');


    // comments Routes
    Route::get('comments/{id}/{type}', 'CommentsController@index')->name('comments');
    Route::post('comments/{post}', 'CommentsController@store');
    Route::delete('comments/destroy/{id}', 'CommentsController@destroy');
    Route::put('comments/edit/{id}', 'CommentsController@edit');

    // Posts Routes
    Route::get('posts', 'PostsController@index')->name('posts');
    Route::get('posts/show/{id}', 'PostsController@show')->where('id', '[0-9]+');
    Route::post('posts/view', 'PostsController@view');
    Route::put('posts/update', 'PostsController@update');

    //Categories Routes
    Route::get('categories/show', 'CategoriesController@show');
    Route::post('categories/view', 'CategoriesController@view');
    Route::get('categories/load/{search?}', 'CategoriesController@load');
    Route::put('categories/update', 'CategoriesController@update');

    // Notifications Routes
    Route::post('attachments/store', 'AttachmentsController@store');
    Route::post('posts/attachments', 'AttachmentsController@uploadFiles');
    Route::post('posts/attachments/upload', 'AttachmentsController@uploadDocuments');
    Route::delete('posts/attachments/delete/{post_id}/{user_id}/{attachment_id}', 'AttachmentsController@destroy')->where('post_id', '[0-9]+')->where('user_id', '[0-9]+')->where('attachment_id', '[0-9]+');

    Route::get('notifications/show/{id}', 'NotificationsController@show');

    // Kids Routes
    Route::get('kid', 'KidsController@kid')->name('kid');
    Route::get('kids/view', 'KidsController@view');
    Route::get('kids/show/{id}', 'KidsController@show')->where('id', '[0-9]+');



});







Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace' => 'User'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/about', 'PagesController@about')->name('about');
    Route::get('/contact', 'PagesController@contact')->name('contact');
    Route::get('/blog', 'PagesController@blog')->name('blog');
    Route::get('/culture', 'PagesController@culture')->name('culture');
    Route::get('/sport', 'PagesController@sport')->name('sport');
    Route::get('/tech', 'PagesController@tech')->name('tech');
    Route::get('/marketing', 'PagesController@marketing')->name('marketing');
    Route::get('/events', 'PagesController@events')->name('events');
    Route::get('/ethic', 'PagesController@ethic')->name('ethic');
    Route::get('/post', 'PagesController@post')->name('post');
    Route::get('/impressum', 'PagesController@impressum')->name('impressum');
});
